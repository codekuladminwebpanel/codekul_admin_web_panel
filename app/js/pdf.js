var count = 0;

$(document).ready(function () {

    $("input").prop("disabled", true);

    $('#idSelect').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        console.log("valueSelected", valueSelected);
        $('#branch1').text(valueSelected);
    });


    var myData = localStorage.getItem('myVariable');
    var myObj = JSON.parse(myData);
    console.log('myData: ', myObj);
    console.log('myData: ', myObj.feesUpdates[0].receiptNo);

    console.log('myData: ', myObj.feesUpdates[myObj.feesUpdates.length - 1].receiptNo);

    // receiptNo
    var mypaidFess = localStorage.getItem('myVariable1');
    console.log('mypaidFess: ', mypaidFess);
    var myPage = localStorage.getItem('Page');
    console.log('myPage: ', myPage);


    // --------------------------Date-------------------------
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = dd + '/' + mm + '/' + yyyy;
    $('#receiptDate').text(today);
    var ReceiptNo = myObj.feesUpdates[myObj.feesUpdates.length - 1].receiptNo;
    $('#receiptNo').text(ReceiptNo);

    $('#receiptDate1').text(today);
    $('#receiptNo1').text(ReceiptNo);
    // --------------------------close Date-------------------------


    // console.log("Hello", myVariable, myVariable1, Page);


    var fullName = myObj.firstName + " " + myObj.middleName + " " + myObj.lastName

    var courses = myObj.fees.courses;
    var paymentMod = myObj.fees.paymentMod;
    var totalFees = myObj.fees.finalFees;
    var paidFees = myObj.fees.totalPaidFees;
    var pendingFees = myObj.fees.pendingFees;



    if (myPage == "FeesUpdate") {
        var FeesUpdateList = myObj.feesUpdates
        var lengthList = FeesUpdateList.length - 1;
        var LastUpdate = FeesUpdateList[lengthList];
        console.log("FeesUpdateList Data :===========>", LastUpdate.paidFees);
        console.log("FeesUpdateList Data :===========>", LastUpdate.paymentMod);
        var updateFeespaymentMod = LastUpdate.paymentMod;
        console.log("Hello");
        if (updateFeespaymentMod == "Cash") {
            cash();
        } else if (updateFeespaymentMod == "Cheque") {
            chequeANDnetBanking();
        } else {
            chequeANDnetBanking();
        }

        function cash() {
            console.log("Cash Payment")
            $("#sFullName").val(fullName);
            $('#sCourse').text(courses);
            $('#branch').val('Nalstop');
            $('#amount').val(LastUpdate.paidFees);
            $('#paidFees').val(paidFees);
            $('#totalFees').val(totalFees);
            $('#remainingFees').val(pendingFees);
            $('#purposePayment').val('Course Fees');

            $("#sFullName1").val(fullName);
            $('#sCourse1').text(courses);
            // $('#branch1').val('Nalstop');
            $('#amount1').val(LastUpdate.paidFees);
            $('#paidFees1').val(paidFees);
            $('#totalFees1').val(totalFees);
            $('#remainingFees1').val(pendingFees);
            $('#purposePayment1').val('Course Fees');

            $('#CashID').attr("checked", "checked");
            $('#CashID1').attr("checked", "checked");
        }
        function chequeANDnetBanking() {
            console.log("cheque and net BankingI Payment")

            var chequePaidFess = mypaidFess;

            var chPaidFees = parseInt(chequePaidFess) + parseInt(myObj.fees.totalPaidFees);
            console.log('chPaidFees', chPaidFees)
            var chPendingFees = parseInt(pendingFees) - parseInt(chequePaidFess);
            console.log('chPaidFees', chPendingFees)



            var finalFees = totalFees;
            var chequePendingFee = finalFees - chequePaidFess;

            console.log("chequePaidFess", chequePaidFess);
            console.log("finalFees", finalFees);
            console.log("chequePendingFee", chequePendingFee);

            $("#sFullName").val(fullName);
            $('#sCourse').text(courses);
            $('#branch').val('Nalstop');
            $('#amount').val(chequePaidFess);
            $('#paidFees').val(chPaidFees);
            $('#totalFees').val(totalFees);
            $('#remainingFees').val(chPendingFees);
            $('#purposePayment').val('Course Fees');


            $("#sFullName1").val(fullName);
            $('#sCourse1').text(courses);
            // $('#branch1').val('Nalstop');
            $('#amount1').val(chequePaidFess);
            // $('#paidFees1').val(chequePaidFess);
            // $('#totalFees1').val(totalFees);
            // $('#remainingFees1').val(chequePendingFee);
            $('#paidFees1').val(chPaidFees);
            $('#totalFees1').val(totalFees);
            $('#remainingFees1').val(chPendingFees);
            $('#purposePayment1').val('Course Fees');

            if (updateFeespaymentMod == "Cheque") {
                console.log("cheque  Payment")

                $('#ChequeID').attr("checked", "checked");
                $('#ChequeID1').attr("checked", "checked");
            } else {
                console.log("  Banking Payment")
                $('#NetBankingID').attr("checked", "checked");
                $('#NetBankingID1').attr("checked", "checked");
            }
        }

    } else {

        if (paymentMod == "Cash") {
            cashEnroll();
        } else if (paymentMod == "Cheque") {
            chequeANDnetBankingEnroll();
        } else {
            chequeANDnetBankingEnroll();
        }

        function cashEnroll() {
            console.log("Cash Payment")
            $("#sFullName").val(fullName);
            $('#sCourse').text(courses);
            $('#branch').val('Nalstop');


            $('#amount').val(paidFees);
            $('#paidFees').val(paidFees);
            $('#totalFees').val(totalFees);
            $('#remainingFees').val(pendingFees);
            $('#purposePayment').val('Course Fees');

            $("#sFullName1").val(fullName);
            $('#sCourse1').text(courses);
            // $('#branch1').val('Nalstop');
            $('#amount1').val(paidFees);
            $('#paidFees1').val(paidFees);
            $('#totalFees1').val(totalFees);
            $('#remainingFees1').val(pendingFees);
            $('#purposePayment1').val('Course Fees');

            $('#CashID').attr("checked", "checked");
            $('#CashID1').attr("checked", "checked");
        }
        function chequeANDnetBankingEnroll() {
            console.log("cheque and net BankingI Payment")

            var chequePaidFess = mypaidFess;
            var finalFees = totalFees;
            var chequePendingFee = finalFees - chequePaidFess;

            console.log("chequePaidFess", chequePaidFess);
            console.log("finalFees", finalFees);
            console.log("chequePendingFee", chequePendingFee);

            $("#sFullName").val(fullName);
            $('#sCourse').text(courses);
            $('#branch').val('Nalstop');
            $('#amount').val(chequePaidFess);
            $('#paidFees').val(chequePaidFess);
            $('#totalFees').val(totalFees);
            $('#remainingFees').val(chequePendingFee);
            $('#purposePayment').val('Course Fees');


            $("#sFullName1").val(fullName);
            $('#sCourse1').text(courses);
            // $('#branch1').val('Nalstop');
            $('#amount1').val(chequePaidFess);
            $('#paidFees1').val(chequePaidFess);
            $('#totalFees1').val(totalFees);
            $('#remainingFees1').val(chequePendingFee);
            $('#purposePayment1').val('Course Fees');

            if (paymentMod == "Cheque") {
                console.log("cheque  Payment")

                $('#ChequeID').attr("checked", "checked");
                $('#ChequeID1').attr("checked", "checked");
            } else {
                console.log("  Banking Payment")
                $('#NetBankingID').attr("checked", "checked");
                $('#NetBankingID1').attr("checked", "checked");
            }
        }
    }



});