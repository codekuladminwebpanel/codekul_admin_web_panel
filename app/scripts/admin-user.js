

$(document).ready(function () {

    // "http://192.168.1.19:7080/getuser";
    var getCoursesUrl = 'http://192.168.1.19:7080/getuser';
    // var getCoursesUrl = protocol + server + ":" + port + appName + "/allCourses";
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                var xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (exception) {
                var xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }
        } else {
            var xhr = new XMLHttpRequest();
        }
    } else {
        alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('GET', getCoursesUrl, true);
    console.log('getCoursesUrl', getCoursesUrl);
    xhr.send();
    var courseList;
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                courseList = JSON.parse(xhr.responseText);
                console.log('courseList', courseList);
                // var inHTML = "";
                // $.each(courseList, function (index, value) {
                //     // console.log("courseList", courseList);
                //     var val = value.courseName;
                //     var htm = '';
                //     htm += '<option value ="' + index + '">' + val + '</option>';
                //     $('#chCourseList').append(htm);
                //     $('#chCourseList').multiselect('rebuild');
                //     var htm = '';
                //     htm += '<option value ="' + index + '">' + val + '</option>';
                //     $('#chCourseListEx').append(htm);
                //     $('#chCourseListEx').multiselect('rebuild');
                // });
            } else {
                console.log('start web services', + xhr.status + 'Error Message: ' + xhr.statusText)

            }
        }
    };
});
//---------------------------------- validaions -------------------------------------------------------------------------------------------------------------------
function validateFullName(id) {
    var fullName = $('#' + id).val();
    var validateName = /^[0-9]+$/;
    if (fullName === '') {
        $('#errorFName').addClass('showItem');
        $('.errorMessage').css('color', 'red');
        $('#errorFName').html('Please enter name');
        $('#' + id).css('border', '1px solid red');
        $(id).css('display', 'none');
        return false;
    } else {
        if (fullName.match(validateName)) {
            $('#errorFName').addClass('showItem');
            $('.errorMessage').css('color', 'red');
            $('#errorFName').html('Please enter only alphabets');
            $('#' + id).css('border', '1px solid red');
            $(id).css('display', 'none');
            return false;
        } else {
            $('#errorFName').removeClass('showItem');
            $('#errorFName').addClass('showItem');
            $('#errorFName').html(' ');
            $('#' + id).css('border', '1px solid green');
            $(id).css('display', 'none');
            return true;
        }
    }
}
function validateBranch(id) {
    var Branch = $('#' + id).val();
    var validateName = /^[0-9]+$/;
    if (Branch === '') {
        $('#errorBranch').addClass('showItem');
        $('.errorMessage').css('color', 'red');
        $('#errorBranch').html('Please enter Branch');
        $('#' + id).css('border', '1px solid red');
        $(id).css('display', 'none');
        return false;
    } else {
        if (Branch.match(validateName)) {
            $('#errorBranch').addClass('showItem');
            $('.errorMessage').css('color', 'red');
            $('#errorBranch').html('Please enter only alphabets');
            $('#' + id).css('border', '1px solid red');
            $(id).css('display', 'none');
            return false;
        } else {
            $('#errorBranch').removeClass('showItem');
            $('#errorBranch').addClass('showItem');
            $('#errorBranch').html(' ');
            $('#' + id).css('border', '1px solid green');
            $(id).css('display', 'none');
            return true;
        }
    }
}
function validateEmail(id) {
    var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var emailID = $('#' + id).val();
    if (emailID === '') {
        $('#errorEmail').addClass('showItem');
        $('#errorEmail').html('Please enter email');
        $('#' + id).css('border', '1px solid red');
        return false;
    } else {
        if (emailID.match(mailformat)) {
            $('#errorEmail').removeClass('showItem');
            $('#errorEmail').html(' ');
            $('#' + id).css('border', '1px solid green');
            return true;
        } else {
            $('#errorEmail').addClass('showItem');
            $('#errorEmail').html('Please enter valid mail ID');
            $('#' + id).css('border', '1px solid red');
            return false;
        }
    }
}
function validateMobileNo(id) {
    var phNoFormat = /^\d{10}$/;
    var mobileNo = $('#' + id).val();
    if (mobileNo === '') {
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html('Please enter mobile no.');
        $('#' + id).css('border', '1px solid red');
        return false;
    } else {
        if (mobileNo.length == 10) {
            if (mobileNo.match(phNoFormat)) {
                $('#errorMobileNo').removeClass('showItem');
                $('#errorMobileNo').addClass('showItem');
                $('#errorMobileNo').html(' ');
                $('#' + id).css('border', '1px solid green');
                return true;
            } else {
                $('#errorMobileNo').removeClass('showItem');
                $('#errorMobileNo').addClass('showItem');
                $('#errorMobileNo').html(' ');

                $('#' + id).css('border', '1px solid green');
                $('#errorMobileNo').addClass('showItem');
                $('#errorMobileNo').html('Please enter valid mobile no.');
                $('#' + id).css('border', '1px solid red');
                return false;
            }

        } else {
            $('#errorMobileNo').removeClass('showItem');
            $('#errorMobileNo').addClass('showItem');
            $('#errorMobileNo').html('Please enter valid mobile no.');
            $('#' + id).css('border', '1px solid red');
        }

    }




}
function validateUserName(id) {
    var userName = $('#' + id).val();
    var validateName = /^[0-9]+$/;
    if (userName === '') {
        $('#errorUserName').addClass('showItem');
        $('.errorMessage').css('color', 'red');
        $('#errorUserName').html('Please enter user name');
        $('#' + id).css('border', '1px solid red');
        $(id).css('display', 'none');

        return false;
    } else {
        if (userName.match(validateName)) {
            $('#errorUserName').addClass('showItem');
            $('.errorMessage').css('color', 'red');
            $('#errorUserName').html('Please enter only alphabets');
            $('#' + id).css('border', '1px solid red');
            $(id).css('display', 'none');
            return false;
        } else {
            $('#errorUserName').removeClass('showItem');
            $('#errorUserName').addClass('showItem');
            $('#errorUserName').html(' ');
            $('#' + id).css('border', '1px solid green');
            $(id).css('display', 'none');
            return true;
        }
    }

}
function validatePassword(id) {
    var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

    var password = $('#' + id).val();
    if (password === '') {
        $('#errorPass').addClass('showItem');
        $('#errorPass').html('Please enter Password');
        $('#' + id).css('border', '1px solid red');
        return false;
    } else {
        if (password.match(regularExpression)) {
            $('#errorPass').removeClass('showItem');
            $('#errorPass').html(' ');
            $('#' + id).css('border', '1px solid green');
            return true;
        } else {
            $('#errorPass').addClass('showItem');
            $('#errorPass').html('Please enter valid Password (symbol, number, characters)  ');
            $('#' + id).css('border', '1px solid red');
            //alert("not mathed");
            return false;
        }
    }
}


//-----------------------------------------validations on Submit------------------------------------------------------------------------------------------------------------
function sendUser() {
    validateFullName('fullName');
    validateBranch('branch');
    validateEmail('email');
    validateMobileNo('mobileNo');
    validateUserName('userName');
    validatePassword('emailPass');
    if (validateFullName('fullName') && validateBranch('branch') && validateEmail('email') && validateMobileNo('mobileNo') &&
        validateUserName('userName') && validatePassword('emailPass')) {
        console.log('Okay');
        saveUser();
    }
}

function saveUser() {
    var saveEnroll = 'http://192.168.1.19:7080/website/usersave';
    // var saveEnroll = protocol + server + ':' + port + appName + '/saveEnrollment';
    var today = new Date();
    var technology = [];
    var courses;
    $.each($('#chCourseList option:selected'), function () {
        technology.push($(this).html());
        courses = technology.toString();
    });

    var fullName = $('#fullName').val();
    var role = 'Admin'
    var branch = $('#branch').val();
    var email = $('#email').val();
    var mobileNo = $('#mobileNo').val();
    var userName = $('#userName').val();
    var password = $('#emailPass').val();


    var userData = {
        fullName: fullName,
        role: role,
        branch: branch,
        email: email,
        mobileNo: mobileNo,
        userName: userName,
        password: password,



    };
    console.log('userData=>', userData);
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                var xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (exception) {
                var xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }
        } else {
            var xhr = new XMLHttpRequest();
        }
    } else {
        alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('POST', saveEnroll, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(userData));
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log('', xhr.responseText);
                var userSaveResponse = JSON.parse(xhr.responseText);
                // $('#userCodekulId_ID').text(codekulIDObj.id);
                // $('#userName_ID').text(fullName);
                console.log('got success', userSaveResponse);

                $('#formLogin')[0].reset();


                $('form#form1 :input').each(function () {
                    var input = $(this); // This is the jquery object of the input, do what you will
                    $(this).css('border', '1px solid #aaa');
                    // console.log("input", input);
                });



                document.getElementById('openMsg_model').click();
            } else {
                document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText;
                alert('Not');
                alert(document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText);
            }
        }
    };
}

