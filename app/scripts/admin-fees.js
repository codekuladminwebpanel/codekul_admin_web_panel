localStorage.pagePath = '/fees.html'
if (localStorage.userName == undefined) {
  window.location.href = 'index.html';
} else {
  // console.log('localStorage.pagePath =>', localStorage.pagePath)
}

var user;
var codekulUserID;
var codekulUserName;

var feesObj;
var getFeesPending;
feesPending();
// ------------------------------------------------- searchName -------------------------------------------------------------------------
var options = {
  url: function (phraseName) {
    return protocol + server + ':' + port + '/codekul/searchByName/' + phraseName;
  },
  getValue: function (element) {
    return element.fullName;
  },
  list: {
    onSelectItemEvent: function () {
      var value = $('#searchName').getSelectedItemData();
      user = value;
      codekulUserID = value.codekulId;
      codekulUserName = value.fullName;
      $('#studName').text(codekulUserName);
      $('#userId').text(codekulUserID);
      $('#fullname').text(codekulUserName);
      $('#fullId').text(codekulUserID);
      return value;
    }
  }
};
$('#searchName').easyAutocomplete(options);
$('#searchName').focus(function () {
  $('#searchCodekulID').val('');
});
$('#searchName').blur(function () {
  // console.log('options :', user);
  // var btnName = $('#searchName').val()
  // if (btnName === '') {
  //   console.log('emt');
  //   $(document.getElementById("btnGo")).prop('disabled', true);
  // } else {
  //   console.log('full');
  //   $(document.getElementById("btnGo")).prop('disabled', false);
  // }
});
// ------------------------------------------------- searchCodekulID -------------------------------------------------------------------------
var options = {
  url: function (phraseID) {
    return protocol + server + ':' + port + '/codekul/searchById/' + phraseID;
  },
  getValue: function (element) {
    return element.codekulId;
  },
  list: {
    onSelectItemEvent: function () {
      var valueCodekulID = $('#searchCodekulID').getSelectedItemData();
      user = valueCodekulID;
      codekulUserID = valueCodekulID.codekulId;
      codekulUserName = valueCodekulID.fullName;
      $('#studName').text(codekulUserName);;
      $('#userId').text(codekulUserID);
      $('#fullname').text(codekulUserName);
      $('#fullId').text(codekulUserID);
      return valueCodekulID;
    }
  }
};
$('#searchCodekulID').easyAutocomplete(options);
$('#searchCodekulID').focus(function () {
  $('#searchName').val('');
});
$('#searchCodekulID').on('input', function (e) {
  // $("#searchName").html("");
  // $("#goSearch").html("");
  // $("#goSearch").css("padding-bottom", "0px");
  // $("#header_fees").html("");
  // $("#feesInfo").html("");
});
$('#searchCodekulID').blur(function () {
  // console.log('options :', user);
  // var btnID = $('#searchCodekulID').val()
  // if (btnID === '') {
  //   console.log('emt');
  //   $(document.getElementById("btnGo")).prop('disabled', true);
  // } else {
  //   console.log('full');
  //   $(document.getElementById("btnGo")).prop('disabled', false);
  // }
});

// ------------------------------------ Get User Info or Serach -------------------------------------------------
function getUserInfo() {
  var sName = $('#searchName').val();
  var sCodekulID = $('#searchCodekulID').val();
  getUserInfoSearch();
}
function getUserInfoSearch() {
  var param = codekulUserID;
  var searchUrl = protocol + server + ':' + port + appName + '/getStudent/' + param;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', searchUrl, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        $('#goSearch').html('');
        $('#searchHeadingName').html('');
        $('#searchHeadingRow').html('');
        $('#searchData').html('');
        $('#dataRow').html('');
        $('#pendingTbl').html('');
        $('#goSearch').css('padding-bottom', '0px');
        $('#pendingTbl').css('padding-bottom', '0px');

        feesObj = JSON.parse(xhr.responseText);
        // console.log("feesObj", feesObj);
        var count = 1;
        var no = 1;
        var goSearchTable =
          '<div class=\'row\' style=\'padding: 15px 0px;\' id=\'searchHeadingName\'>' +
          '<div class=\'col-lg-4\'> <label style=\'font-size: 1.2rem;    color: #483fb5;\'> Name :</label> <label id=\'userName\' style=\'font-size:1rem;    text-align: center;    color: green;\' >' + user.fullName + '</label></div> ' +
          '<div class=\'col-lg-4\'> <label style=\'font-size: 1.2rem;    color: #483fb5;\'> Codekul ID :</label> <label id=\'userID\'  style=\'font-size:1rem;    text-align: center;    color: green;\'> ' + user.codekulId + '</label></div> ' +
          '<div class=\'col-lg-4\'>  </div></div> ' +
          '<div class=\'row\'id=\'searchHeadingRow\' style=\'padding: 0px;margin: 0px;\'>' +
          '<div class=\'col-lg-1 tblHeader\'   >Sr. no.</div>' +
          '<div class=\'col-lg-2 tblHeader\' > Course Name</div>' +
          '<div class=\'col-lg-2 tblHeader\' > Final Fees</div>' +
          '<div class=\'col-lg-2 tblHeader\' > Fees Paid</div>' +
          '<div class=\'col-lg-1 tblHeader\' > Pending</div>' +
          '<div class=\'col-lg-2 tblHeader\' >  </div>' +
          '<div class=\'col-lg-2 tblHeader\' >  </div>' +
          ' </div></div>' +
          '<div class=\'row\'id=\'searchData\' style=\'padding: 0px;margin: 0px;height:auto; max-height: 400px;overflow-y: auto;\'>';
        $('#goSearch').append(goSearchTable);
        $('#goSearch').css('padding-bottom', '25px');
        $.each(feesObj, function (index, fees) {
          var markup =
            '<div class=\'row \' id=\'dataRow\' style=\'width:100%;padding: 0px;margin: 0px;\'>' +
            '<div class=\'col-lg-1 \' >' + (count++) + ' </div>' +
            '<div class=\'col-lg-2 \' >' + fees.courses + '</div>' +
            '<div class=\'col-lg-2 \' >' + fees.finalFees + '</div>' +
            '<div class=\'col-lg-2 \' >' + fees.totalPaidFees + '</div>' +
            '<div class=\'col-lg-1 \' >' + fees.pendingFees + '</div>' +
            '<div class=\'col-lg-2 \' style=\'display: block; flex-wrap: wrap; word-wrap: break-word;text-align: center;\'>' + '<button class=\'btn btn-primary btn-raised\' id=\'' + fees.sId + '\' onClick=\'sendPayNow(' + fees.sId + ');\' >Pay Now  </button></div>  ' +
            '<div class=\'col-lg-2 \' style=\'display: block; flex-wrap: wrap; word-wrap: break-word;text-align: center;\'> ' +
            '<a class=\'btn btn-primary btn-raised\' id=\'sendEnqBtn\' onClick=\'clickFeesDetails(' + index + ');\' >  Details  </a></div> ' +
            '</div> </div> ';
          $('#searchData').append(markup);
          if (fees.feesStatus) {
            var id = fees.sId
            $(document.getElementById(id)).prop('disabled', true);
            // $(document.getElementById(id)).css('display', 'none');
          } else {
            // console.log('Fees Status :', (no++), fees.feesStatus);
          }
        });
      } else {
        console.log('xhr.responseText', xhr.responseText);
      }
    }
  };
}
// ------------------------------------  Pay Now  -------------------------------------------------
function validatePaymentDtl(id) {
  var PaymentDtl = $('#' + id).val();
  var validatePayDtl = /^[0-9]+$/;
  if (PaymentDtl === '') {
    $('#errorPayDtl').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorPayDtl').html('Please enter Payment Details');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (PaymentDtl.match(validatePayDtl)) {
      $('#errorPayDtl').addClass('showItem');
      $('#errorPayDtl').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      return false;
    } else {
      $('#errorPayDtl').removeClass('showItem');
      $('#errorPayDtl').addClass('showItem');
      $('#errorPayDtl').html(' ');
      $('#' + id).css('border', '1px solid green');
      return true;
    }
  }
}
function validateFeesPay(id) {
  var feesFormat = /^[0-9]+$/;
  var fees = $('#' + id).val();
  if (fees === '') {
    $('#errorFeesEx').addClass('showItem');
    $('#errorFeesEx').html('Please enter Fees.');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (fees.length <= 10) {
      if (fees.match(feesFormat)) {
        $('#errorFeesEx').removeClass('showItem');
        $('#errorFeesEx').addClass('showItem');
        $('#errorFeesEx').html(' ');
        $('#' + id).css('border', '1px solid green');
        return true;
      } else {
        $('#errorFeesEx').removeClass('showItem');
        $('#errorFeesEx').addClass('showItem');
        $('#errorFeesEx').html(' ');

        $('#' + id).css('border', '1px solid green');

        $('#errorFeesEx').addClass('showItem');
        $('#errorFeesEx').html('Please enter Fees');
        $('#' + id).css('border', '1px solid red');
        return false;
      }

    } else {
      $('#errorFees').removeClass('showItem');
      $('#errorFees').addClass('showItem');
      $('#errorFees').html('Please enter valid mobile no.');
      $('#' + id).css('border', '1px solid red');
    }
  }
}
$('input[type=radio][name=enterFeesUpdate]').change(function () {
  payment()
});
function payment() {
  var all_input = document.getElementsByClassName('check_in');
  for (var i = 0; i < all_input.length; ++i) {
    if (all_input[i].checked == true) {
      var selectedVal = all_input[i].value;
      if (selectedVal == 'Cash') {
        console.log('1');
        $('#id_PaymentModeFees').val(selectedVal)
        $('#feesAmtEx').css({
          'width': '340%'
        });
        $('#feesAmtEx').animate({
          width: '340%',
        });
        $('#idPaymentDtlEx').css('display', 'none');
      } else if (selectedVal == 'Cheque') {
        console.log('2');
        $('#id_PaymentModeFees').val(selectedVal)

        $('#feesAmtEx').animate({
          width: '100%',
        });
        $('#idPaymentDtlEx').css('display', 'block');

      } else if (selectedVal == 'NetBanking') {
        console.log('3');
        $('#id_PaymentModeFees').val(selectedVal)
        $('#feesAmtEx').animate({
          width: '100%',
        });
        $('#idPaymentDtlEx').css('display', 'block');

      } else {
        console.log('4');
        $('#idPaymentDtlEx').css('display', 'none');
        $('#feesAmtEx').css({
          'width': '340%'
        });
        $('#feesAmtEx').animate({
          width: '340%',
        });

        $('#id_PaymentModeFees').val('Cash')
        $('#id_CashEx').css('display', 'none');
        $('#id_ChequeEx').css('display', 'none');
        $('#id_OnlineEx').css('display', 'none');
      }
    }
  }
}
function sendPayNow(indx) {
  $('#feesPaidNow').modal('show');
  $('#formPayFee')[0].reset();
  // console.log('1');
  $('#feesAmtEx').css({
    'width': '340%'
  });
  $('#feesAmtEx').animate({
    width: '340%',
  });
  $('#idPaymentDtlEx').css('display', 'none');
  $('form#formPayFee :input').each(function () {
    var input = $(this);
    $(this).css('border', '1px solid #aaa');
  });
  // console.log("indx", indx);
  var paymentMod = $('#id_PaymentModeFees').val();
  // console.log("paymentMod", paymentMod);
  var Fees_Pending
  $.each(feesObj, function (index, fees) {
    if (indx == fees.sId) {
      Fees_Pending = fees.pendingFees;
    }
  });
  $('#sendFeesUpdate').unbind().click(function () {
    payment();
    if (validateFeesPay('feesAmtEx')) {
      var paidFees = $('#feesAmtEx').val();
      if (paidFees <= Fees_Pending) {
        if ($('#cheque').is(':checked') || $('#netBanking').is(':checked')) {
          if (validatePaymentDtl('idPaymentDtlEx')) {

            var updateFeesUrl = protocol + server + ':' + port + appName + '/updateFees';
            var feesId = indx;
            var today = new Date();
            var paidFees = $('#feesAmtEx').val();
            var paymentMod = $('#id_PaymentModeFees').val();
            var paymentDtl = $('#idPaymentDtlEx').val();
            var paidDate = today.getTime();

            var feesUpdate = {
              codekulId: user.codekulId,
              feesId: feesId,
              paidFees: paidFees,
              paymentMod: paymentMod,
              paymentDtl: paymentDtl,
              paidDate: paidDate,
              enteredBy: localStorage.userName,
            };
            if (window.XMLHttpRequest || window.ActiveXObject) {
              if (window.ActiveXObject) {
                try {
                  var xhr = new ActiveXObject('Msxml2.XMLHTTP');
                } catch (exception) {
                  var xhr = new ActiveXObject('Microsoft.XMLHTTP');
                }
              } else {
                var xhr = new XMLHttpRequest();
              }
            } else {
              alert('Your browser does not support XMLHTTP Request...!');
            }
            xhr.open('POST', updateFeesUrl, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify(feesUpdate));
            xhr.onreadystatechange = function () {
              if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                  var codekulIDObj = JSON.parse(xhr.responseText);

                  console.log('thisIsAnObject', codekulIDObj);

                  var w = window.open('pdf.html');
                  var Page = 'FeesUpdate'
                  localStorage.removeItem('myVariable');
                  localStorage.removeItem('myVariable1');
                  localStorage.removeItem('Page');
                  localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
                  localStorage.setItem('myVariable1', paidFees);
                  localStorage.setItem('Page', Page);

                  console.log('got success');
                  $('#formPayFee')[0].reset();
                  $('#goSearch').html('');
                  $('#goSearch').css('padding-bottom', '0px');
                  getUserInfoSearch();
                  console.log('got success', getUserInfoSearch());
                  $('#feesPaidNow').modal('hide');



                } else {
                  console.log('xhr.responseText', xhr.responseText);
                }
              }
            };
          }
        } else {
          var updateFeesUrl = protocol + server + ':' + port + appName + '/updateFees';
          var feesId = indx;
          var today = new Date();
          var paidFees = $('#feesAmtEx').val();
          var paymentMod = $('#id_PaymentModeFees').val();
          var paymentDtl = $('#idPaymentDtlEx').val();
          var paidDate = today.getTime();

          var feesUpdate = {
            codekulId: user.codekulId,
            feesId: feesId,
            paidFees: paidFees,
            paymentMod: paymentMod,
            paymentDtl: paymentDtl,
            paidDate: paidDate,
            enteredBy: localStorage.userName,

          };
          if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
              try {
                var xhr = new ActiveXObject('Msxml2.XMLHTTP');
              } catch (exception) {
                var xhr = new ActiveXObject('Microsoft.XMLHTTP');
              }
            } else {
              var xhr = new XMLHttpRequest();
            }
          } else {
            alert('Your browser does not support XMLHTTP Request...!');
          }
          xhr.open('POST', updateFeesUrl, true);
          xhr.setRequestHeader('Content-Type', 'application/json');
          xhr.send(JSON.stringify(feesUpdate));
          xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
              if (xhr.status == 200) {
                var codekulIDObj = JSON.parse(xhr.responseText);

                console.log('thisIsAnObject', codekulIDObj);

                var w = window.open('pdf.html');
                var Page = 'FeesUpdate'
                localStorage.removeItem('myVariable');
                localStorage.removeItem('myVariable1');
                localStorage.removeItem('Page');
                localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
                localStorage.setItem('myVariable1', paidFees);
                localStorage.setItem('Page', Page);


                console.log('got success');
                $('#formPayFee')[0].reset();
                $('#goSearch').html('');
                $('#goSearch').css('padding-bottom', '0px');
                getUserInfoSearch();
                // $('#openModel_payNow').click();
                $('#feesPaidNow').modal('hide');
                // window.location.href = "pdf.html";
              } else {
                console.log('xhr.responseText', xhr.responseText);
              }
            }
          };
        }
      } else {
        alert('Paid fees Grater than Pending fees');
      }
    } else { }
  });
}

var feesUpdateObjList;
// ------------------------------------ Fees Details -------------------------------------------------
function clickFeesDetails(ind) {
  var index = feesObj[ind];
  $('#modal-feesDetails').modal('show');
  $(document).keyup(function (e) {
    if (e.keyCode == 27) { // esc keycode
      $('#modal-feesDetails').modal('hide');
    }
  });
  var getFeesUpdatesUrl = protocol + server + ':' + port + appName + '/getFeesUpdates/' + index.sId;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getFeesUpdatesUrl, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var feesUpdateObj = JSON.parse(xhr.responseText);
        // console.log("feesUpdateObj.List : ", feesUpdateObj.List);
        // console.log("feesUpdateObj.updates : ", feesUpdateObj.updates);
        // console.log("feesUpdateObj.updates.paidFees : ", feesUpdateObj.updates.paidFees);

        $('#TotalCoursesFees').text(feesUpdateObj.updates.totalFees);
        $('#FinalCoursesFees').text(feesUpdateObj.updates.finalFees);
        $('#TotalPaidFees').text(feesUpdateObj.updates.paidFees);
        $('#TotalPendingFees').text(feesUpdateObj.updates.pendingFees);
        var count = 1;
        var table =
          '<div class=\'row\'  style=\'border-bottom: 1px solid;margin-bottom: 5px;padding: 5px 10px;     font-weight: 600;\'>' +
          '<div class=\'col-lg-1\'> No.</div>' +
          '<div class=\'col-lg-2\'> Paid Fees</div>' +
          '<div class=\'col-lg-2\'> Payment Mode  </div>' +
          '<div class=\'col-lg-3\'> Payment Details  </div>' +
          '<div class=\'col-lg-2\'> Fees Paid Date </div>' +
          '<div class=\'col-lg-2\'> </div></div>' +
          '<div class=\'row\' id=\'feesInfo\' style=\'height: auto; max-height:255px;overflow-y: auto;width: 100%;text-align: left;padding: 0px;margin: 0px;flex-wrap: wrap;overflow-x: hidden;\'>'
        $('#header_fees').append(table);
        feesUpdateObjList = feesUpdateObj.List;
        $.each(feesUpdateObjList, function (index, feesNew) {
          // console.log("paymentStatus:", feesNew.paymentStatus)
          var date = new Date(feesNew.paidDate);
          var todayTime = new Date(date.toString());
          var month = todayTime.getMonth() + 1;
          var day = todayTime.getDate();
          var year = todayTime.getFullYear();
          var feesPaidDate = day + '/' + month + '/' + year;
          var feesData =
            '<div class=\'row\'  style=\'height: auto; max-height:255px;overflow-y: auto;width: 100%;text-align: left;padding: 5px 0px;margin: 0px;flex-wrap: wrap;overflow-x: hidden;    border-bottom: 1px solid #aaa;\'> ' +
            '<div class=\'col-lg-1\'>' + (count++) + '</div>' +
            '<div class=\'col-lg-2\'>' + feesNew.paidFees + '</div>' +
            '<div class=\'col-lg-2\'>' + feesNew.paymentMod + '</div>' +
            '<div class=\'col-lg-3\'>' + feesNew.paymentDtl + '</div>' +
            '<div class=\'col-lg-2\'>' + feesPaidDate + '</div>  ' +
            '<div class=\'col-lg-2\' >' +
            ' <a class=\'btn btn-primary btn-raised\' id=\'idBtnAdd' + index + '\' onClick=\'clickFeesReceived(' + feesNew.sId + ');\' > Received  </a>' +
            '</div>' +
            ' </div></div>';
          $('#feesInfo').append(feesData);
          if (feesNew.paymentStatus) {
            $('#idBtnAdd' + index).css('display', 'none');
          } else {
            $('#idBtnAdd' + index).css('display', 'block');
          }
        });
      } else {
        console.log('xhr.status', xhr.status);
        // document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText;
        // alert('Not');
      }
    }
  };
}
function clickFeesReceived(id) {
  // console.log("feesUpdateObjList id :", feesUpdateObjList[id]);
  // console.log("feesUpdateObjList id :", id);
  // console.log("feesUpdateObjList id :", id);
  // console.log("feesUpdateObjList id :", id);
  // $("#idBtnAdd" + id).css("display", "none");
  // console.log(" ", $("#idBtnAdd" + id));
  var feesStatusURL = protocol + server + ':' + port + appName + '/updatePaymentStatus/' + id;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', feesStatusURL, true);
  xhr.send();
  // console.log("feesStatusURL", feesStatusURL);
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var feesReceivedStatus = JSON.parse(xhr.responseText);
        // console.log("feesReceivedStatus :", feesReceivedStatus);
        getUserInfo();
        document.getElementById('openFeeDetails').click();
      } else {
        console.log('xhr.status ', xhr.status);
        // document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText;
        // alert('Not');
      }
    }
  };
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------
function myFunction(event) {
  var key = window.event ? event.keyCode : event.which;
  if (event.keyCode == 9) {
    return true;
    //  document.querySelector('.multiselect').onfocus = function() {
    //      $('.btn-group').addClass('open');
    //  };
  }
}
function validateOnBlur(functionToCall) {
  functionToCall;
  console.log('Function to call ' + functionToCall);
}
function removeErrorOnFocus(id, borderId) {
  $(id).removeClass('showItem');
  $('#' + borderId).css('border', '1px solid blue');
}
function onlyNumbersAllowed(event) {
  var key = window.event ? event.keyCode : event.which;
  if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 37 ||
    event.keyCode == 39) {
    return true;
  } else if (key < 48 || key > 57) {
    return false;
  } else
    return true;
}
function specialCharNotAllowed(e) {
  var specialKeys = new Array();
  specialKeys.push(8); //Backspace
  specialKeys.push(9); //Tab
  specialKeys.push(46); //Delete
  specialKeys.push(36); //Home
  specialKeys.push(35); //End
  specialKeys.push(37); //Left
  specialKeys.push(39); //Right
  var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
  if (keyCode == 32) {
    // console.log('found space');
  }
  var ret = ((keyCode <= 48 && keyCode >= 57) ||
    (keyCode >= 65 && keyCode <= 90) ||
    (keyCode = 32) ||
    (keyCode >= 97 && keyCode <= 122)
    //                        || (47 < keyCode && keyCode < 58) || (keyCode = 45)
    ||
    (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
  return ret;
}
//------------------------------------------------------------ pending Fees ------------------------------------------------------------//

function feesPending() {
  $('#searchName').css('border-color', '#aaa');
  $('#searchCodekulID').css('border-color', '#aaa');

  var getFeesPendingUrl = protocol + server + ':' + port + appName + '/getFeesPending';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getFeesPendingUrl, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        $('#searchCodekulID').val('');
        $('#searchName').val('');
        $('#goSearch').html('');
        $('#goSearch').css('padding-bottom', '0px');
        $('#pendingTbl').html('');
        $('#rowData').html('');
        $('#pendingTbl').css('padding-bottom', '0px');

        getFeesPending = JSON.parse(xhr.responseText);
        // console.log("getFeesPending", getFeesPending.totalPending);
        var getPendingList = getFeesPending.pendingList;
        $('#studName').text(codekulUserName);
        $('#userId').text(codekulUserID);
        var count = 1;
        var no = 1;
        var Course;
        var goSearchTable =
          '<div class=\'row\'id=\'headingData\' style=\'padding: 0px;margin: 0px;\'>' +
          '<div class=\'col-lg-2 tblHeader\'style=\' padding:10px; \'> Codekul ID</div>' +
          '<div class=\'col-lg-2 tblHeader\'style=\' padding:10px; \'onClick=\'sortName();\' > ' +
          '<p> Student Name' + '<i class="fa fa-sort" aria-hidden="true" id="srNm"></i> </p>' +
          '</div>' +
          '<div class=\'col-lg-2 tblHeader  \'style=\' padding:10px; \'> <p> Course Name </p> </div>' +
          '<div class=\'col-lg-1 tblHeader\'style=\'padding-left: 0px; padding-right: 0px; \' onClick=\'sortDate();\'> ' +
          '<p> Enroll Date' + '<i class="fa fa-sort" aria-hidden="true" id="srDt"></i> </p>' +
          '</div>' +
          '<div class=\'col-lg-1 tblHeader\'style=\'text-align: center; padding-left: 0px;    padding-right: 0px;\'> Paid  </div>' +
          '<div class=\'col-lg-1 tblHeader\'style=\'text-align: center; padding-left: 0px;    padding-right: 0px \' onClick=\'sortPending();\'>  ' +
          '<p> Pending   ' + '<i class="fa fa-sort" aria-hidden="true" id="srPe"></i>' +
          '<span id="show" ></span> ' +
          // '<span id="hide" ></span> </p>' +
          '</div>' +
          '<div class=\'col-lg-1 tblHeader\'style=\'text-align: center;    padding-left: 0px;    padding-right: 0px;\'> Total   </div>' +
          '<div class=\'col-lg-2 tblHeader\'> <b id="id_count" style="text-align: center;background-color: #4e8910;padding: 10px;border-radius: 50%;color: #fff;margin: 10px auto;border-radius: 4px;margin: 10px 50px; "> </b>    </div>' +
          '</div>' +
          '<div class=\'row\'id=\'searchData\' style=\'padding: 0px;margin: 0px;height:auto; max-height: 400px;overflow-y: auto;\'>';
        $('#pendingTbl').append(goSearchTable);
        $('#pendingTbl').css('padding-bottom', '25px');
        $('#id_count').dblclick(function () {
          // console.log("Hello", getFeesPending.totalPending);
          $('#totalPendingID').text(getFeesPending.totalPending);
          $('#modal-5').modal('show');
        });
        $.each(getPendingList, function (index, FeesPending) {
          var totalCount = no++;
          $('#id_count').text(totalCount);
          var date = new Date(FeesPending.enroll_date);
          var todayTime = new Date(date.toString());
          var month = todayTime.getMonth() + 1;
          var day = todayTime.getDate();
          var year = todayTime.getFullYear();
          var feesPaidDate = day + '/' + month + '/' + year;
          var markup =
            '<div class=\'row \' id=\'rowData\' style=\'width:100%; padding: 0px;margin: 0px; \'>' +
            '<div class=\'col-lg-2 \' >' + FeesPending.codekul_id + ' </div>' +
            '<div class=\'col-lg-2 \' >' + FeesPending.name + '</div>' +
            '<div class=\'col-lg-2 \' style=\'display: block;    flex-wrap: wrap;    word-wrap: break-word;\' >' + FeesPending.courses + '</div>' +
            '<div class=\'col-lg-1 \' >' + feesPaidDate + '</div>' +
            '<div class=\'col-lg-1 \' style=\'text-align: right;\'>' + FeesPending.total_paid_fees + '</div>' +
            '<div class=\'col-lg-1 \' style=\'text-align: right;\'>' + FeesPending.pending_fees + '</div>' +
            '<div class=\'col-lg-1 \' style=\'text-align: right;\'>' + FeesPending.final_fees + '</div>' +
            '<div class=\'col-lg-2 \' style=\'text-align: center;\'>' +
            '<button class=\'btn btn-primary btn-raised\' id=\'' + FeesPending.s_id + '\' onClick=\'sendPayNowPending(' + FeesPending.s_id + ');\'>Pay Now  </button></div>  ' +
            '</div></div> ';
          $('#searchData').append(markup);
        });
      } else {
        console.log('xhr.responseText', xhr.responseText);
      }
    }
  };
}
function feesPendingSort(order) {
  $('#searchData').html('');
  // console.log("order", order);
  var pendingFeesSortUrl = protocol + server + ':' + port + appName + '/getFeesPending/' + order;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', pendingFeesSortUrl, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        getFeesPending = JSON.parse(xhr.responseText);
        var getPendingList = getFeesPending.pendingList;
        $('#studName').text(codekulUserName);
        $('#userId').text(codekulUserID);
        var count = 1;
        var no = 1;
        var Course;
        $.each(getPendingList, function (index, FeesPending) {
          var date = new Date(FeesPending.enroll_date);
          var todayTime = new Date(date.toString());
          var month = todayTime.getMonth() + 1;
          var day = todayTime.getDate();
          var year = todayTime.getFullYear();
          var feesPaidDate = day + '/' + month + '/' + year;
          var markup =
            '<div class=\'row \' id=\'rowData\' style=\'width:100%; padding: 0px;margin: 0px; \'>' +
            '<div class=\'col-lg-2 \' >' + FeesPending.codekul_id + ' </div>' +
            '<div class=\'col-lg-2 \' >' + FeesPending.name + '</div>' +
            '<div class=\'col-lg-2 \' style=\'display: block;    flex-wrap: wrap;    word-wrap: break-word;\' >' + FeesPending.courses + '</div>' +
            '<div class=\'col-lg-1 \'  >' + feesPaidDate + '</div>' +
            '<div class=\'col-lg-1 \' style=\'text-align: right;\'  >' + FeesPending.total_paid_fees + '</div>' +
            '<div class=\'col-lg-1 \' style=\'text-align: right;\'  >' + FeesPending.pending_fees + '</div>' +
            '<div class=\'col-lg-1 \' style=\'text-align: right;\'   >' + FeesPending.final_fees + '</div>' +
            '<div class=\'col-lg-2 \' style=\'text-align: center;\'>' +
            '<button class=\'btn btn-primary btn-raised\' id=\'' + FeesPending.s_id + '\' onClick=\'sendPayNowPending(' + FeesPending.s_id + ');\'   >Pay Now  </button></div>  ' +
            '</div> ';
          $('#searchData').append(markup);


        });
      } else {
        console.log('xhr.responseText', xhr.responseText);

      }
    }
  };
}
function sendPayNowPending(indx) {
  // console.log("indx", indx);
  $('#feesPaidNow').modal('show');
  $('#formPayFee')[0].reset();
  // console.log('1');
  $('#feesAmtEx').css({
    'width': '340%'
  });
  $('#feesAmtEx').animate({
    width: '340%',
  });
  $('#idPaymentDtlEx').css('display', 'none');
  $('form#formPayFee :input').each(function () {
    var input = $(this);
    $(this).css('border', '1px solid #aaa');
  });
  var getPendingListIndex = getFeesPending.pendingList;
  $.each(getPendingListIndex, function (index, ListPending) {
    if (indx == ListPending.s_id) {
      // console.log("getFeesPending", ListPending.name);
      $('#studName').text('');
      $('#userId').text('');
      $('#studName').text(ListPending.name);
      $('#userId').text(ListPending.codekul_id);
      $('#model_feesUpdate').html('');
      $('#sendFeesUpdate').unbind().click(function () {
        validateFeesPay('feesAmtEx');
        payment();
        var paidFees = parseInt($('#feesAmtEx').val());
        var chPendingFees = parseInt(ListPending.pending_fees)
        if (paidFees <= chPendingFees) {
          if ($('#cheque').is(':checked') || $('#netBanking').is(':checked')) {
            console.log('Chq or Net ==========>')
            if (validatePaymentDtl('idPaymentDtlEx')) {
              console.log('Okay');
              var newUrl = protocol + server + ':' + port + appName + '/updateFees';
              var feesId = indx;
              var codekulId = ListPending.codekul_id;
              var today = new Date();
              var paidFees = $('#feesAmtEx').val();
              var paymentMod = $('#id_PaymentModeFees').val();
              var paymentDtl = $('#idPaymentDtlEx').val();
              var paidDate = today.getTime();
              var feesUpdate = {
                codekulId: codekulId,
                feesId: feesId,
                paidFees: paidFees,
                paymentMod: paymentMod,
                paymentDtl: paymentDtl,
                paidDate: paidDate,
                enteredBy: localStorage.userName
              };
              console.log('feesUpdate :', feesUpdate);
              document.getElementById('req_text').innerHTML = 'Process Started...';
              if (window.XMLHttpRequest || window.ActiveXObject) {
                if (window.ActiveXObject) {
                  try {
                    var xhr = new ActiveXObject('Msxml2.XMLHTTP');
                  } catch (exception) {
                    var xhr = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                } else {
                  var xhr = new XMLHttpRequest();
                }
              } else {
                alert('Your browser does not support XMLHTTP Request...!');
              }
              xhr.open('POST', newUrl, true);
              xhr.setRequestHeader('Content-Type', 'application/json');
              xhr.send(JSON.stringify(feesUpdate));
              xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                  if (xhr.status == 200) {

                    var codekulIDObj = JSON.parse(xhr.responseText);

                    console.log('thisIsAnObject', codekulIDObj);

                    var w = window.open('pdf.html');
                    var Page = 'FeesUpdate'
                    localStorage.removeItem('myVariable');
                    localStorage.removeItem('myVariable1');
                    localStorage.removeItem('Page');
                    localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
                    localStorage.setItem('myVariable1', paidFees);
                    localStorage.setItem('Page', Page);



                    console.log('got success');
                    $('#formPayFee')[0].reset();
                    $('#goSearch').html('');
                    $('#goSearch').css('padding-bottom', '0px');
                    $('#feesPaidNow').modal('hide');

                    feesPending();

                    // window.location.href = "pdf.html";

                  } else {
                    console.log('xhr.responseText', xhr.responseText);
                  }
                }
              };
            }
          } else {
            console.log('Cash ==========>')
            var newUrl = protocol + server + ':' + port + appName + '/updateFees';
            var feesId = indx;
            var codekulId = ListPending.codekul_id;
            var today = new Date();
            var paidFees = $('#feesAmtEx').val();
            var paymentMod = $('#id_PaymentModeFees').val();
            var paymentDtl = $('#idPaymentDtlEx').val();
            var paidDate = today.getTime();
            var feesUpdate = {
              codekulId: codekulId,
              feesId: feesId,
              paidFees: paidFees,
              paymentMod: paymentMod,
              paymentDtl: paymentDtl,
              paidDate: paidDate,
              enteredBy: localStorage.userName,
            };
            console.log('feesUpdate :', feesUpdate);
            document.getElementById('req_text').innerHTML = 'Process Started...';
            if (window.XMLHttpRequest || window.ActiveXObject) {
              if (window.ActiveXObject) {
                try {
                  var xhr = new ActiveXObject('Msxml2.XMLHTTP');
                } catch (exception) {
                  var xhr = new ActiveXObject('Microsoft.XMLHTTP');
                }
              } else {
                var xhr = new XMLHttpRequest();
              }
            } else {
              alert('Your browser does not support XMLHTTP Request...!');
            }
            xhr.open('POST', newUrl, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify(feesUpdate));
            xhr.onreadystatechange = function () {
              if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                  var codekulIDObj = JSON.parse(xhr.responseText);

                  console.log('thisIsAnObject', codekulIDObj);

                  var w = window.open('pdf.html');
                  var Page = 'FeesUpdate'
                  localStorage.removeItem('myVariable');
                  localStorage.removeItem('myVariable1');
                  localStorage.removeItem('Page');
                  localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
                  localStorage.setItem('myVariable1', paidFees);
                  localStorage.setItem('Page', Page);


                  console.log('got success');
                  $('#formPayFee')[0].reset();
                  $('#goSearch').html('');
                  $('#goSearch').css('padding-bottom', '0px');

                  $('#feesPaidNow').modal('hide');
                  feesPending();
                  // window.location.href = "pdf.html";

                } else {
                  console.log('xhr.responseText', xhr.responseText);
                }
              }
            };
          }
        } else {
          alert('Paid fees Grater than Pending fees');
        }
      });
    }
  });
}
function sortName() {
  $('span#show').toggle();
  var isVisible = $('#show').is(':visible');
  if (isVisible) {
    $('#srNm').addClass('fa-sort-asc');
    $('#srNm').removeClass('fa-sort-desc');
    feesPendingSort('nameAsc');
  } else {
    $('#srNm').addClass('fa-sort-desc');
    $('#srNm').removeClass('fa-sort-asc');
    feesPendingSort('nameDesc');
  }
}
function sortDate() {
  $('span#show').toggle();
  var isVisible = $('#show').is(':visible');
  if (isVisible) {
    $('#srDt').addClass('fa-sort-asc');
    $('#srDt').removeClass('fa-sort-desc');

    feesPendingSort('dateAsc');
  } else {
    $('#srDt').addClass('fa-sort-desc');
    $('#srDt').removeClass('fa-sort-asc');
    feesPendingSort('dateDesc');
  }
}
function sortPending() {
  $('span#show').toggle();
  var isVisible = $('#show').is(':visible');
  if (isVisible) {
    $('#srPe').addClass('fa-sort-asc');
    $('#srPe').removeClass('fa-sort-desc');
    console.log('Hi');
    feesPendingSort('pendingAsc');
  } else {
    $('#srPe').addClass('fa-sort-desc');
    $('#srPe').removeClass('fa-sort-asc');
    console.log('Hello');
    feesPendingSort('pendingDesc');
  }
}

// ------------------------------------ Modal Closed -------------------------------------------------
$('#modal-payNow').on('hidden.bs.modal', function (e) {
  $('#header_fees').html('');
  $('#feesInfo').html('');
  $('#formPayFee')[0].reset();


})
$('#exampleModal').on('hidden.bs.modal', function (e) {

  $('#formPayFee')[0].reset();

  $('#feesAmtEx').css({
    'width': '340%',
    'border': '1px solid #aaa'
  });
  $('#idPaymentDtlEx').css('display', 'none');
})
$('#modal-feesDetails').on('hidden.bs.modal', function (e) {
  $('#header_fees').html('');
  $('#feesInfo').html('');

})
