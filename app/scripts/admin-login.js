function validateEmail(id) {
  var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var emailID = $('#' + id).val();
  if (emailID === '') {
    $('#errorEmail').addClass('showItem');
    $('#errorEmail').html('Please enter email');
    $('#' + id).css('border-bottom', '2px solid red');
    return false;
  } else {
    if (emailID.match(mailformat)) {
      $('#errorEmail').removeClass('showItem');
      $('#errorEmail').html(' ');
      $('#' + id).css('border-bottom', '2px solid green');
      return true;
    } else {
      $('#errorEmail').addClass('showItem');
      $('#errorEmail').html('Please enter valid mail ID');
      $('#' + id).css('border-bottom', '2px solid red');
      return false;
    }
  }
}
function validatePassword(id) {
  // var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  var regularExpression = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,16}$/;
  var password = $('#' + id).val();
  if (password === '') {
    $('#errorPass').addClass('showItem');
    $('#errorPass').html('Please enter Password');
    $('#' + id).css('border-bottom', '2px solid red');
    return false;
  } else {
    if (!password.match(regularExpression)) {
      $('#errorPass').removeClass('showItem');
      $('#errorPass').html(' ');
      $('#' + id).css('border-bottom', '2px solid green');
      return true;
    } else {
      $('#errorPass').addClass('showItem');
      $('#errorPass').html(' ');
      // $('#errorPass').html('Please enter valid Password (at least one number )');
      $('#' + id).css('border-bottom', '2px solid green');
      return true;
    }
  }
}

function removeErrorOnFocus(id, borderId) {
  $(id).removeClass('showItem');
  $('#' + borderId).css('border-bottom', '2px solid #3f51b5');
}
$('input').keypress(function (e) {
  if (e.which == 13) {
    login();
  }
});
$('#loginClick').click(function () {
  validateEmail('email');
  validatePassword('emailPass');
  if (validateEmail('email') && validatePassword('emailPass')) {
    login();
  } else {
    // login();
  }
});
// dummy code
$('#loginClick1').click(function () {
  validateEmail('email');
  validatePassword('emailPass');
  if (validateEmail('email') && validatePassword('emailPass')) {
    login1();
  } else {
    // login();
  }
});


function login() {
  var email = $('#email').val();
  var password = $('#emailPass').val();

       
        localStorage.userName = 'Nikhil';
        localStorage.userRole = 'admin';
        localStorage.userEmail = 'nikhil@gmail.com';

        console.log('localStorage.pagePath', localStorage.pagePath);

        if (localStorage.pagePath == '/index.html') {
          window.location.href = 'home.html';
        } else if (localStorage.pagePath == undefined) {
          window.location.href = 'home.html';
        } else {
          window.location.href = localStorage.pagePath;
        }

}
// dummy code
function login() {
  var email = $('#email').val();
  var password = $('#emailPass').val();
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  var loginAdminURL = protocol + server + ':' + port + appName + '/login?userName=' + email + '&password=' + password;
  var today = new Date();
  xhr.open('POST', loginAdminURL, true);
  console.log('loginAdminURL', loginAdminURL);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var loginStatus = JSON.parse(xhr.responseText);
        localStorage.userName = loginStatus.status.name;
        localStorage.userRole = loginStatus.status.role;
        localStorage.userEmail = loginStatus.status.userName;

        console.log('localStorage.pagePath', localStorage.pagePath);

        if (localStorage.pagePath == '/index.html') {
          window.location.href = 'home.html';
        } else if (localStorage.pagePath == undefined) {
          window.location.href = 'home.html';
        } else {
          window.location.href = localStorage.pagePath;
        }


      } else {
        console.log('Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText);
        $('#modal-1').modal('show');
      }
    }
  };
}


