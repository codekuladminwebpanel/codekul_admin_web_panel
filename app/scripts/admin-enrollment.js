localStorage.pagePath = '/enroll.html';
if (localStorage.userName == undefined) {
  window.location.href = 'index.html';
} else {
  console.log('localStorage.pagePath =>', localStorage.pagePath)
}


$(document).ready(function () {
  $('#chCourseList').multiselect({
    maxHeight: 150,
    buttonWidth: '100%',
    nonSelectedText: 'Please select technology',
    onChange: function (option, checked, select) {
      var course = courseList[$(option).val()];
      if (checked) {
        var chOpt = course.fees;
        var Ch = parseInt(chOpt);
        console.log('feesCourses : ', Ch);
        Add(Ch);
      } else {
        var unchOpt = course.fees;
        var Unch = parseInt(unchOpt);
        Sub(Unch);
      }
    },
    onDropdownHidden: function (event) {

      $('#feesCourses').val($('#autoFees').val());

      var am = $('#feesCourses').val();
      console.log('feesCourses : ', am);
      var ta = 0;
      var total = (am * ta) / 100;

      $('#idServicesTax').val(total);

      console.log('Tax', total);
      var totalFees = parseInt(total) + parseInt(am)
      $('#totalFees').val(totalFees);
      $('#finalFees').val(totalFees);

      console.log('totalFees : ', totalFees);
    }
  });
  $('#chCourseListEx').multiselect({
    maxHeight: 150,
    buttonWidth: '100%',
    nonSelectedText: 'Please select technology',
    onChange: function (option, checked, select) {
      var course = courseList[$(option).val()];
      if (checked) {
        var chOpt = course.fees;
        var Ch = parseInt(chOpt);
        myAdd(Ch);
      } else {
        var unchOpt = course.fees;
        var Unch = parseInt(unchOpt);
        mySub(Unch);
      }
    },
    onDropdownHidden: function (event) {
      $('#feesCoursesEx').val($('#autoFeesEx').val());
      var am = $('#feesCoursesEx').val();
      var ta = 0;
      var total = (am * ta) / 100;
      $('#idServicesTaxEx').val(total);
      var totalFees = parseInt(total) + parseInt(am)
      $('#totalFeesEx').val(totalFees);
      $('#finalFeesEx').val(totalFees);
    }
  });
  var getCoursesUrl = protocol + server + ':' + port + appName + '/allCourses';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getCoursesUrl, true);
  // console.log("getCoursesUrl", getCoursesUrl);
  xhr.send();
  var courseList;
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        courseList = JSON.parse(xhr.responseText);
        // console.log("courseList", courseList);
        var inHTML = '';
        $.each(courseList, function (index, value) {
          // console.log("courseList", courseList);
          var val = value.courseName;
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseList').append(htm);
          $('#chCourseList').multiselect('rebuild');
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseListEx').append(htm);
          $('#chCourseListEx').multiselect('rebuild');
        });
      } else {
        console.log('start web services', document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText)
        alert('Not');
      }
    }
  };
});
//---------------------------------- validaions -------------------------------------------------------------------------------------------------------------------
function validateFname(id) {
  var FName = $('#' + id).val();
  var validateName = /^[0-9]+$/;
  if (FName === '') {
    $('#errorFName').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFName').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');

    return false;
  } else {
    if (FName.match(validateName)) {
      $('#errorFName').addClass('showItem');
      $('.errorMessage').css('color', 'red');
      $('#errorFName').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      $(id).css('display', 'none');
      return false;
    } else {
      $('#errorFName').removeClass('showItem');
      $('#errorFName').addClass('showItem');
      $('#errorFName').html(' ');
      $('#' + id).css('border', '1px solid green');
      $(id).css('display', 'none');
      return true;
    }
  }
}
// function validateMname(id) {
//   var MName = $('#' + id).val();
//   var validateName = /^[0-9]+$/;
//   if (MName === '') {
//     $('#errorMName').addClass('showItem');
//     $('.errorMessage').css('color', 'red');
//     $('#errorMName').html('Please enter name');
//     $('#' + id).css('border', '1px solid red');
//     $(id).css('display', 'none');
//     return false;
//   } else {
//     if (MName.match(validateName)) {
//       $('#errorMName').addClass('showItem');
//       $('.errorMessage').css('color', 'red');
//       $('#errorMName').html('Please enter only alphabets');
//       $('#' + id).css('border', '1px solid red');
//       $(id).css('display', 'none');
//       return false;
//     } else {
//       $('#errorMName').removeClass('showItem');
//       $('#errorMName').addClass('showItem');
//       $('#errorMName').html(' ');
//       $('#' + id).css('border', '1px solid green');
//       $(id).css('display', 'none');
//       return true;
//     }
//   }
// }
function validateLname(id) {
  var LName = $('#' + id).val();
  var validateName = /^[0-9]+$/;
  if (LName === '') {
    $('#errorLName').addClass('showItem');
    $('#errorLName').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (LName.match(validateName)) {
      $('#errorLName').addClass('showItem');
      $('#errorLName').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      return false;
    } else {
      $('#errorLName').removeClass('showItem');
      $('#errorLName').addClass('showItem');
      $('#errorLName').html(' ');
      $('#' + id).css('border', '1px solid green');
      return true;
    }
  }
}
function validateGender() {
  var gander;
  if ($('input[name=Gender]:checked').length <= 0) {
    alert('No radio checked')
    return false;
  } else {
    if ($('#male').prop('checked') == true) {
      gander = 'male';
      $('#id_Gander').val(gander)
      return true;
    }
    if ($('#female').prop('checked') == true) {
      gander = 'female';
      $('#id_Gander').val(gander)
      return true;
    }
  }

}
function validateEmail(id) {
  var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var emailID = $('#' + id).val();
  if (emailID === '') {
    $('#errorEmail').addClass('showItem');
    $('#errorEmail').html('Please enter email');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (emailID.match(mailformat)) {
      $('#errorEmail').removeClass('showItem');
      $('#errorEmail').html(' ');
      $('#' + id).css('border', '1px solid green');
      return true;
    } else {
      $('#errorEmail').addClass('showItem');
      $('#errorEmail').html('Please enter valid mail ID');
      $('#' + id).css('border', '1px solid red');
      return false;
    }
  }
}
function validatePassword(id) {
  var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

  var password = $('#' + id).val();
  if (password === '') {
    $('#errorPass').addClass('showItem');
    $('#errorPass').html('Please enter Password');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (password.match(regularExpression)) {
      $('#errorPass').removeClass('showItem');
      $('#errorPass').html(' ');
      $('#' + id).css('border', '1px solid green');
      return true;
    } else {
      $('#errorPass').addClass('showItem');
      $('#errorPass').html('Please enter valid Password (symbol, number, characters)  ');
      $('#' + id).css('border', '1px solid red');
      //alert("not mathed");
      return false;
    }
  }
}
function validateMobileNo(id) {
  var phNoFormat = /^\d{10}$/;
  var mobileNo = $('#' + id).val();
  if (mobileNo === '') {
    $('#errorMobileNo').addClass('showItem');
    $('#errorMobileNo').html('Please enter mobile no.');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (mobileNo.length == 10) {
      if (mobileNo.match(phNoFormat)) {
        $('#errorMobileNo').removeClass('showItem');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html(' ');
        $('#' + id).css('border', '1px solid green');
        return true;
      } else {
        $('#errorMobileNo').removeClass('showItem');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html(' ');

        $('#' + id).css('border', '1px solid green');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html('Please enter valid mobile no.');
        $('#' + id).css('border', '1px solid red');
        return false;
      }

    } else {
      $('#errorMobileNo').removeClass('showItem');
      $('#errorMobileNo').addClass('showItem');
      $('#errorMobileNo').html('Please enter valid mobile no.');
      $('#' + id).css('border', '1px solid red');
    }

  }




}
// function validateCity(id) {
//   var city = $('#' + id).val();
//   var validateName = /^[0-9]+$/;
//   if (city === '') {
//     $('#errorCity').addClass('showItem');
//     $('.errorMessage').css('color', 'red');
//     $('#errorCity').html('Please enter city');
//     $('#' + id).css('border', '1px solid red');
//     return false;
//   } else {
//     if (city.match(validateName)) {
//       $('#errorCity').addClass('showItem');
//       $('#errorCity').html('Please enter only alphabets');
//       $('#' + id).css('border', '1px solid red');
//       return false;
//     } else {
//       $('#errorCity').removeClass('showItem');
//       $('#errorCity').addClass('showItem');
//       $('#errorCity').html(' ');
//       $('#' + id).css('border', '1px solid green');
//       return true;
//     }
//   }
// }
function validateTechnology() {
  var technology = [];
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
  });

  if (technology.length === 0) {

    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.tech').css('border', '1px solid red');

    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.tech').css('border', '1px solid green');
    // $('.ms-options-wrap').css('border-radius', '4px');
    return true;
  }

}
function validateFinalFees(id) {

  var feesFormat = /^[0-9]+$/;
  var fees = $('#' + id).val();
  if (fees === '') {
    $('#errorFinalFees').addClass('showItem');
    $('#errorFinalFees').html('Please enter Fees.');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    // var TotalFees = parseInt($('#totalFees ').val());
    // var PaidFees = parseInt($('#feesAmt ').val());
    // var PendingFees = TotalFees - PaidFees;
    // if (PaidFees <= TotalFees) {
    //   $('#errorFinalFees').removeClass('showItem');
    //   $('#errorFinalFees').addClass('showItem');
    //   $('#' + id).css('border', '1px solid green');
    //   $('#errorFinalFees').html('Pending  Fees : ' + PendingFees);
    //   return true;
    // } else {
    //   $('#errorFinalFees').removeClass('showItem');
    //   $('#errorFinalFees').addClass('showItem');
    //   $('#errorFinalFees').html('Please Check total Fees.');
    //   $('#' + id).css('border', '1px solid red');
    // }

  }




}
function validateFees(id) {
  var feesFormat = /^[0-9]+$/;
  var fees = $('#' + id).val();
  if (fees === '') {
    $('#errorFees').addClass('showItem');
    $('#errorFees').html('Please enter Fees.');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    var FinalFees = parseInt($('#finalFees ').val());
    var TotalFees = parseInt($('#totalFees ').val());
    var PaidFees = parseInt($('#feesAmt ').val());
    var PendingFees = FinalFees - PaidFees;
    console.log('PendingFees', PendingFees);
    if (PaidFees <= FinalFees) {
      $('#errorFees').removeClass('showItem');
      $('#errorFees').addClass('showItem');
      $('#' + id).css('border', '1px solid green');
      $('#errorFees').html('Pending  Fees : ' + PendingFees);
      return true;
    } else {
      $('#errorFees').removeClass('showItem');
      $('#errorFees').addClass('showItem');
      $('#errorFees').html('Please Check total Fees.');
      $('#' + id).css('border', '1px solid red');
    }
  }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------
$('input[type=radio][name=enterFeesType]').change(function () {
  validatePaymentMode()
});
function validatePaymentDtl(id) {
  var PaymentDtl = $('#' + id).val();
  var validatePayDtl = /^[0-9]+$/;


  if (PaymentDtl === '') {
    $('#errorPayDtl').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorPayDtl').html('Please enter Payment Details');
    $('#' + id).css('border', '1px solid red');

    return false;
  }
  //  else {
  //   if (PaymentDtl.match(validatePayDtl)) {
  //     $('#errorPayDtl').addClass('showItem');
  //     $('#errorPayDtl').html('Please enter only alphabets');
  //     $('#' + id).css('border', '1px solid red');

  //     return false;
  //   }
  else {
    $('#errorPayDtl').removeClass('showItem');
    $('#errorPayDtl').addClass('showItem');
    $('#errorPayDtl').html(' ');
    $('#' + id).css('border', '1px solid green');

    return true;
  }
  // }
}
function validatePaymentMode() {
  var all_input = document.getElementsByClassName('check_in');
  for (var i = 0; i < all_input.length; ++i) {
    if (all_input[i].checked == true) {
      var selectedVal = all_input[i].value;

      console.log('selectedVal', selectedVal);

      if (selectedVal == 'Cash') {

        $('#id_PaymentMode').val(selectedVal)
        $('#feesAmt').animate({
          width: '300%',
        });
        $('#idPaymentDtl').css('display', 'none');
        // $('#feesAmt').val('');
      } else if (selectedVal == 'Cheque') {

        $('#id_PaymentMode').val(selectedVal)
        $('#feesAmt').animate({
          width: '100%',
        });
        $('#idPaymentDtl').animate({
          width: '95%',
        });
        $('#idPaymentDtl').css('display', 'block');

        // $('#feesAmt').val('');
      } else if (selectedVal == 'NetBanking') {

        $('#id_PaymentMode').val(selectedVal)
        $('#feesAmt').animate({
          width: '100%',

        });
        $('#idPaymentDtl').animate({
          width: '95%',
        });
        $('#idPaymentDtl').css('display', 'block');

      } else { }
    } else {
      $('#errorPaymentMode').css('display', 'block');
    }
  }
}
//-----------------------------------------validations on Submit------------------------------------------------------------------------------------------------------------
function sendEnroll() {
  validateFname('fName');
  // validateMname('mName');
  validateLname('lName');
  validateEmail('email');
  validatePassword('emailPass');
  validateMobileNo('mobileNo');
  validateTechnology();
  // validateCity('city');
  validateFees('feesAmt');
  validatePaymentMode();
  // console.log(" validatePaymentMode()", validatePaymentMode());
  // validateMname('mName ') &&
  // validateCity('city') &&
  if (validateFname('fName ') && validateLname('lName ') && validateEmail('email') &&
    validatePassword('emailPass') && validateMobileNo('mobileNo') && validateTechnology() &&
    validateFees('feesAmt') && validateGender()) {
    if ($('#cheque').is(':checked') || $('#netBanking').is(':checked')) {
      if (validatePaymentDtl('idPaymentDtl')) {
        callEnrollForm();
      }
    } else {
      callEnrollForm();
    }
  }
}
function callEnrollForm() {
  var saveEnroll = protocol + server + ':' + port + appName + '/saveEnrollment';
  var today = new Date();
  var technology = [];
  var courses;
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });

  var firstName = $('#fName').val();
  var middleName = $('#mName').val();
  var lastName = $('#lName').val();
  var gender = $('#id_Gander').val();
  var email = $('#email').val();
  var password = $('#emailPass').val();
  var mobileNo = $('#mobileNo').val();
  var city = $('#city').val();
  var enrollDate = today.getTime();
  var courses = courses;
  var pendingFees = $('#finalFees').val() - $('#feesAmt').val();
  var actualFees = $('#feesCourses').val();
  var serviceTaxAmt = $('#idServicesTax').val();
  var paidFees = $('#feesAmt').val();
  var pendingFees = pendingFees;
  var totalFees = $('#totalFees').val();
  var finalFees = $('#finalFees').val();
  var paymentMod = $('#id_PaymentMode').val();
  var paymentDtl = $('#idPaymentDtl').val();
  var paidDate = today.getTime();
  var fullName = firstName + ' ' + lastName;
  // console.log("paymentMod", paymentMod);
  var studentEnroll = {
    firstName: firstName,
    middleName: middleName,
    lastName: lastName,
    gender: gender,
    email: email,
    password: password,
    mobileNo: mobileNo,
    city: city,
    enrollDate: enrollDate,
    fees: {
      finalFees: finalFees,
      courses: courses,
      actualFees: actualFees,
      serviceTaxAmt: serviceTaxAmt,
      totalPaidFees: paidFees,
      pendingFees: pendingFees,
      totalFees: totalFees,
      paymentMod: paymentMod,
      paymentDtl: paymentDtl,
      paidDate: paidDate,
      enteredBy: localStorage.userName,
    }
  };
  // console.log("studentEnroll=>", studentEnroll);
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', saveEnroll, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(studentEnroll));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var codekulIDObj = JSON.parse(xhr.responseText);
        console.log('thisIsAnObject', codekulIDObj);
        // $('#modal-1').modal('show');
        $('#showCodekulIdModel').click();


        $('#IdCodekulCreate').unbind().click(function () {
          console.log('IdCodekulCreate');
          var w = window.open('pdf.html');
          // w.myVariable = codekulIDObj;
          var Page = 'enroll'
          localStorage.removeItem('myVariable');
          localStorage.removeItem('myVariable1');
          localStorage.removeItem('Page');
          localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
          localStorage.setItem('myVariable1', paidFees);
          localStorage.setItem('Page', Page);
          $('#feesAmt').css({
            'width': '300%',
            ' z-index': '1000',
            'position': 'relative'
          });
        });


        // w.myVariable1 = paidFees;
        // w.Page = "enroll";

        $('#userCodekulId_ID').text(codekulIDObj.codekulId);
        $('#userName_ID').text(fullName);
        // console.log('got success', codekulIDObj);
        $('.multiselect-selected-text').text('Please select technology');
        $('.multiselect').attr('title', 'Please select technology');
        $('#form1')[0].reset();
        $('.tech').css('border', 'none');
        $('form#form1 :input').each(function () {
          var input = $(this); // This is the jquery object of the input, do what you will
          $(this).css('border', '1px solid #aaa');
        });


      } else {
        console.log('xhr.status', xhr.status);
      }
    }
  };
}
var sum = 0;
var sub = 0;
function Add(item) {
  var textVal = $('#autoFees').val();
  var no1 = parseInt(textVal);
  var no2 = parseInt(item);
  var sum = no1 + no2;
  $('#autoFees').val(sum);
}
function Sub(item) {
  var textVal = $('#autoFees').val();
  var sub = textVal - item;
  $('#autoFees').val(sub);
}
var sum = 0;
var sub = 0;
function myAdd(item) {
  var textVal = $('#autoFeesEx').val();
  var no1 = parseInt(textVal);
  var no2 = parseInt(item);
  var sum = no1 + no2;
  $('#autoFeesEx').val(sum);
}
function mySub(item) {
  var textVal = $('#autoFeesEx').val();
  var sub = textVal - item;
  $('#autoFeesEx').val(sub);
}
// -------------------------------------------- Ex User ----------------------------------------------------
var user;
var codekulUserID;
var codekulUserName;
// ------------------------------------------------- searchName -------------------------------------------------------------------------
var options = {
  url: function (phraseName) {
    return protocol + server + ':' + port + '/codekul/searchByName/' + phraseName;
  },
  getValue: function (element) {
    return element.fullName;
  },
  list: {
    onSelectItemEvent: function () {
      var value = $('#searchName').getSelectedItemData();
      user = value;
      codekulUserID = value.codekulId;
      codekulUserName = value.fullName;
      // console.log("user:", user);
      $('#studName').text(codekulUserName);
      $('#userId').text(codekulUserID);
      $('#fullname').text(codekulUserName);
      $('#fullId').text(codekulUserID);
      $('#exUserName').text(user.fullName);
      $('#exCodekulId').text(user.codekulId);
      return value;
    }
  }
};
$('#searchName').easyAutocomplete(options);
$('#searchName').focus(function () {
  $('#searchCodekulID').val('');
});
$('#searchName').on('input', function (e) {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
  $('#header_fees').html('');
  $('#feesInfo').html('');
});
$('#searchName').blur(function () {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
});
// ------------------------------------------------- searchCodekulID -------------------------------------------------------------------------
var options = {
  url: function (phraseID) {
    return protocol + server + ':' + port + '/codekul/searchById/' + phraseID;
  },
  getValue: function (element) {
    return element.codekulId;
  },
  list: {
    onSelectItemEvent: function () {
      var valueCodekulID = $('#searchCodekulID').getSelectedItemData();
      user = valueCodekulID;
      codekulUserID = valueCodekulID.codekulId;
      codekulUserName = valueCodekulID.fullName;
      $('#exUserName').text(user.fullName);
      $('#exCodekulId').text(user.codekulId);
      return valueCodekulID;
    }
  }

};
$('#searchCodekulID').easyAutocomplete(options);
$('#searchCodekulID').focus(function () {
  $('#searchName').val('');
});
$('#searchCodekulID').on('input', function (e) {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
  $('#header_fees').html('');
  $('#feesInfo').html('');
});
$('#searchCodekulID').blur(function () {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
});
// ------------------------------------------------- Validate -------------------------------------------------------------------------
function validateTechnologyEx() {
  var technology = [];
  $.each($('#chCourseListEx option:selected'), function () {
    technology.push($(this).html());
  });
  if (technology.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.exCourse').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.exCourse').css('border', '1px solid green');
    return true;
  }

}
function validateFeesEx(id) {
  var feesFormat = /^[0-9]+$/;
  var fees = $('#' + id).val();
  if (fees === '') {
    $('#errorExFee').addClass('showItem');
    $('#errorExFee').html('Please enter Fees.');
    $('#errorExFee').css('color', 'blue!important');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    var TotalFees = parseInt($('#totalFeesEx').val());
    var PaidFees = parseInt($('#feesAmtEx').val());
    var PendingFees = TotalFees - PaidFees;
    if (PaidFees <= TotalFees) {
      $('#errorExFee').removeClass('showItem');
      $('#errorExFee').addClass('showItem');
      $('#' + id).css('border', '1px solid green');
      $('#errorExFee').html('Pending Fees : ' + PendingFees);
      return true;
    } else {
      $('#errorExFee').removeClass('showItem');
      $('#errorExFee').addClass('showItem');
      $('#errorExFee').html('Pending Fees : ' + PendingFees);
      $('#' + id).css('border', '1px solid red');
    }
  }
}
function validatePaymentDtlEx(id) {
  var PaymentDtl = $('#' + id).val();
  var validatePayDtl = /^[0-9]+$/;
  if (PaymentDtl === '') {
    $('#errorPayDtlEx').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorPayDtlEx').html('Please enter Payment Details');
    $('#' + id).css('border', '1px solid red');

    return false;
  }
  //  else {
  //   if (PaymentDtl.match(validatePayDtl)) {
  //     $('#errorPayDtlEx').addClass('showItem');
  //     $('#errorPayDtlEx').html('Please enter only alphabets');
  //     $('#' + id).css('border', '1px solid red');

  //     return false;
  //   }
  else {
    $('#errorPayDtlEx').removeClass('showItem');
    $('#errorPayDtlEx').addClass('showItem');
    $('#errorPayDtlEx').html(' ');
    $('#' + id).css('border', '1px solid green');

    return true;
  }
  // }
}
$('input[type=radio][name=enterFeesTypeEx]').change(function () {
  palymentEx()
});
function palymentEx() {
  var all_input = document.getElementsByClassName('check_inEx');
  for (var i = 0; i < all_input.length; ++i) {
    if (all_input[i].checked == true) {
      var selectedVal = all_input[i].value;
      if (selectedVal == 'Cash') {
        $('#id_PaymentModeEx').val(selectedVal)
        $('#feesAmtEx').css({
          'width': '340%'
        });
        $('#feesAmtEx').animate({
          width: '340%',
        });
        $('#idPaymentDtlEx').css('display', 'none');
      } else if (selectedVal == 'Cheque') {
        $('#feesAmtEx').animate({
          width: '120%',
        });
        $('#idPaymentDtlEx').css('display', 'block');
        $('#id_PaymentModeEx').val(selectedVal)
        $('#idPaymentDtlEx').animate({
          width: '95%',
        });
      } else if (selectedVal == 'NetBanking') {
        $('#id_PaymentModeEx').val(selectedVal)
        $('#feesAmtEx').animate({
          width: '120%',
        });
        $('#idPaymentDtlEx').css('display', 'block');
        $('#idPaymentDtlEx').animate({
          width: '95%',
        });
      } else { }
    } else {
    }
  }
}
function callExUserUpdate() {
  // console.log("user", user);
  validateTechnologyEx();
  validateFeesEx('feesAmtEx');
  palymentEx();
  if (validateTechnologyEx() && validateFeesEx('feesAmtEx')) {
    if ($('#chequeEx').is(':checked') || $('#netBankingEx').is(':checked')) {
      if (validatePaymentDtlEx('idPaymentDtlEx')) {
        sendExUserUpdate()
      }
    } else {
      sendExUserUpdate()
    }
  } else {
    console.log('Exter User Name or Codekul ID');
  }
}
function sendExUserUpdate() {
  var exUserUrl = protocol + server + ':' + port + appName + '/fees';
  var technologyEx = [];
  var newCourses;
  var today = new Date();
  $.each($('#chCourseListEx option:selected'), function () {
    technologyEx.push($(this).html());
    newCourses = technologyEx.toString();
  });
  // console.log("technologyEx", technologyEx);
  var totalF = $('#totalFeesEx').val();
  var courseF = $('#feesAmtEx').val();
  var pendingFees = $('#finalFeesEx').val() - $('#feesAmtEx').val();
  var courses = newCourses;
  var totalFees = $('#totalFeesEx').val();
  var finalFees = $('#finalFeesEx').val();
  var paidFees = $('#feesAmtEx').val();
  var actualFees = $('#feesCoursesEx').val();
  var pendingFees = pendingFees;
  var serviceTaxAmt = $('#idServicesTaxEx').val();
  var paymentMod = $('#id_PaymentModeEx').val();
  var paymentDtl = $('#idPaymentDtlEx').val();
  var paidDate = today.getTime();

  var fees = {
    codekulId: codekulUserID,
    courses: courses,
    totalFees: totalFees,
    finalFees: finalFees,
    totalPaidFees: paidFees,
    actualFees: actualFees,
    pendingFees: pendingFees,
    serviceTaxAmt: serviceTaxAmt,
    paymentMod: paymentMod,
    paymentDtl: paymentDtl,
    paidDate: paidDate,
    enteredBy: localStorage.userName,
  };
  // console.log("fees", fees);
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', exUserUrl, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(fees));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {

        var codekulIDObj = JSON.parse(xhr.responseText);
        console.log('thisIsAnObject', codekulIDObj);
        // $('#modal-enroll').modal('show');
        $('#showExCodekulIdModel').click();

        $('#ExEnroll').unbind().click(function () {
          console.log('IdCodekulCreate');
          var w = window.open('pdf.html');
          // w.myVariable = codekulIDObj;
          var Page = 'enroll'
          localStorage.removeItem('myVariable');
          localStorage.removeItem('myVariable1');
          localStorage.removeItem('Page');
          localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
          localStorage.setItem('myVariable1', paidFees);
          localStorage.setItem('Page', Page);
          $('#feesAmt').css({
            'width': '300%',
            ' z-index': '1000',
            'position': 'relative'
          });
        });




        // console.log("thisIsAnObject", codekulIDObj);
        // var w = window.open("pdf.html");
        // w.myVariable = codekulIDObj;
        // w.myVariable1 = paidFees;
        // w.Page = "enroll";

        // $('#ExEnroll').unbind().click(function () {
        //   console.log("IdCodekulCreate");
        //   var w = window.open("pdf.html");
        //   // w.myVariable = codekulIDObj;
        //   var Page = "FeesUpdate"
        // localStorage.removeItem("myVariable");
        // localStorage.removeItem("myVariable1");
        // localStorage.removeItem("Page");
        //   localStorage.setItem('myVariable', JSON.stringify(codekulIDObj));
        //   localStorage.setItem('myVariable1', paidFees);
        //   localStorage.setItem('Page', Page);
        // });

        // console.log('got success :', xhr);
        $('.multiselect-selected-text').text('Please select technology');
        $('.multiselect').attr('title', 'Please select technology');
        $('#form2')[0].reset();
        $('.exCourse').css('border', 'none');
        $('form#form2 :input').each(function () {
          var input = $(this); // This is the jquery object of the input, do what you will
          $(this).css('border', '1px solid #aaa');
        });
        // document.getElementById('openMsg_Enroll').click();
      } else {
        console.log('got success', xhr.responseText);
      }
    }
  };
}
function openModel() {
  // $('#modal-open').modal('show');
  // var thisIsAnObject = { foo: 'bar' };
  // console.log("thisIsAnObject", thisIsAnObject);
  // var w = window.open("pdf.html");
  // w.myVariable = thisIsAnObject;
  // var myVariable = window.opener.thisIsAnObject;
}