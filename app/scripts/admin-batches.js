localStorage.pagePath = '/batches.html'
if (localStorage.userName == undefined) {
  window.location.href = 'index.html';
} else {
  // console.log('localStorage.pagePath =>', localStorage.pagePath)
}

$(document).ready(function () {
  clickRegularBatches();
  // http://timepicker.co/
  // https://jqueryui.com/datepicker/
  //------------------------------- all time picker------------------------------------------
  $('#basicExample').timepicker();
  $('input.timepicker#batchStartT').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '10:00pm',
    defaultTime: '10:00 AM',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
  $('input.timepicker#batchEndT').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '10:00pm',
    defaultTime: '10:00 PM',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
  $('input.timepicker#batchStartTUpdate').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '10:00pm',
    defaultTime: '10:00 PM',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
  $('input.timepicker#batchEndTUpdate').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '10:00pm',
    defaultTime: '10:00 PM',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
  //------------------------------- all date picker------------------------------------------
  $('#datepickerS').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#datepickerE').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#datepickerSUpdate').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#datepickerEUpdate').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  //------------------------------- all Multi Select------------------------------------------
  $('#chCourseList').multiselect({
    maxHeight: 100,
    buttonWidth: '100%',
    nonSelectedText: 'Please select courses',
    onChange: function (option, checked, select) {
    },
    onDropdownHidden: function (event) {
      $('#feesCourses').val($('#autoFees').val());
      var am = $('#feesCourses').val();
      var ta = 15;
      var total = (am * ta) / 100;
      $('#idServicesTax').val(total);
      var totalFees = parseInt(total) + parseInt(am)
      $('#totalFees').val(totalFees);
    }
  });
  $('#chCourseListEx').multiselect({
    maxHeight: 100,
    buttonWidth: '100%',
    nonSelectedText: 'Please select Trainer',
    onChange: function (option, checked, select) {
    },
    onDropdownHidden: function (event) {
      $('#feesCoursesEx').val($('#autoFeesEx').val());
      var am = $('#feesCoursesEx').val();
      var ta = 15;
      var total = (am * ta) / 100;
      $('#idServicesTaxEx').val(total);
      var totalFees = parseInt(total) + parseInt(am)
      $('#totalFeesEx').val(totalFees);
    }
  });
  $('#chCourseListExUpdate').multiselect({
    maxHeight: 100,
    buttonWidth: '100%',
    nonSelectedText: 'Please select Trainer',
    onChange: function (option, checked, select) {
    },
  });
  //------------------------------- get allCourses webserives ------------------------------------------
  var getCoursesUrl = protocol + server + ':' + port + appName + '/allCourses';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhrc = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhrc = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhrc = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhrc.open('GET', getCoursesUrl, true);
  xhrc.send();
  xhrc.onreadystatechange = function () {
    if (xhrc.readyState == 4) {
      if (xhrc.status == 200) {
        var courseList;
        courseList = JSON.parse(xhrc.responseText);
        var inHTML = '';
        $.each(courseList, function (index, value) {
          var val = value.courseName;
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseList ').append(htm);
          $('#chCourseList').multiselect('rebuild');
          $('#chCourseListUpdate').append(htm);
          $('#chCourseListUpdate').multiselect('rebuild');
        });
      } else {
        console.log('start web services', document.getElementById('req_text').innerHTML = 'Error Code: ' + xhrc.status + 'Error Message: ' + xhrc.statusText)
      }
    }
  };
  //------------------------------- get allTeacher webserives ------------------------------------------
  var getTeachesUrl = protocol + server + ':' + port + appName + '/getAllTeacher';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getTeachesUrl, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var trainerList;
        trainerList = JSON.parse(xhr.responseText);
        var inHTML = '';
        $.each(trainerList, function (index, value) {
          var val = value.teacherName;
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseListEx').append(htm);
          $('#chCourseListEx').multiselect('rebuild');
        });
      } else {
        console.log('start web services', document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText)
      }
    }
  };

});
// ---------------------------------------- Show Batches-------------------------------------------------------------
function clickRegularBatches() {
  var getRegularBatches = protocol + server + ':' + port + appName + '/getBatchByType/Regular';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhrc = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhrc = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhrc = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhrc.open('GET', getRegularBatches, true);
  xhrc.send();
  xhrc.onreadystatechange = function () {
    if (xhrc.readyState == 4) {
      if (xhrc.status == 200) {
        $('#regularBatches').html('');
        var Regular;
        Regular = JSON.parse(xhrc.responseText);
        var count = 1;
        var goSearchTable =
          '<div class=\'row\'id=\'batchData\' style=\'width: 100%;font-weight: 600;border-bottom: 1px solid #aaa;padding: 0px 0px;margin-bottom: 5px;\'   >' +
          '<div class=\'col-lg-1 \' > No. </div>' +
          '<div class=\'col-lg-2 \' > Course Name   </div>' +
          '<div class=\'col-lg-3 \' onClick=\'sortDate("Regular");\'  style=\'text-align:center;\'> ' +
          '<p> Batch Duration ' +
          '<span id="show"><i class="fa fa-sort" aria-hidden="true" id="srDtAcs" ></i></span>' +
          '<span id="hide" style="display:none"><i class="fa fa-sort" aria-hidden="true" id="srDtDec" ></i></span>  </p>' +
          '</div>' +
          '<div class=\'col-lg-3  \'style=\'text-align:center;\' >  Batch Time</div>' +
          '<div class=\'col-lg-2 \' onClick=\'sortName("Regular");\' style=\'padding:0px;\'> ' +
          '<p  > Trainer Name </p>' +
          '</div>' +
          '<div class=\'col-lg-1  \' >   </div>' +
          '</div>' +
          '<div class=\'row\'id=\'regularBatchInfo\' style=\'width:100%; margin:0 auto;max-height: 425px;overflow-y: auto;\' > ';
        $('#regularBatches').append(goSearchTable);
        $.each(Regular, function (index, value) {
          var dateS = new Date(value.startDate);
          var dateE = new Date(value.endDate);
          var todayTime = new Date(dateS.toString());
          var month = todayTime.getMonth() + 1;
          var day = todayTime.getDate();
          var year = todayTime.getFullYear();
          var todayTimeE = new Date(dateE.toString());
          var monthE = todayTimeE.getMonth() + 1;
          var dayE = todayTimeE.getDate();
          var yearE = todayTimeE.getFullYear();
          var batchStartDate = day + '-' + month + '-' + year;
          var batchEndDate = dayE + '-' + monthE + '-' + yearE;
          var batchData =
            '<div class=\'row\' id=\'inline\' style=\'width:100%;\'  >' +
            '<div class=\'col-lg-1\'>' + (count++) + '</div>' +
            '<div class=\'col-lg-2\'>' + value.course + '</div>' +
            '<div class=\'col-lg-3\' >' +
            '<div class=\'row\' style=\'text-align:center;\'>' +
            '<div class=\'col-lg-5\' style=\'padding:0px;\'>' + batchStartDate + '</div>' +
            '<div class=\'col-lg-2\'style=\'padding:0px;\' ><b>to</b></div>' +
            '<div class=\'col-lg-5\'style=\'padding:0px;\' >' + batchEndDate + '</div>' +
            '</div>' +
            '</div>' +

            '<div class=\'col-lg-3\' >' +
            '<div class=\'row\' style=\'text-align:center;\'>' +
            '<div class=\'col-lg-5\' style=\'padding:0px;\'>' + value.startTime + '</div>' +
            '<div class=\'col-lg-2\'style=\'padding:0px;\' ><b>to</b></div>' +
            '<div class=\'col-lg-5\'style=\'padding:0px;\' >' + value.endTime + '</div>' +
            '</div>' +
            '</div>' +

            '<div class=\'col-lg-1\' style=\'text-align:center;padding:0px;  \'>' + value.teacherName + '</div>' +
            '<div class=\'col-lg-1\' style=\'text-align:right;  \' > ' +
            '<button type="button" onclick="callBack(' + value.batchId + ');" class="btn  bmd-btn-fab bmd-btn-fab-sm" style=\'background-color: #009688;color: #fff; font-size: 20px;\'>' +
            '<i class="fa fa-pencil fa-1x" aria-hidden="true" style=\'color:#fff;\'></i> ' +
            '</button>' +
            '</div> ' +
            '<div class=\'col-lg-1\' style=\'text-align:right; \' > ' +
            '<button type="button" onclick="callDelete(' + value.batchId + ');" class="btn  bmd-btn-fab bmd-btn-fab-sm" style=\'background-color: #009688;color: #fff; font-size: 20px;\'>' +
            '<i class="fa fa-trash fa-1x" aria-hidden="true"></i>                    ' +
            '</button>' +
            '</div> ' +
            '</div> </div>';
          $('#regularBatchInfo').append(batchData);
        });
      } else {
        console.log('start web services', document.getElementById('req_text').innerHTML = 'Error Code: ' + xhrc.status + 'Error Message: ' + xhrc.statusText)
      }
    }
  };
}
function clickWeekendBatches() {
  var getWeekendBatches = protocol + server + ':' + port + appName + '/getBatchByType/Weekend';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhrc = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhrc = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhrc = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhrc.open('GET', getWeekendBatches, true);
  xhrc.send();
  xhrc.onreadystatechange = function () {
    if (xhrc.readyState == 4) {
      if (xhrc.status == 200) {
        $('#weekendBatches').html('');
        var Weekend;
        Weekend = JSON.parse(xhrc.responseText);
        var count = 1;
        var table =
          '<div class=\'row\'id=\'batchData\'  style=\'width: 100%;font-weight: 600;border-bottom: 1px solid #aaa;padding: 0px 0px;margin-bottom: 5px;\'  >' +
          '<div class=\'col-lg-1 \' > No. </div>' +
          '<div class=\'col-lg-2 \' > <p> Course Name </p> </div>' +
          '<div class=\'col-lg-3 \' onClick=\'sortDateWeekend("Weekend");\'  style=\'text-align:center;\'>  ' +
          '<p>  Batch Duration' +
          '<span id="showWeek"><i class="fa fa-sort" aria-hidden="true" id="srDtWeekendAcs" ></i></span>' +
          '<span id="hideWeek" style="display:none"><i class="fa fa-sort" aria-hidden="true" id="srDtWeekendDec" ></i></span>  </p>' +
          '</div>' +
          '<div class=\'col-lg-3  \'style=\'text-align:center;\' >  Batch Time</div>' +
          '<div class=\'col-lg-2 \' onClick=\'sortNameWeekend("Weekend");\'  style=\'padding:0px;\'> ' +
          '<p  > Trainer Name </p>' +
          '</div>' +

          '<div class=\'col-lg-1 \' > </div></div>' +
          '<div class=\'row\'id=\'weekendBatchInfo\' style=\'width:100%; margin:0 auto;max-height: 425px;overflow-y: auto;\' >';
        $('#weekendBatches').append(table);
        $.each(Weekend, function (index, value) {
          var dateS = new Date(value.startDate);
          var dateE = new Date(value.endDate);
          var todayTime = new Date(dateS.toString());
          var month = todayTime.getMonth() + 1;
          var day = todayTime.getDate();
          var year = todayTime.getFullYear();
          var todayTimeE = new Date(dateE.toString());
          var monthE = todayTimeE.getMonth() + 1;
          var dayE = todayTimeE.getDate();
          var yearE = todayTimeE.getFullYear();
          var batchStartDate = day + '-' + month + '-' + year;
          var batchEndDate = dayE + '-' + monthE + '-' + yearE;

          var batchData =
            '<div class=\'row\' id=\'inline\' style=\'width:100%;\' >' +
            '<div class=\'col-lg-1\'>' + (count++) + '</div>' +
            '<div class=\'col-lg-2\'>' + value.course + '</div>' +
            '<div class=\'col-lg-3\' >' +
            '<div class=\'row\' style=\'text-align:center;\'>' +
            '<div class=\'col-lg-5\' style=\'padding:0px;\'>' + batchStartDate + '</div>' +
            '<div class=\'col-lg-2\'style=\'padding:0px;\' ><b>to</b></div>' +
            ' <div class=\'col-lg-5\'style=\'padding:0px;\' >' + batchEndDate + '</div>' +
            '</div></div>' +

            '<div class=\'col-lg-3\' >' +
            '<div class=\'row\' style=\'text-align:center;\'>' +
            '<div class=\'col-lg-5\' style=\'padding:0px;\'>' + value.startTime + '</div>' +
            '<div class=\'col-lg-2\'style=\'padding:0px;\' ><b>to</b></div>' +
            ' <div class=\'col-lg-5\'style=\'padding:0px;\' >' + value.endTime + '</div>' +
            '</div></div>' +


            '<div class=\'col-lg-1\' style=\'text-align:center; padding:0px;\'>' + value.teacherName + '</div>' +
            '<div class=\'col-lg-1\' style=\'text-align:right;  \' > ' +
            '<button type="button" onclick="callBack(' + value.batchId + ');" class="btn  bmd-btn-fab bmd-btn-fab-sm" style=\'background-color: #009688;color: #fff; font-size: 20px;\'>' +
            '<i class="fa fa-pencil fa-1x" aria-hidden="true" style=\'color:#fff;\'></i> ' +
            '</button>' +

            '</div> ' +
            '<div class=\'col-lg-1\' style=\'text-align:right; \' > ' +
            '<button type="button" onclick="callDelete(' + value.batchId + ');" class="btn  bmd-btn-fab bmd-btn-fab-sm" style=\'background-color: #009688;color: #fff; font-size: 20px;\'>' +
            '<i class="fa fa-trash fa-1x" aria-hidden="true"></i>                    ' +
            '</button>' +
            '</div> ' +
            '</div> </div>';
          $('#weekendBatchInfo').append(batchData);
        });
      } else {
        console.log('start web services', document.getElementById('req_text').innerHTML = 'Error Code: ' + xhrc.status + 'Error Message: ' + xhrc.statusText)
      }
    }
  };
}
// ---------------------------------------- Sort Batches-------------------------------------------------------------
function sortNameWeekend(batchMode) {
  $('span').toggle();
  var isVisible = $('#showWeek').is(':visible');
  var isHidden = $('#showWeek').is(':hidden');
  $('#srDtWeekendAcs').removeClass('fa-sort-asc');
  $('#srDtWeekendDec').removeClass('fa-sort-desc');
  if (isVisible) {
    $('#srNmWeekendAcs').addClass('fa-sort-asc');
    $('#srNmWeekendDec').removeClass('fa-sort-desc');
    batchesSort(batchMode, 'Asc')
  } else {
    $('#srNmWeekendAcs').removeClass('fa-sort-asc');
    $('#srNmWeekendDec').addClass('fa-sort-desc');
    batchesSort(batchMode, 'Desc')
  }
}
function sortDateWeekend(batchMode) {
  $('span').toggle();
  var isVisible = $('#showWeek').is(':visible');
  var isHidden = $('#showWeek').is(':hidden');
  $('#srNmWeekendAcs').removeClass('fa-sort-asc');
  $('#srNmWeekendDec').removeClass('fa-sort-desc');
  if (isVisible) {
    $('#srDtWeekendAcs').addClass('fa-sort-asc');
    $('#srDtWeekendDec').removeClass('fa-sort-desc');
    batchesSort(batchMode, 'Asc')
  } else {
    $('#srDtWeekendAcs').removeClass('fa-sort-asc');
    $('#srDtWeekendDec').addClass('fa-sort-desc');
    batchesSort(batchMode, 'Desc')
  }
}
function sortName(batchMode) {
  $('span').toggle();
  var isVisible = $('#show').is(':visible');
  var isHidden = $('#show').is(':hidden');
  $('#srDtAcs').removeClass('fa-sort-asc');
  $('#srDtDec').removeClass('fa-sort-desc');
  if (isVisible) {
    $('#srNmAcs').addClass('fa-sort-asc');
    $('#srNmDec').removeClass('fa-sort-desc');
    batchesSort(batchMode, 'Asc')
  } else {
    $('#srNmAcs').removeClass('fa-sort-asc');
    $('#srNmDec').addClass('fa-sort-desc');
    batchesSort(batchMode, 'Desc')
  }
}
function sortDate(batchMode) {
  $('span').toggle();
  var isVisible = $('#show').is(':visible');
  var isHidden = $('#show').is(':hidden');
  $('#srNmAcs').removeClass('fa-sort-asc');
  $('#srNmDec').removeClass('fa-sort-desc');
  if (isVisible) {
    $('#srDtAcs').addClass('fa-sort-asc');
    $('#srDtDec').removeClass('fa-sort-desc');
    batchesSort(batchMode, 'Asc')
  } else {
    $('#srDtAcs').removeClass('fa-sort-asc');
    $('#srDtDec').addClass('fa-sort-desc');
    batchesSort(batchMode, 'Desc')
  }
}
function batchesSort(batchType, order) {
  $('#regularBatchInfo').html('');
  $('#weekendBatchInfo').html('');
  var getBatchesSort = protocol + server + ':' + port + appName + '/getBatchByTypeSortByDate/' + batchType + '/' + order;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhrc = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhrc = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhrc = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhrc.open('GET', getBatchesSort, true);
  xhrc.send();
  xhrc.onreadystatechange = function () {
    if (xhrc.readyState == 4) {
      if (xhrc.status == 200) {
        var Regular;
        Regular = JSON.parse(xhrc.responseText);
        var count = 1;
        $.each(Regular, function (index, value) {
          var dateS = new Date(value.startDate);
          var dateE = new Date(value.endDate);
          var todayTime = new Date(dateS.toString());
          var month = todayTime.getMonth() + 1;
          var day = todayTime.getDate();
          var year = todayTime.getFullYear();
          var todayTimeE = new Date(dateE.toString());
          var monthE = todayTimeE.getMonth() + 1;
          var dayE = todayTimeE.getDate();
          var yearE = todayTimeE.getFullYear();
          var batchStartDate = day + '-' + month + '-' + year;
          var batchEndDate = dayE + '-' + monthE + '-' + yearE;
          var batchData =
            '<div class=\'row\'   style=\'width:100%;\'  >' +
            '<div class=\'col-lg-1\'>' + (count++) + '</div>' +
            '<div class=\'col-lg-2\'>' + value.course + '</div>' +
            '<div class=\'col-lg-3\' >' +
            '<div class=\'row\' style=\'text-align:center;\'>' +
            '<div class=\'col-lg-5\' style=\'padding:0px;\'>' + batchStartDate + '</div>' +
            '<div class=\'col-lg-2\'style=\'padding:0px;\' ><b>to</b></div>' +
            '<div class=\'col-lg-5\'style=\'padding:0px;\' >' + batchEndDate + '</div>' +
            '</div>' +
            '</div>' +

            '<div class=\'col-lg-3\' >' +
            '<div class=\'row\' style=\'text-align:center;\'>' +
            '<div class=\'col-lg-5\' style=\'padding:0px;\'>' + value.startTime + '</div>' +
            '<div class=\'col-lg-2\'style=\'padding:0px;\' ><b>to</b></div>' +
            '<div class=\'col-lg-5\'style=\'padding:0px;\' >' + value.endTime + '</div>' +
            '</div>' +
            '</div>' +

            '<div class=\'col-lg-1\' style=\'text-align:center;padding:0px;  \'>' + value.teacherName + '</div>' +
            '<div class=\'col-lg-1\' style=\'text-align:right;  \' > ' +
            '<button type="button" onclick="callBack(' + value.batchId + ');" class="btn  bmd-btn-fab bmd-btn-fab-sm" style=\'background-color: #009688;color: #fff; font-size: 20px;\'>' +
            '<i class="fa fa-pencil fa-1x" aria-hidden="true" style=\'color:#fff;\'></i> ' +
            '</button>' +
            '</div> ' +
            '<div class=\'col-lg-1\' style=\'text-align:right; \' > ' +
            '<button type="button" onclick="callDelete(' + value.batchId + ');" class="btn  bmd-btn-fab bmd-btn-fab-sm" style=\'background-color: #009688;color: #fff; font-size: 20px;\'>' +
            '<i class="fa fa-trash fa-1x" aria-hidden="true"></i>                    ' +
            '</button>' +
            '</div> ' +
            '</div> </div>';

          $('#regularBatchInfo').append(batchData);
          $('#weekendBatchInfo').append(batchData);
        });
      } else {

      }
    }
  };
}

// ---------------------------------------- validations Batches-------------------------------------------------------------
function validateTechnology() {
  var technology = [];
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
  });
  if (technology.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.tech').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.tech').css('border', '1px solid green');
    return true;
  }

}
function validateDateS() {
  var currentDate = $('#datepickerS').datepicker('getDate');
  var txtVal = $('#datepickerS').val();
  if (isDate(txtVal)) {
    $('#datepickerS').css('border', '1px solid green');
    return true
  } else {
    $('#datepickerS').css('border', '1px solid red');
    return false
  }

  function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '') {
      return false;
    } else {
      var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
      var dtArray = currVal.match(rxDatePattern); // is format OK?


      if (dtArray == null)
        return false;

      //Checks for mm/dd/yyyy format.
      var dtMonth = dtArray[3];
      var dtDay = dtArray[1];
      var dtYear = dtArray[5];

      if (dtDay < 1 || dtDay > 31)
        return false;
      else if (dtMonth < 1 || dtMonth > 12)
        return false;
      else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
      else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
          return false;
      }
      return true;
    }
  }
}
function validateDateE() {
  var currentDate = $('#datepickerE').datepicker('getDate');
  var txtVal = $('#datepickerE').val();
  if (isDate(txtVal)) {
    $('#datepickerE').css('border', '1px solid green');
    return true
  } else {
    $('#datepickerE').css('border', '1px solid red');
    return false
  }

  function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
      return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
      return false;

    //Checks for mm/dd/yyyy format.
    var dtMonth = dtArray[3];
    var dtDay = dtArray[1];
    var dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
      return false;
    else if (dtDay < 1 || dtDay > 31)
      return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
      return false;
    else if (dtMonth == 2) {
      var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
      if (dtDay > 29 || (dtDay == 29 && !isleap))
        return false;
    }
    return true;
  }
}
function validateSTime() {
  var txtVal = $('#batchStartT').val();


  if (txtVal == '') {
    $('#batchStartT').css('border', '1px solid red');
    return false
  } else {
    $('#batchStartT').css('border', '1px solid green');
    return true
  }


}
function validateETime() {
  var txtVal = $('#batchEndT').val();
  if (txtVal == '') {
    $('#batchEndT').css('border', '1px solid red');
    return false
  } else {
    $('#batchEndT').css('border', '1px solid green');
    return true
  }
}
function validateBatchType() {
  var batchType;
  if ($('input[name=batchType]:checked').length <= 0) {
    alert('No radio checked')
    return false;
  } else {
    if ($('#regularBatch').prop('checked') == true) {
      batchType = 'Regular';
      $('#id_batchType').val(batchType)
      return true;
    }
    if ($('#weekendBatch').prop('checked') == true) {
      batchType = 'Weekend';
      $('#id_batchType').val(batchType)
      return true;
    }
  }
}
function validateTeacher() {
  var teacher = [];
  $.each($('#chCourseListEx option:selected'), function () {
    teacher.push($(this).html());
  });
  if (teacher.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.teacher').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.teacher').css('border', '1px solid green');
    return true;
  }
}

// ------------------------------------------ Add batches  ------------------------------------------------
$('#btnAddBatch').click(function () {
  // console.log("Add batch");
  $('#modal-1').modal('show');
  $(document).keyup(function (e) {
    if (e.keyCode == 27) { // esc keycode
      $('#modal-1').modal('hide');
    }
  });
});
function sendBatch() {
  validateTechnology();
  validateDateS();
  validateDateE();
  validateSTime();
  validateETime();
  validateBatchType();
  validateTeacher();
  if (validateTechnology() && validateDateS() && validateDateE() && validateSTime() && validateETime() && validateBatchType() && validateTeacher()) {
    callBatch();
  }
}
function callBatch() {
  var saveUrl = protocol + server + ':' + port + appName + '/addBatch';
  var technology = [];
  var courses;
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });
  var trainerList = [];
  var Trainer;
  $.each($('#chCourseListEx option:selected'), function () {
    trainerList.push($(this).html());
    Trainer = trainerList.toString();
  });
  var fTime = parseFloat($('#batchStartT').val());
  var lTime = parseFloat($('#batchEndT').val());
  var totalHr = fTime - lTime;
  var currentDateS = $('#datepickerS').datepicker('getDate');
  var currentDateE = $('#datepickerE').datepicker('getDate');
  var course = courses;
  var batchDateS = currentDateS.getTime();
  var batchDateE = currentDateE.getTime();
  var bStartTime = $('#batchStartT').val();
  var bEndTime = $('#batchEndT').val();
  var bType = $('#id_batchType').val();
  var Trainer = Trainer;
  var timeHr = totalHr;


  var createBatch = {
    course: course,
    startDate: batchDateS,
    endDate: batchDateE,
    startTime: bStartTime,
    endTime: bEndTime,
    batchType: bType,
    teacherName: Trainer,
  };
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', saveUrl, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(createBatch));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        // console.log(' ', xhr.responseText);
        // console.log('got success');
        $('.multiselect-selected-text').text('Please select  ');
        $('.multiselect-selected-text').text('Please select  ');
        $('#form1')[0].reset();
        $('#chCourseList').multiselect({
          maxHeight: 100,
          buttonWidth: '100%',
          nonSelectedText: 'Please select courses',
          optionLabel: function (element) {
            return 'Please select courses';
          }
        });
        $('.tech').css('border', 'none');
        $('.teacher').css('border', 'none');
        $('form#form1 :input').each(function () {
          var input = $(this);
          $(this).css('border', '1px solid #aaa');
        });
        $('#modal-1').modal('hide');
        showBack();
      } else {
        console.log('', 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText);
      }
    }
  };
}
// ------------------------------------------   batches call ------------------------------------------------
function showBack() {
  clickRegularBatches();
  clickWeekendBatches();
}
// ------------------------------------------ Show Update batch------------------------------------------------
function callBack(bId) {
  // console.log("Update ");
  // console.log("bId", bId);
  $('#modal-2').modal('show');
  $(document).keyup(function (e) {
    if (e.keyCode == 27) { // esc keycode
      $('#modal-2').modal('hide');
    }
  });
  // $("#datepickerSUpdate").datepicker();
  var getBatchesById = protocol + server + ':' + port + appName + '/getBatchById/' + bId;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhrc = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhrc = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhrc = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhrc.open('GET', getBatchesById, true);
  xhrc.send();
  xhrc.onreadystatechange = function () {
    if (xhrc.readyState == 4) {
      if (xhrc.status == 200) {
        var Regular;
        var getBatchBybIdObj = JSON.parse(xhrc.responseText);

        var dateS = new Date(getBatchBybIdObj.startDate);
        var dateE = new Date(getBatchBybIdObj.endDate);
        var todayTime = new Date(dateS.toString());
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
        var todayTimeE = new Date(dateE.toString());
        var monthE = todayTimeE.getMonth() + 1;
        var dayE = todayTimeE.getDate();
        var yearE = todayTimeE.getFullYear();
        var batchStartDate = day + '-' + month + '-' + year;
        var batchEndDate = dayE + '-' + monthE + '-' + yearE;

        $('#batchID').val(getBatchBybIdObj.batchId);
        $('#idBatchType').val(getBatchBybIdObj.batchType);
        $('#updateCoursesValue').val(getBatchBybIdObj.course);
        $('#datepickerSUpdate').val(batchStartDate);
        $('#datepickerEUpdate').val(batchEndDate);
        $('input.timepicker#batchStartTUpdate').val(getBatchBybIdObj.startTime);
        $('input.timepicker#batchEndTUpdate').val(getBatchBybIdObj.endTime);
        $('#chCourseListExUpdatevalue').val(getBatchBybIdObj.teacherName);

        var htm;
        var options = $('#chCourseListEx option');
        $.each(options, function (index, value) {
          if (getBatchBybIdObj.teacherName.indexOf(value.text) !== -1) {
            htm += '<option value ="' + value.text + '" selected>' + value.text + '</option>';
          } else {
            htm += '<option value ="' + value.text + '" >' + value.text + '</option>';
          }
        });
        $('#chCourseListExUpdate').html('');
        $('#chCourseListExUpdate').append(htm);
        $('#chCourseListExUpdate').multiselect('rebuild');
      } else {
        console.log('start web services', document.getElementById('req_text').innerHTML = 'Error Code: ' + xhrc.status + 'Error Message: ' + xhrc.statusText)
      }
    }
  };
}
// ------------------------------------------  Update batch  ------------------------------------------------
function sendBatchUpdate() {
  var saveUrl = protocol + server + ':' + port + appName + '/addBatch';
  var technology = [];
  var courses;
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });
  var trainerList = [];
  var Trainer;
  $.each($('#chCourseListExUpdate option:selected'), function () {
    trainerList.push($(this).html());
    Trainer = trainerList.toString();

  });

  var batchId = $('#batchID').val();
  var currentDateSUpdate = $('#datepickerSUpdate').datepicker('getDate');
  var currentDateEUpdate = $('#datepickerEUpdate').datepicker('getDate');
  var course = $('#updateCoursesValue').val();
  var batchDateS = currentDateSUpdate.getTime();
  var batchDateE = currentDateEUpdate.getTime();
  var bStartTime = $('#batchStartTUpdate').val();
  var bEndTime = $('#batchEndTUpdate').val();
  var bType = $('#idBatchType').val();
  var Trainer = $('#chCourseListExUpdatevalue').val();

  var updateBatches = {
    batchId: batchId,
    course: course,
    startDate: batchDateS,
    endDate: batchDateE,
    startTime: bStartTime,
    endTime: bEndTime,
    batchType: bType,
    teacherName: Trainer,
  };
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', saveUrl, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(updateBatches));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        console.log('got success');
        $('.multiselect-selected-text').text('Please select  ');
        $('.multiselect-selected-text').text('Please select  ');
        $('#form2')[0].reset();
        showBack();
        $('#modal-2').modal('hide');
        $('.teacherUpdate').css('border', 'none');
        $('form#form2 :input').each(function () {
          var input = $(this);
          $(this).css('border', '1px solid #aaa');
        });
      } else {
        console.log('', 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText);
      }
    }
  };
}
// ------------------------------------------ Delete batch------------------------------------------------
function callDelete(bId) {
  var getBatchesById = protocol + server + ':' + port + appName + '/deleteBatch/' + bId;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhrc = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhrc = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhrc = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhrc.open('GET', getBatchesById, true);
  xhrc.send();
  xhrc.onreadystatechange = function () {
    if (xhrc.readyState == 4) {
      if (xhrc.status == 200) {
        console.log('delete batch: ', xhrc.responseText);
        clickRegularBatches();
        clickWeekendBatches();
      } else {
        console.log('delete batch: ', xhrc.responseText);
      }
    }
  };


}
// function gotoBtach() {
//   $('#createBatch').css('display', 'block');
//   $('#dispayRegulerBatches').css('display', 'none');
//   $('#showBatches').css('display', 'none');
//   $('#form1').css('display', 'block');
// }