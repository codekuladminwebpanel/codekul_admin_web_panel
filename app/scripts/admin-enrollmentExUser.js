var user;
var codekulUserID;
var codekulUserName;
// ------------------------------------------------- searchName -------------------------------------------------------------------------
var options = {
  url: function (phraseName) {
    // http://192.168.0.32:4444/codekul/allCourses
    return 'http://192.168.0.21:4444/codekul/searchByName/' + phraseName;
  },
  getValue: function (element) {
    return element.fullName;
  },
  list: {
    onSelectItemEvent: function () {
      var value = $('#searchName').getSelectedItemData();
      user = value;
      codekulUserID = value.codekulId;
      codekulUserName = value.fullName;
      $('#studName').text(codekulUserName);
      $('#userId').text(codekulUserID);
      $('#fullname').text(codekulUserName);
      $('#fullId').text(codekulUserID);
      $('#exUserName').text(user.fullName);
      $('#exCodekulId').text(user.codekulId);
      return value;
    }
  }
};
$('#searchName').easyAutocomplete(options);

$('#searchName').focus(function () {
  $('#searchCodekulID').val('');
});
$('#searchName').on('input', function (e) {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
  $('#header_fees').html('');
  $('#feesInfo').html('');
});
$('#searchName').blur(function () {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
});
// ------------------------------------------------- searchCodekulID -------------------------------------------------------------------------
var options = {
  url: function (phraseID) {
    return 'http://192.168.0.21:4444/codekul/searchById/' + phraseID;
  },
  getValue: function (element) {
    return element.codekulId;
  },
  list: {
    onSelectItemEvent: function () {
      var valueCodekulID = $('#searchCodekulID').getSelectedItemData();
      user = valueCodekulID;
      codekulUserID = valueCodekulID.codekulId;
      codekulUserName = valueCodekulID.fullName;
      $('#exUserName').text(user.fullName);
      $('#exCodekulId').text(user.codekulId);
      return valueCodekulID;
    }
  }

};
$('#searchCodekulID').easyAutocomplete(options);
$('#searchCodekulID').focus(function () {
  $('#searchName').val('');
});
$('#searchCodekulID').on('input', function (e) {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
  $('#header_fees').html('');
  $('#feesInfo').html('');
});
$('#searchCodekulID').blur(function () {
  $('#goSearch').html('');
  $('#goSearch').css('padding-bottom', '0px');
});
// ------------------------------------------------- Validate -------------------------------------------------------------------------
function validateTechnologyEx() {
  var technology = [];
  $.each($('#chCourseListEx option:selected'), function () {
    technology.push($(this).html());
  });
  if (technology.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.exCourse').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.exCourse').css('border', '1px solid green');
    return true;
  }

}

function validateFeesEx(id) {
  var feesFormat = /^[0-9]+$/;
  var fees = $('#' + id).val();
  if (fees === '') {
    $('#errorExFee').addClass('showItem');
    $('#errorExFee').html('Please enter Fees.');
    $('#errorExFee').css('color', 'blue!important');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    var TotalFees = parseInt($('#totalFeesEx').val());
    var PaidFees = parseInt($('#feesAmtEx').val());
    var PendingFees = TotalFees - PaidFees;
    if (PaidFees <= TotalFees) {
      $('#errorExFee').removeClass('showItem');
      $('#errorExFee').addClass('showItem');
      $('#' + id).css('border', '1px solid green');
      $('#errorExFee').html('Pending Fees : ' + PendingFees);
      return true;
    } else {
      $('#errorExFee').removeClass('showItem');
      $('#errorExFee').addClass('showItem');
      $('#errorExFee').html('Pending Fees : ' + PendingFees);
      $('#' + id).css('border', '1px solid red');
    }
  }
}

function validatePaymentDtlEx(id) {
  var PaymentDtl = $('#' + id).val();
  var validatePayDtl = /^[0-9]+$/;
  if (PaymentDtl === '') {
    $('#errorPayDtlEx').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorPayDtlEx').html('Please enter Payment Details');
    $('#' + id).css('border', '1px solid red');

    return false;
  } else {
    if (PaymentDtl.match(validatePayDtl)) {
      $('#errorPayDtlEx').addClass('showItem');
      $('#errorPayDtlEx').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');

      return false;
    } else {
      $('#errorPayDtlEx').removeClass('showItem');
      $('#errorPayDtlEx').addClass('showItem');
      $('#errorPayDtlEx').html(' ');
      $('#' + id).css('border', '1px solid green');

      return true;
    }
  }
}
$('input[type=radio][name=enterFeesTypeEx]').change(function () {
  palymentEx()
});

function palymentEx() {
  var all_input = document.getElementsByClassName('check_inEx');
  for (var i = 0; i < all_input.length; ++i) {
    if (all_input[i].checked == true) {
      var selectedVal = all_input[i].value;
      if (selectedVal == 'cash') {
        $('#id_PaymentModeEx').val(selectedVal)
        $('#feesAmtEx').css({
          'width': '340%'
        });
        $('#feesAmtEx').animate({
          width: '340%',
        });
        $('#idPaymentDtlEx').css('display', 'none');
      } else if (selectedVal == 'cheque') {
        $('#feesAmtEx').animate({
          width: '120%',
        });
        $('#idPaymentDtlEx').css('display', 'block');
        $('#id_PaymentModeEx').val(selectedVal)
        $('#idPaymentDtlEx').animate({
          width: '95%',
        });

      } else if (selectedVal == 'netBanking') {
        $('#id_PaymentModeEx').val(selectedVal)
        $('#feesAmtEx').animate({
          width: '120%',
        });
        $('#idPaymentDtlEx').css('display', 'block');
        $('#idPaymentDtlEx').animate({
          width: '95%',
        });

      } else {}
    } else {

    }
  }

}

function callExUserUpdate() {
  validateTechnologyEx();
  validateFeesEx('feesAmtEx');
  palymentEx();
  if (validateTechnologyEx() && validateFeesEx('feesAmtEx')) {
    if ($('#chequeEx').is(':checked') || $('#netBankingEx').is(':checked')) {
      if (validatePaymentDtlEx('idPaymentDtlEx')) {
        sendExUserUpdate()
      }
    } else {
      sendExUserUpdate()
    }
  } else {
    console.log('Exter User Name or Codekul ID');
  }
}

function sendExUserUpdate() {
  var exUserUrl = protocol + server + ':' + port + appName + '/fees';
  var technologyEx = [];
  var newCourses;
  var today = new Date();
  $.each($('#chCourseListEx option:selected'), function () {
    technologyEx.push($(this).html());
    newCourses = technologyEx.toString();
  });
  var totalF = $('#totalFeesEx').val();
  var courseF = $('#feesAmtEx').val();
  var paindingFees = $('#totalFeesEx').val() - $('#feesAmtEx').val();
  var courses = newCourses;
  var totalFees = $('#totalFeesEx').val();
  var paidFees = $('#feesAmtEx').val();
  var actualFees = $('#feesCoursesEx').val();
  var pendingFees = paindingFees;
  var serviceTaxAmt = $('#idServicesTaxEx').val();
  var paymentMod = $('#id_PaymentModeEx').val();
  var paymentDtl = $('#idPaymentDtlEx').val();
  var paidDate = today.getTime();

  var fees = {
    codekulId: codekulUserID,
    courses: courses,
    totalFees: totalFees,
    totalPaidFees: paidFees,
    actualFees: actualFees,
    pendingFees: pendingFees,
    serviceTaxAmt: serviceTaxAmt,
    paymentMod: paymentMod,
    paymentDtl: paymentDtl,
    paidDate: paidDate,
    enteredBy: localStorage.userName,
  };
  console.log('fees', fees);
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', exUserUrl, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(fees));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        console.log('got success', xhr.responseText);
        $('.multiselect-selected-text').text('Please select technology');
        $('.multiselect').attr('title', 'Please select technology');
        $('#form2')[0].reset();
        document.getElementById('openMsg_Enroll').click();
      } else {
        console.log('got success', xhr.responseText);
      }
    }
  };
}
