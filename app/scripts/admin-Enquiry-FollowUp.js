localStorage.pagePath = '/home.html'
if (localStorage.userName == undefined) {
  window.location.pathname = 'index.html';
} else {
  console.log('localStorage.pagePath =>', localStorage.pagePath)
}


// ------------------------------------------- Document get Ready  -----------------------------------------------
var demoLStatusCH = false;
var demoLStatusAdd = false;
var demoLStatusView = false;
$(function () {
  var currentValue = localStorage.getItem('newContactPage');
  if (!currentValue || currentValue == null || currentValue == '' || currentValue == NaN) {
    var defaultValue = '1';
    localStorage.setItem('newContactPage', defaultValue);
    console.log('Initial newContactPage', localStorage.getItem('newContactPage'));
  }
  else {
    console.log('Not set contact');
  }

  var currentValue1 = localStorage.getItem('newEnquiryPage');

  if (!currentValue1 || currentValue1 == null || currentValue1 == '' || currentValue1 == NaN) {
    var defaultValue = '1';
    localStorage.setItem('newEnquiryPage', defaultValue);
    console.log('Initial newEnquiryPage', localStorage.getItem('newEnquiryPage'));
  }
  else {
    console.log('Not set enq');
  }

  var currentValue1 = localStorage.getItem('newFollowupPage');
  if (!currentValue1 || currentValue1 == null || currentValue1 == '' || currentValue1 == NaN) {
    var defaultValue = '1';
    localStorage.setItem('newFollowupPage', defaultValue);
    console.log('Initial newFollowupPage', localStorage.getItem('newFollowupPage'));
  }
  else {
    console.log('Not set followup');
  }

});
function displayEnquiryCount() {
  var countData;
  var total;
  var handled;
  var notHandled;
  var allEnquiryCount;

  var followUpStartDate = $('#sDate').val();
  var followUpEndDate = $('#eDate').val();

  if (isNaN(followUpStartDate) == isNaN(followUpEndDate)) {
    var startDate = Date.parse($('#sDate').datepicker('getDate'));
    var endDate = Date.parse($('#eDate').datepicker('getDate'));

    if (isNaN(startDate) || isNaN(endDate)) {
      var startDate = new Date();
    
      startDate.setHours(0, 0, 0, 0);
      var startDate  = Date.parse(startDate);
      var endDate = startDate + 86399000;
      var getCountEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/getAllEnquiriesCount?startDate=' + startDate + '&endDate=' + endDate;
    }
    else {
      endDate += 86399000;
      var getCountEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/getAllEnquiriesCount?startDate=' + startDate + '&endDate=' + endDate;
    }
    if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
        try {
          var xhr = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (exception) {
          var xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
      } else {
        var xhr = new XMLHttpRequest();
      }
    } else {
      alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('GET', getCountEnquiryURL, true);
    xhr.send();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          allEnquiryCount = JSON.parse(xhr.responseText);
          handled = allEnquiryCount.handled;
          notHandled = allEnquiryCount.notHandled;
          total = allEnquiryCount.total;
          countData = '<div class="col-lg-2"><strong>Total Enquires : </strong>' +
            total + '</div><div class="col-lg-2"><strong>Handled : </strong>' +
            handled + '</div><div class="col-lg-2"><strong>Not-Handled : </strong>' +
            notHandled + '</div>';
          $('#Count').html(countData);
        }
      }
    }
  }
  else {

    console.log('Enter all values');
    if (followUpStartDate === '') {
      $('#sDate').css('border-bottom', '1px solid red');

    } else {
      $('#sDate').css('border-bottom', '1px solid green');
    }

    if (followUpEndDate === '') {
      $('#eDate').css('border-bottom', '1px solid red');
    } else {
      $('#eDate').css('border-bottom', '1px solid green');
    }

  }

}


function displayFollowupCount() {

  var countData;
  var allFollowupCount;

  var followUpStartDate = $('#sDate').val();
  var followUpEndDate = $('#eDate').val();

  if (isNaN(followUpStartDate) == isNaN(followUpEndDate)) {
    var startDate = Date.parse($('#sDate').datepicker('getDate'));
    var endDate = Date.parse($('#eDate').datepicker('getDate'));

    if (isNaN(startDate) || isNaN(endDate)) {
//      var startDate = new Date();
      var startDate = new Date();
      startDate.setHours(0, 0, 0, 0);
      var startDate  = Date.parse(startDate);
      var endDate = startDate + 86399000;
      var getCountFollowupURL = protocol + server + ':' + port + appName + '/inquiry/getAllFollowUpCount?startDate=' + startDate + '&endDate=' + endDate;
    }
    else {
      endDate += 86399000;
      var getCountFollowupURL = protocol + server + ':' + port + appName + '/inquiry/getAllFollowUpCount?startDate=' + startDate + '&endDate=' + endDate;
    }

    if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
        try {
          var xhr = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (exception) {
          var xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
      } else {
        var xhr = new XMLHttpRequest();
      }
    } else {
      alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('GET', getCountFollowupURL, true);
    xhr.send();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          allFollowupCount = JSON.parse(xhr.responseText);
          var followUpStatusEnrolled = allFollowupCount.followUpStatusEnrolled;
          var followUpStatusFake = allFollowupCount.followUpStatusFake;
          var followUpStatusNotInterested = allFollowupCount.followUpStatusNotInterested;
          var followUpStatusDuplicate = allFollowupCount.followUpStatusDuplicate;
          countData = '<div class="col-lg-2"><strong>Enrolled Followup : </strong>' +
            followUpStatusEnrolled + '</div><div class="col-lg-2"><strong>Fake Followup : </strong>' +
            followUpStatusFake + '</div><div class="col-lg-3"><strong>Not-Interested Followup : </strong>' +
            followUpStatusNotInterested + '</div><div class="col-lg-2"><strong>Duplicate Followup : </strong>' +
            followUpStatusDuplicate + '</div>';
          $('#Count').html(countData);
        }
      }
    }
  }
  else {

    console.log('Enter all values');
    if (followUpStartDate === '') {
      $('#sDate').css('border-bottom', '1px solid red');

    } else {
      $('#sDate').css('border-bottom', '1px solid green');
    }

    if (followUpEndDate === '') {
      $('#eDate').css('border-bottom', '1px solid red');
    } else {
      $('#eDate').css('border-bottom', '1px solid green');
    }

  }

}
// ------------------------------------------- Document get Ready  -----------------------------------------------
var demoLStatusCH = false;
var demoLStatusAdd = false;
var demoLStatusView = false;
$(function () {
  $('#demoStatus').click(function () {
    if ($(this).is(':checked')) {
      demoLStatusCH = $(this).is(':checked');
    } else {
    }
  });
  $('#addDemoStatus').click(function () {
    if ($(this).is(':checked')) {
      demoLStatusAdd = $(this).is(':checked');
    } else {
      demoLStatusAdd = $(this).is(':checked');
    }
  });
  $('#viewDemoStatus').click(function () {
    if ($(this).is(':checked')) {
      demoLStatusView = $(this).is(':checked');

    } else {
    }
  });
});
function setDefaultDate(){
  var searchFollowup = $('#searchFollowup').val();
  var startDate = Date.parse($('#sDate').datepicker('getDate'));
  var endDate = Date.parse($('#eDate').datepicker('getDate'));
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = '0' + dd
  }
  if (mm < 10) {
    mm = '0' + mm
  }
  today = dd + '-' + mm + '-' + yyyy;
  
  $( "#sDate" ).val(today);
  $( "#eDate" ).val(today);
}

$(function () {
  setDefaultDate();
  var clickEve = 'pageLoad';
  getAllEnquiry(clickEve);
  $('#userNameLogin').text(localStorage.userName);
  $('#userRoleLogin').text(localStorage.userRole);
  $('#userEmailLogin').text(localStorage.userEmail);
  $('#followUp').html('');
  $('#enquiry').html('');
  $('#chCourseList').multiselect({
    maxHeight: 150,
    buttonWidth: '100%',
    nonSelectedText: 'Please select Course',
  });
  $('#chCourseList2').multiselect({
    maxHeight: 150,
    buttonWidth: '100%',
    nonSelectedText: 'Please select Course',
  });
  $('#chCourseListNew').multiselect({
    maxHeight: 150,
    buttonWidth: '100%',
    nonSelectedText: 'Please select Course',
  });
  // --------------------------------------- get allCourses --------------------------------------------
  var getCoursesUrl = protocol + server + ':' + port + appName + '/allCourses';
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getCoursesUrl, true);
  xhr.send();
  var courseList;
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        courseList = JSON.parse(xhr.responseText);
        var inHTML = '';
        $.each(courseList, function (index, value) {
          var val = value.courseName;
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseList').append(htm);
          $('#chCourseList').multiselect('rebuild');
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseList2').append(htm);
          $('#chCourseList2').multiselect('rebuild');
          var htm = '';
          htm += '<option value ="' + index + '">' + val + '</option>';
          $('#chCourseListNew').append(htm);
          $('#chCourseListNew').multiselect('rebuild');
        });
      } else {
        console.log('start web services', xhr.statusText)
      }
    }
  };

  // --------------------------------------- Close get allCourses --------------------------------------------
});

$(function () {
  var today = new Date();
  $('#datePickerNew').datepicker({
    dateFormat: 'dd-mm-yy',
    minDate: today
  });

  $('#datePickerAdd').datepicker({
    dateFormat: 'dd-mm-yy',
    minDate: today
  });

  $('#mydate').datepicker({
    dateFormat: 'dd-mm-yy'
  });

  $('#datePickerView').datepicker({
    dateFormat: 'dd-mm-yy',
    minDate: today
  });
  $('#sDate').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#eDate').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#datepickerS').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#datepickerS1').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $('#datepickerE').datepicker({
    dateFormat: 'dd-mm-yy'
  });
});

$(function () {
  $('[name="dataSelect"]').change(function () {
    if ($(this).is(':checked')) {
      $('#addFollowUpForm :input').attr('disabled', true);
    } else {
      $('#addFollowUpForm :input').attr('disabled', false);
    };
  });
});

$(function () {
  $('[name="dataSelect2"]').change(function () {
    if ($(this).is(':checked')) {
      $('#addFollowUpForm2 :input').attr('disabled', true);
    } else {
      $('#addFollowUpForm2 :input').attr('disabled', false);
    };
  });
});

// ------------------------------------------- Crate New Enroll And FollowUp  -----------------------------------------------
$('#btnCrateNewFollow').click(function () {
  $('#modal-3').modal('show');
  $('#addEnquiryFollowUpForm')[0].reset();
  $('.courseList').css('border', 'none');
  $('.multiselect-selected-text').text('Please select ');
  $('.multiselect').attr('title', 'Please select ');
  $('form#addEnquiryFollowUpForm :input').each(function () {
    var input = $(this);
    $(this).css('border', '1px solid #aaa');
  });
  $(document).keyup(function (e) {
    if (e.keyCode == 27) { // esc keycode
      $('#modal-3').modal('hide');
    }
  });
});
function validateFullName(id) {
  var FName = $('#' + id).val();
  var validateName = /^[0-9]+$/;
  if (FName === '') {
    $('#errorFName').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFName').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');

    return false;
  } else {
    if (FName.match(validateName)) {
      $('#errorFName').addClass('showItem');
      $('.errorMessage').css('color', 'red');
      $('#errorFName').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      $(id).css('display', 'none');
      return false;
    } else {
      $('#errorFName').removeClass('showItem');
      $('#errorFName').addClass('showItem');
      $('#errorFName').html(' ');
      $('#' + id).css('border', '1px solid green');
      $(id).css('display', 'none');
      return true;
    }
  }
}
function validateEmail(id) {
  var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var emailID = $('#' + id).val();
  if (emailID === '') {
    $('#errorEmail').addClass('showItem');
    $('#errorEmail').html('Please enter email');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (emailID.match(mailformat)) {
      $('#errorEmail').removeClass('showItem');
      $('#errorEmail').html(' ');
      $('#' + id).css('border', '1px solid green');
      return true;
    } else {
      $('#errorEmail').addClass('showItem');
      $('#errorEmail').html('Please enter valid mail ID');
      $('#' + id).css('border', '1px solid red');
      return false;
    }
  }
}
function validateMobileNo(id) {
  var phNoFormat = /^\d{10}$/;
  var mobileNo = $('#' + id).val();
  if (mobileNo === '') {
    $('#errorMobileNo').addClass('showItem');
    $('#errorMobileNo').html('Please enter mobile no.');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (mobileNo.length == 10) {
      if (mobileNo.match(phNoFormat)) {
        $('#errorMobileNo').removeClass('showItem');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html(' ');
        $('#' + id).css('border', '1px solid green');
        return true;
      } else {
        $('#errorMobileNo').removeClass('showItem');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html(' ');

        $('#' + id).css('border', '1px solid green');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html('Please enter valid mobile no.');
        $('#' + id).css('border', '1px solid red');
        return false;
      }
    } else {
      $('#errorMobileNo').removeClass('showItem');
      $('#errorMobileNo').addClass('showItem');
      $('#errorMobileNo').html('Please enter valid mobile no.');
      $('#' + id).css('border', '1px solid red');
    }
  }
}
function validateLocations(id) {
  var place = $('#' + id).val();
  var validatePlace = /^[0-9]+$/;
  if (place === '') {
    $('#errorFName').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFName').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    if (place.match(validatePlace)) {
      $('#errorFName').addClass('showItem');
      $('.errorMessage').css('color', 'red');
      $('#errorFName').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      $(id).css('display', 'none');
      return false;
    } else {
      $('#errorFName').removeClass('showItem');
      $('#errorFName').addClass('showItem');
      $('#errorFName').html(' ');
      $('#' + id).css('border', '1px solid green');
      $(id).css('display', 'none');
      return true;
    }
  }
}
function validateEnquiryType(id) {
  var EnrollType = $('#' + id).val();
  var validateName = /^[0-9]+$/;
  if (EnrollType === '') {
    $('#errorEnrollType').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorEnrollType').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorEnrollType').removeClass('showItem');
    $('#errorEnrollType').addClass('showItem');
    $('#errorEnrollType').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateFollowUpType(id) {
  var FollowUpType = $('#' + id).val();

  if (FollowUpType === '') {
    $('#errorFollowUpType').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFollowUpType').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorFollowUpType').removeClass('showItem');
    $('#errorFollowUpType').addClass('showItem');
    $('#errorFollowUpType').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateBatchType(id) {
  var BatchType = $('#' + id).val();

  if (BatchType === '') {
    $('#errorBatchType').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorBatchType').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorBatchType').removeClass('showItem');
    $('#errorBatchType').addClass('showItem');
    $('#errorBatchType').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateCourses() {
  var technology = [];
  $.each($('#chCourseListNew option:selected'), function () {
    technology.push($(this).html());
  });
  if (technology.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.courseList').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.courseList').css('border', '1px solid green');
    return true;
  }

}
function validateCommentMsg(id) {
  var CommentMsg = $('#' + id).val();
  var trimMsg = $.trim(CommentMsg);
  if (trimMsg >= 0) {
    $('#errorCommentMsg').addClass('showItem');
    $('.errorCommentMsg').css('color', 'red');
    $('#errorCommentMsg').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorCommentMsg').removeClass('showItem');
    $('#errorCommentMsg').addClass('showItem');
    $('#errorCommentMsg').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateDatePicker(id) {
  var DatePicker = $('#' + id).val();

  if (DatePicker === '') {
    $('#errorDatePickerNew').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorDatePickerNew').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorDatePickerNew').removeClass('showItem');
    $('#errorDatePickerNew').addClass('showItem');
    $('#errorDatePickerNew').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}

$('#createNewEnqFoll').click(function () {
 
  var ref_this = $('ul.mynavlist li a.active');
  var selectedTab = ref_this.data('id');

  validateFullName('fullName');
  validateEmail('email');
  validateMobileNo('mobileNo');
  validateLocations('location');
  validateEnquiryType('enquiryType');
  validateFollowUpType('followUpType');
  validateBatchType('batchType');
  validateCourses();
  validateCommentMsg('commentMsg');
  validateDatePicker('datePickerNew');
  if (validateFullName('fullName') && validateEmail('email') && validateMobileNo('mobileNo') &&
    validateEnquiryType('enquiryType') && validateFollowUpType('followUpType') && validateBatchType('batchType') &&
    validateCourses() && validateCommentMsg('commentMsg') && validateDatePicker('datePickerNew')) {
      $(this).val('test...').prop('disabled', true);
      newEnquiryFollowUp();
   
  } else {
    console.log('Check All Filled');
  }
});
function newEnquiryFollowUp(selectedTab) {
  NProgress.start();
  
    var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/addNewFollowUp';
  var fullName = $('#fullName').val();
  var email = $('#email').val();
  var mobNo = $('#mobileNo').val();
  var location = $('#location').val();
  var enquiryType = 'codekul Admin panel';
  var followUpType = $('#followUpType').val();
  var batchType = $('#batchType').val();
  var commentMsg = $('#commentMsg').val();
  var clientIPNo = $('#clientIPNo').val();
  var datePickerEnroll = $('#datePickerNew').datepicker('getDate');
  var newDatePickerEnroll = datePickerEnroll.getTime();
  var technology = [];
  var courses;
  var fromPage = window.location.pathname;
   $.each($('#chCourseListNew option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });
  var newEnquiry = {
    applicantName: fullName,
    eMail: email,
    courses: technology,
    mobileNo: mobNo,
    inquiryType: enquiryType,
    clientIp: clientIPNo,
    fromPage: fromPage,
    enteredBy: localStorage.userName,
    enquiryHandle: false,
    tenant: 'codekul admin panel home page',
    demoLectureStatus: demoLStatusCH,
    message: commentMsg,
    locality: location,
    batchType: batchType,
    nextFollowUpDate: newDatePickerEnroll,
    followUpType: followUpType,
  };
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', addFollowUpURL, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(newEnquiry));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        // alert('Enquiry added...!!!');
        $('#alertText').html('Enquiry added...!!!');
        $('#myModal').modal('show');
        NProgress.done();
        $('#createNewEnqFoll').prop('disabled', false);
        var clickEve = 'followup';
        getAllEnquiry(clickEve);
        demoLStatusCH = false;
        $('#addEnquiryFollowUpForm')[0].reset();
        $('.courseList').css('border', 'none');
        $('.multiselect-selected-text').text('Please select ');
        $('.multiselect').attr('title', 'Please select ');
        $('form#addEnquiryFollowUpForm :input').each(function () {
          var input = $(this);
          $(this).css('border', '1px solid #aaa');
        });
        $('#modal-3').modal('hide');
      } else {
        console.log('xhr.statusText', xhr.statusText);
      }
    }
  };
}
// ------------------------------------------- Close Crate New Enroll And FollowUp  -----------------------------------------------

// ------------------------------------------- Enquiry Page FollowUp  -----------------------------------------------

function validateEmailAdd(id) {
  var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var emailID = $('#' + id).val();
  if (emailID === '') {
    $('#errorEmail').addClass('showItem');
    $('#errorEmail').html('Please enter email');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (emailID.match(mailformat)) {
      $('#errorEmail').removeClass('showItem');
      $('#errorEmail').html(' ');
      $('#' + id).css('border', '1px solid green');
      return true;
    } else {
      $('#errorEmail').addClass('showItem');
      $('#errorEmail').html('Please enter valid mail ID');
      $('#' + id).css('border', '1px solid red');
      return false;
    }
  }
}
function validateMobileNoAdd(id) {
  var phNoFormat = /^\d{10}$/;
  var mobileNo = $('#' + id).val();
  if (mobileNo === '') {
    $('#errorMobileNo').addClass('showItem');
    $('#errorMobileNo').html('Please enter mobile no.');
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    if (mobileNo.length == 10) {
      if (mobileNo.match(phNoFormat)) {
        $('#errorMobileNo').removeClass('showItem');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html(' ');
        $('#' + id).css('border', '1px solid green');
        return true;
      } else {
        $('#errorMobileNo').removeClass('showItem');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html(' ');

        $('#' + id).css('border', '1px solid green');
        $('#errorMobileNo').addClass('showItem');
        $('#errorMobileNo').html('Please enter valid mobile no.');
        $('#' + id).css('border', '1px solid red');
        return false;
      }

    } else {
      $('#errorMobileNo').removeClass('showItem');
      $('#errorMobileNo').addClass('showItem');
      $('#errorMobileNo').html('Please enter valid mobile no.');
      $('#' + id).css('border', '1px solid red');
    }

  }

}
function validateLocationsAdd(id) {
  var placeAdd = $('#' + id).val();
  var validatePlaceAdd = /^[0-9]+$/;
  if (placeAdd === '') {
    $('#errorFName').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFName').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    if (placeAdd.match(validatePlaceAdd)) {
      $('#errorFName').addClass('showItem');
      $('.errorMessage').css('color', 'red');
      $('#errorFName').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      $(id).css('display', 'none');
      return false;
    } else {
      $('#errorFName').removeClass('showItem');
      $('#errorFName').addClass('showItem');
      $('#errorFName').html(' ');
      $('#' + id).css('border', '1px solid green');
      $(id).css('display', 'none');
      return true;
    }
  }
}
function validateFollowUpTypeAdd(id) {
  var FollowUpType = $('#' + id).val();
  if (FollowUpType === '') {
    $('#errorFollowUpType').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFollowUpType').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorFollowUpType').removeClass('showItem');
    $('#errorFollowUpType').addClass('showItem');
    $('#errorFollowUpType').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateBatchTypeAdd(id) {
  var BatchType = $('#' + id).val();

  if (BatchType === '') {
    $('#errorBatchTypeAdd').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorBatchTypeAdd').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorBatchTypeAdd').removeClass('showItem');
    $('#errorBatchTypeAdd').addClass('showItem');
    $('#errorBatchTypeAdd').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateCoursesAdd() {
  var technology = [];
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
  });
  if (technology.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.courseListAdd').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.courseListAdd').css('border', '1px solid green');
    return true;
  }

}
function validateCommentMsgAdd(id) {
  var CommentMsgAdd = $('#' + id).val();
  var trimMsg1 = $.trim(CommentMsgAdd);
  if (trimMsg1 >= 0) {
    $('#errorCommentMsgAdd').addClass('showItem');
    $('.errorCommentMsg').css('color', 'red');
    $('#errorCommentMsgAdd').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorCommentMsgAdd').removeClass('showItem');
    $('#errorCommentMsgAdd').addClass('showItem');
    $('#errorCommentMsgAdd').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateDatePickerAdd(id) {
  var DatePicker = $('#' + id).val();

  if (DatePicker === '') {
    $('#' + id).css('border', '1px solid red');
    return false;
  } else {
    $('#errorDatePickerAdd').removeClass('showItem');
    $('#errorDatePickerAdd').html(' ');
    $('#' + id).css('border', '1px solid green');
    return true;
  }

}
$('#createEnqFollowUp').click(function () {
  validateEmailAdd('emailAdd');
  validateMobileNoAdd('mobileAdd')
  validateLocationsAdd('locationAdd');
  validateFollowUpTypeAdd('followUpTypeAdd');
  validateBatchTypeAdd('batchTypeAdd');
  validateCoursesAdd();
  validateCommentMsgAdd('commentMsgAdd');
  validateDatePickerAdd('datePickerAdd');
  if (validateFollowUpTypeAdd('followUpTypeAdd') && validateBatchTypeAdd('batchTypeAdd') && validateCoursesAdd() &&
    validateCommentMsgAdd('commentMsgAdd') && validateDatePickerAdd('datePickerAdd') && validateLocationsAdd('locationAdd')) {
    addFollowUp();
  } else {
    console.log('Check All Filled');
  }
});
function addFollowUp() {
  var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/addFollowUp';
  var mobileNoAdd = $('#mobileAdd').val();
  var emailAdd = $('#emailAdd').val();
  var locationAdd = $('#locationAdd').val();
  var followUpType = $('#followUpTypeAdd').val();
  var batchType = $('#batchTypeAdd').val();

  $('#addDemoStatus').click(function () {
    if ($(this).is(':checked')) {
      demoLStatusAdd = true;
    } else {
      demoLStatusAdd = false;
    }
  });

  var technology = [];
  var courses;
  $.each($('#chCourseList option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });
  console.log('Id : ',viewSeletedRowData.id);
  var commentMsg = $('#commentMsgAdd').val();
  var nxtFollowUpdate = $('#datePickerAdd').datepicker('getDate');
  var dateNxtFollowUp = nxtFollowUpdate.getTime();
 var fullName = $('#uname').val();
  var followUpSend = {
   applicantName: fullName,
    eMail: emailAdd,
    mobileNo: mobileNoAdd,
    locality: locationAdd,
    followUpType: followUpType,
    batchType: batchType,
    addCourses: technology,
    comment: commentMsg,
    nextFollowUpDate: dateNxtFollowUp,
    demoLectureStatus: demoLStatusAdd,
    enteredBy: localStorage.userName,
    enquiries: {
      id: viewSeletedRowData.id
    },
  };
  console.log('followupSend',followUpSend);
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', addFollowUpURL, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(followUpSend));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        // alert('Followup added...!!!');
        $('#alertText').html('Followup added...!!!');
        $('#myModal').modal('show');
        $('.multiselect-selected-text').text('Please select  ');
        $('.multiselect').attr('title', 'Please select  ');
        $('#modal-1').modal('hide');

        var ref_this = $('ul.mynavlist li a.active');
        var selectedTab = ref_this.data('id');

        if (selectedTab == 1) {
          
          paginationFunction();
        }
        else if (selectedTab == 2) {
          getAllEnquiryContactPage();
        }
        demoLStatusAdd = false;
        $('.courseListAdd').css('border', 'none');
        $('form#addFollowUpForm :input').each(function () {
          var input = $(this);
          $(this).css('border', '1px solid #aaa');
        });
      } else {
        console.log('xhr.statusText', xhr.statusText);
      }
    }
  };
}
// ------------------------------------------- Close Enquiry Page FollowUp  -----------------------------------------------

// -------------------------------------------   Enquiry Contact Page    -----------------------------------------------


function getAllEnquiryContactPage() {
  $('#Count').html('');
  $('#PageView').css('display', 'block');
  $('#pagination').twbsPagination('destroy');
  paginationInquiryContact();
};
var paginationFunctionContact = function () {
  NProgress.start();
  var $pagination = $('#pagination');
  var page = $pagination.twbsPagination('getCurrentPage');
  var size;
  size = 10;
  var pageData = parseInt(localStorage.getItem('newContactPage')) - 1;

  var getAllEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/getInquiry/contact/' + pageData + '/' + size;
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getAllEnquiryURL, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        $('#btnLebel').css('display', 'block');
        $('#followUp').html('');
        $('#enqContact').html('');
        $('#btnFollow').addClass('disabled');
        $('#btnFollow').css('display', 'block');
        $('#dateContainer').css('display', 'none');
        //$('#dateContainer').css('display', 'block');
        $('#btnCrateNewFollow').css('display', 'none');
        NProgress.done();
        allEnquiry = JSON.parse(xhr.responseText);
        var listOfPage = allEnquiry.content
        var count = 0;
        var no = 1;

        var enqTable =
          '<div class=\'row tblHeading\' >' +
          '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Date </div>' +
          '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Name </div>' +
          '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Message </div>' +
          '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Mobile No. </div>' +
          '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Email Id </div>' +
          '<div class=\'col-lg-3 \'> ' +
          '<div class=\'row \'>' +
          '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> From Page </div>' +
          '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> Enquiry Type </div>' +
          '</div> ' +
          '</div>' +
          '<div class=\'col-lg-1 \'> </div>' +
          '</div>' +
          '<div class=\'col-lg-12  \' id=\'tbldataContact\'   style="height: 360px;overflow-y: auto;padding:0px;  overflow-style: marquee,panner;" >';
        $('#enqContact').append(enqTable);

        if(jQuery.isEmptyObject(listOfPage))
        {
          var markup =
          '<div class=\'row  \' style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\'>' +
          '<div class=\'col-lg-12 \' style=\'text-align:center;\'><strong>No data found</strong></div>' +
          '</div> ';
          $('#tbldataContact').append(markup);
        } 
        

        $.each(listOfPage, function (index, enquiry) {
          count++;

          var string = enquiry.fromPage;
          var result = string.split('/');
          var final = result[result.length - 1];
          var previous = result.join('/');

          var id = final;
          var arr = id.split('-');
          var FromPageTech = arr[0];


          var enqToday = new Date(enquiry.inquiryTimestamp);
          var dd = enqToday.getDate();
          var mm = enqToday.getMonth() + 1; //January is 0!
          var yyyy = enqToday.getFullYear();
          if (dd < 10) {
            dd = '0' + dd
          }
          if (mm < 10) {
            mm = '0' + mm
          }
          enqToday = dd + '-' + mm + '-' + yyyy;
          var markup = '<div class=\'row  \' id=\'rowId' + index +
            '\'style=" width: 100%;  margin: 0 auto;  border-bottom: 1px solid rgba(170, 170, 170, 0.53); padding: 7px 0px;" >' +
            '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> ' + enqToday +
            '</div>' +
            '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.applicantName +
            '</div>' +
            '<div class=\'col-lg-2 \' id=\'tool' + enquiry.id + '\' style="word-wrap: break-word; width: 60%; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;"  data-toggle="tooltip" data-placement="right" title=""> ' + enquiry.massage +
            '</div>' +
            '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.mobileNo +
            '</div>' +
            '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.eMail +
            '</div>' +
            '<div class=\'col-lg-3 \' style="word-wrap: break-word;">' +
            '<div class=\'row\' >' +
            '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + FromPageTech + '</div>' +
            '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + enquiry.inquiryType + '</div>' +
            '</div> ' +
            '</div>' +
            '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;text-align: left;">' +
            '<a onClick=\'clickAddFolloupModel("' + enquiry.id + '","' + index + '");\' style=" color:#07639d;    cursor: pointer;"> <u> Add Follow Up   </u></a> ' +
            '</div>' +
            '</div> ';
          $('#tbldataContact').append(markup);
          var element = $('#tool' + enquiry.id);

          if (element[0].outerText.length > 30) {
            $('#tool' + enquiry.id).attr('title', enquiry.massage);
          }
        });
      }
    }
  }
}
function paginationInquiryContact() {
  if($('#pagination').data("twbs-pagination")){
    $('#pagination').twbsPagination('destroy');
}
NProgress.start();
  var $pagination = $('#pagination');
  var pageData = 0;
  var defaultOpts = {
    totalPages: 20
  };
  var obj = $('#pagination').twbsPagination({
    onPageClick: function (event, page) {
      var size;
      size = 10;
      localStorage.setItem('newContactPage', page);
      var pageData = parseInt(localStorage.getItem('newContactPage')) - 1;
      var getAllEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/getInquiry/contact/' + pageData + '/' + size;
      if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
          try {
            var xhr = new ActiveXObject('Msxml2.XMLHTTP');
          } catch (exception) {
            var xhr = new ActiveXObject('Microsoft.XMLHTTP');
          }
        } else {
          var xhr = new XMLHttpRequest();
        }
      } else {
        alert('Your browser does not support XMLHTTP Request...!');
      }
      xhr.open('GET', getAllEnquiryURL, true);
      xhr.send();
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            $('#btnLebel').css('display', 'block');
            $('#followUp').html('');
            $('#enqContact').html('');
            $('#btnFollow').addClass('disabled');
            $('#btnFollow').css('display', 'block');
            $('#dateContainer').css('display', 'none');
            $('#btnCrateNewFollow').css('display', 'none');
            NProgress.done();
            allEnquiry = JSON.parse(xhr.responseText);
            if(allEnquiry.totalPages == 0)
            {
              totalPages = 1;
              currentPage = 1;
            }
            else{
              totalPages = allEnquiry.totalPages;
              listOfPage = allEnquiry.content
              var currentPage = $pagination.twbsPagination('getCurrentPage');
              console.log('newVarPage', currentPage);
            }
            $pagination.twbsPagination('destroy');
            $pagination.twbsPagination($.extend({}, defaultOpts, {
              totalPages: totalPages,
              startPage: currentPage,
              initiateStartPageClick: false,
              onPageClick: paginationFunctionContact
            }));
            var count = 0;
            var no = 1;
            var enqTable =
              '<div class=\'row tblHeading\' >' +
              '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Date </div>' +
              '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Name </div>' +
              '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Message </div>' +
              '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Mobile No. </div>' +
              '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Email Id </div>' +
              '<div class=\'col-lg-3 \'> ' +
              '<div class=\'row \'>' +
              '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> From Page </div>' +
              '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> Enquiry Type </div>' +
              '</div> ' +
              '</div>' +
              '<div class=\'col-lg-1 \'> </div>' +
              '</div>' +
              '<div class=\'col-lg-12  \' id=\'tbldataContact\'   style="height: 360px;overflow-y: auto;padding:0px;  overflow-style: marquee,panner;" >';
            $('#enqContact').append(enqTable);
            if(jQuery.isEmptyObject(listOfPage))
            {
              var markup =
              '<div class=\'row  \' style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\'>' +
              '<div class=\'col-lg-12 \' style=\'text-align:center;\'><strong>No data found</strong></div>' +
              '</div> ';
              $('#tbldataContact').append(markup);
            } 
          
            $.each(listOfPage, function (index, enquiry) {
              count++;
              var string = enquiry.fromPage;
              var result = string.split('/');
              var final = result[result.length - 1];
              var previous = result.join('/');
              var websitePage = result[2];
              var id = final;
              var arr = id.split('-');
              var FromPageTech = arr[0];
              var enqToday = new Date(enquiry.inquiryTimestamp);
              var dd = enqToday.getDate();
              var mm = enqToday.getMonth() + 1; //January is 0!
              var yyyy = enqToday.getFullYear();
              if (dd < 10) {
                dd = '0' + dd
              }
              if (mm < 10) {
                mm = '0' + mm
              }
              enqToday = dd + '-' + mm + '-' + yyyy;
              var markup = '<div class=\'row  \' id=\'rowId' + index +
                '\'style=" width: 100%;  margin: 0 auto;  border-bottom: 1px solid rgba(170, 170, 170, 0.53); padding: 7px 0px;" >' +
                '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> ' + enqToday +
                '</div>' +
                '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.applicantName +
                '</div>' +
                '<div class=\'col-lg-2 \' id=\'tool' + enquiry.id + '\' style="word-wrap: break-word; width: 60%; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;"  data-toggle="tooltip" data-placement="right" title=""> ' + enquiry.massage +
                '</div>' +
                '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.mobileNo +
                '</div>' +
                '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.eMail +
                '</div>' +
                '<div class=\'col-lg-3 \' style="word-wrap: break-word;">' +
                '<div class=\'row\' >' +
                '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + websitePage + '</div>' +
                '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + enquiry.inquiryType + '</div>' +
                '</div> ' +
                '</div>' +
                '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;text-align: left;">' +
                '<a onClick=\'clickAddFolloupModel("' + enquiry.id + '","' + index + '");\' style=" color:#07639d;    cursor: pointer;"> <u> Add Follow Up   </u></a> ' +
                '</div>' +
                '</div> ';
              $('#tbldataContact').append(markup);
              var element = $('#tool' + enquiry.id);

              if (element[0].outerText.length > 30) {
                $('#tool' + enquiry.id).attr('title', enquiry.massage);
              }
            });
          } else {
            document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status +
              'Error Message: ' + xhr.statusText;
            alert('Not');
          }
        }
      };
    },
    totalPages: totalPages,
    visiblePages: 5,
  });
}

// ------------------------------------------- Close Enquiry Contact Page   -----------------------------------------------

// -------------------------------------------   Get Enquiry Page    -----------------------------------------------
var AllEnquiry;
var allEnquiry;
var listOfPage;
var allInquiry;
var totalPages = 35;
function getAllEnquiry(clickEve) {
  
 if(clickEve != 'OK'){
   setDefaultDate();
   $('#searchFollowup').val('');
 }
  $('#PageView').css('display', 'block');
  $('#pagination').twbsPagination('destroy');
  paginationInquiry();
};

var paginationFunction = function () {

  displayEnquiryCount();
  NProgress.start();
  var $pagination = $('#pagination');
  var newPageData = 3

  var page = $pagination.twbsPagination('getCurrentPage');
  localStorage.setItem('newEnquiryPage', page);
  var size;
  size = 10;
  var pageData = page - 1;
  console.log('page', page)
  console.log('  pagination   ',
    $('#pagination').twbsPagination($('#txtPage').val())
  )

  $('#pagination').twbsPagination({
    totalPages: 10
  });

  var pageData = parseInt(localStorage.getItem('newEnquiryPage')) - 1;

  var followUpStartDate = $('#sDate').datepicker('getDate');
  var followUpEndDate = $('#eDate').datepicker('getDate');
  if (isNaN(followUpStartDate)) {
    var startDate = followUpStartDate.getTime();
  }
  else {
    var startDate = 0;
  }
  if (isNaN(followUpEndDate)) {
    var endDate = followUpEndDate.getTime();
  }
  else {
    var endDate = 0;

  }
  if (startDate <= endDate) {
    var searchName = $('#searchFollowup').val();
    var searchFollowup = $('#searchFollowup').val();
    var startDate = Date.parse($('#sDate').datepicker('getDate'));
    var endDate = Date.parse($('#eDate').datepicker('getDate'));
    if (isNaN(startDate) || isNaN(endDate)) {
      var startDate = new Date();
      startDate.setHours(0, 0, 0, 0);
      var startDate  = Date.parse(startDate);
      var endDate = startDate + 86399000;
      var getAllEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/enquirySearch/?page=' + pageData + '&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyChar=' + searchFollowup;
    }
    else {
      endDate += 86399000;
      var getAllEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/enquirySearch/?page=' + pageData + '&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyChar=' + searchFollowup;
    }

    if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
        try {
          var xhr = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (exception) {
          var xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
      } else {
        var xhr = new XMLHttpRequest();
      }
    } else {
      alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('GET', getAllEnquiryURL, true);
    xhr.send();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          $('#btnLebel').css('display', 'block');
          $('#followUp').html('');
          $('#enqContact').html('');
          $('#btnFollow').addClass('disabled');
          $('#btnFollow').css('display', 'block');
          $('#dateContainer').css('display', 'block');
          $('#btnCrateNewFollow').css('display', 'block');
          NProgress.done();
          allEnquiry = JSON.parse(xhr.responseText);
          console.log('allEnq', allEnquiry);
          var listOfPage = allEnquiry.content
          var count = 0;
          var no = 1;

          var enqTable =
            '<div class=\'row tblHeading\' >' +
            '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Date </div>' +
            '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Name </div>' +
            '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Courses </div>' +
            '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Mobile No. </div>' +
            '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Email Id </div>' +
            '<div class=\'col-lg-3 \'>' +
            ' <div class=\'row \'> ' +
            '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> From Page </div> ' +
            '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> Enquiry Type </div> ' +
            '</div> ' +
            '</div>' +
            '<div class=\'col-lg-1 \'> </div>' +
            '</div>' +
            '<div class=\'col-lg-12  \' id=\'tbldata1\'   style="height: 360px;overflow-y: auto;padding:0px;  overflow-style: marquee,panner;" >';
          $('#enquiry').append(enqTable);
          if(jQuery.isEmptyObject(listOfPage))
          {
            var markup =
            '<div class=\'row  \' style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\'>' +
            '<div class=\'col-lg-12 \' style=\'text-align:center;\'><strong>No data found</strong></div>' +
            '</div> ';
            $('#enquiry').append(markup);
          } 
          
          $.each(listOfPage, function (index, enquiry) {
            var string = enquiry.fromPage;
            var FromPageTech = string;

            count++;
            $('#numbers').html('<li>' + count + '</li>');
            var enqToday = new Date(enquiry.inquiryTimestamp);
            var dd = enqToday.getDate();
            var mm = enqToday.getMonth() + 1; //January is 0!
            var yyyy = enqToday.getFullYear();
            if (dd < 10) {
              dd = '0' + dd
            }
            if (mm < 10) {
              mm = '0' + mm
            }
            enqToday = dd + '-' + mm + '-' + yyyy;

            var markup = '<div class=\'row  \' id=\'rowId' + index +
              '\'style=" width: 100%;  margin: 0 auto;  border-bottom: 1px solid rgba(170, 170, 170, 0.53); padding: 7px 0px;" >' +
              '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> ' + enqToday +
              '</div>' +
              '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.applicantName +
              '</div>' +
              '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.courses +
              '</div>' +
              '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.mobileNo +
              '</div>' +
              '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.eMail +
              '</div>' +
              '<div class=\'col-lg-3 \' style="word-wrap: break-word;">' +
              '<div class=\'row\' >' +
              '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + FromPageTech + '</div>' +
              '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + enquiry.inquiryType + '</div>' +
              '</div> ' +
              '</div>' +
              '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;text-align: left;">' +
              '<a onClick=\'clickAddFolloupModel("' + enquiry.id + '","' + index + '");\' style=" color:#07639d;    cursor: pointer;"> <u> Add Follow Up </u></a> ' +
              '</div>' +
              '</div> ';
            $('#tbldata1').append(markup);

          });
        }
      }
    };
  } else {
    alert('Start Date is bigger than End date');
  }

  $('#btnLebel').css('display', 'block');
  $('#followUp').html('');
  $('#enquiry').html('');
  $('#btnFollow').addClass('disabled');
  $('#btnFollow').css('display', 'block');
  $('#dateContainer').css('display', 'block');
  $('#btnCrateNewFollow').css('display', 'block');

}
var paginationFunctionFollowup = function () {
  displayFollowupCount();
  NProgress.start();
  var $pagination = $('#pagination');

  var page = $pagination.twbsPagination('getCurrentPage');
  localStorage.setItem('newFollowupPage', page);
  var size;
  size = 10;
  var pageData = page - 1;
  console.log('  pagination   ',
    $('#pagination').twbsPagination($('#txtPage').val())
  );

  $('#pagination').twbsPagination({
    totalPages: 10
  });

  var pageData = parseInt(localStorage.getItem('newFollowupPage')) - 1;

  var followUpStartDate = $('#sDate').datepicker('getDate');
  var followUpEndDate = $('#eDate').datepicker('getDate');
  if (isNaN(followUpStartDate)) {
    var startDate = followUpStartDate.getTime();
  }
  else {
    var startDate = 0;
  }
  if (isNaN(followUpEndDate)) {
    var endDate = followUpEndDate.getTime();
  }
  else {
    var endDate = 0;

  }

  if (startDate <= endDate) {
    var searchFollowup = $('#searchFollowup').val();
    var searchName = $('#searchFollowup').val();
console.log('entered string',searchFollowup);
    var startDate = Date.parse($('#sDate').datepicker('getDate'));
    var endDate = Date.parse($('#eDate').datepicker('getDate'));
    if (isNaN(startDate) || isNaN(endDate)) {
      var startDate = new Date();
      startDate.setHours(0, 0, 0, 0);
      var startDate  = Date.parse(startDate);
      var endDate = startDate + 86399000;
      var getAllFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/followUpSearch/?page=' + pageData + '&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyString=' + searchFollowup;
    }
    else {
      endDate += 86399000;
      var getAllFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/followUpSearch/?page=' + pageData + '&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyString=' + searchFollowup;
    }
    if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
        try {
          var xhr = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (exception) {
          var xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
      } else {
        var xhr = new XMLHttpRequest();
      }
    } else {
      alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('GET', getAllFollowUpURL, true);
    xhr.send();

    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          $('#enqContact').html('');
          $('#enquiry').html('');
          $('#btnFollow').css('display', 'none');
          $('#btnLebel').css('display', 'block');
          $('#btnCrateNewFollow').css('display', 'none');
          $('#dateContainer').css('display', 'block');
          $('#PageView').css('display', 'block');

          NProgress.done();
          allEnquiry = JSON.parse(xhr.responseText);
          var listOfPage = allEnquiry.content
          var count = 0;
          var no = 1;

          var FollowUp = allEnquiry.content;

          var count = 0;
          var no = 1;
          var enqTable =
            '<div class=\'row tblHeading\' >' +
            '<div class=\'col-lg-1 \'> Next FollowUp Date </div>' +
            '<div class=\'col-lg-2 \'> Name </div>' +
            '<div class=\'col-lg-3 \'> Courses </div>' +
            '<div class=\'col-lg-1 \'> Mobile No. </div>' +
            '<div class=\'col-lg-3 \'> Email Id </div>' +
            '<div class=\'col-lg-1 \'> Demo Lecture status </div>' +
            '<div class=\'col-lg-1 \'> Location </div>' +
            '</div>' +
            '<div class=\'col-lg-12  \' id=\'tbldataFollow\' style=\'height: 360px;overflow-y: auto;padding:0px; cursor: pointer; overflow-style: marquee,panner; \'>';
          $('#followUp').append(enqTable);
          if(jQuery.isEmptyObject(FollowUp))
          {
            var markup =
            '<div class=\'row  \' style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\'>' +
            '<div class=\'col-lg-12 \' style=\'text-align:center;\'><strong>No data found</strong></div>' +
            '</div> ';
            $('#tbldataFollow').append(markup);
          } 
         
       $.each(FollowUp, function (index, FollowUp) {
            var followUpDate = new Date(FollowUp.followUpDate);
            var nextDateFollowUp = new Date(FollowUp.nextFollowUpDate);
            var todayTime = new Date(followUpDate.toString());
            var month = todayTime.getMonth() + 1;
            var day = todayTime.getDate();
            var year = todayTime.getFullYear();
            var todayTimeE = new Date(nextDateFollowUp.toString());
            var monthE = todayTimeE.getMonth() + 1;
            var dayE = todayTimeE.getDate();
            var yearE = todayTimeE.getFullYear();
            var fDate = day + '/' + month + '/' + year;
            var nDate = dayE + '/' + monthE + '/' + yearE;

            var id = FollowUp.id;
            var applicantName = FollowUp.enquiries.applicantName;
            var mobileNo = FollowUp.mobileNo;
            var eMail = FollowUp.eMail;
            var demoLectureStatusUpdate = 'Not Attend';

            if (FollowUp.demoLectureStatus) {
              demoLectureStatusUpdate = 'Attended';
            } else {
              demoLectureStatusUpdate = 'Not Attend';
            }
            var markup =
              '<div class=\'row  \' id=\'rowId' + index + '\'  style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\' onClick=\'viewFolloupModel(' + index + ');\'>' +
              '<div class=\'col-lg-1 \' id=\'colId' + index +'\' >  ' + nDate + ' </div>' +
              '<div class=\'col-lg-2 \'>  ' + applicantName + '   </div>' +
              '<div class=\'col-lg-3 \'>  ' + FollowUp.addCourses + '   </div>' +
              '<div class=\'col-lg-1 \'>  ' + mobileNo + '   </div>' +
              '<div class=\'col-lg-3 \'>  ' + eMail + '   </div>' +
              '<div class=\'col-lg-1 \'>  ' + demoLectureStatusUpdate + '   </div>' +
              '<div class=\'col-lg-1 \'>  ' + FollowUp.locality + '  </div>' +
              '</div>';

            $('#tbldataFollow').append(markup);
            
            if (FollowUp.followUpStatus == 'Enrolled') {
              $('#colId' + index).css('background-color', '#a5d6a7');
            } else if (FollowUp.followUpStatus == 'Fake') {
              $('#colId' + index).css('background-color', '#ffab91');
            } else if (FollowUp.followUpStatus == 'NotInterested') {
              $('#colId' + index).css('background-color', '#ffcc80');
            }
            else if (FollowUp.followUpStatus == 'pending') {
              $('#colId' + index).css('background-color', '#b2ebf2');
            }
            else if (FollowUp.followUpStatus == 'Duplicate') {
              $('#colId' + index).css('background-color', '#bdbdbd');
            }
            else {
              $('#colId' + tempRowID).css('background-color', 'white');
            }
          });
        }
      }
    };
  } else {
    alert('Start Date is bigger than End date');
  }

  $('#btnLebel').css('display', 'block');
  $('#followUp').html('');
  $('#enquiry').html('');
  $('#btnFollow').addClass('disabled');
  $('#btnFollow').css('display', 'block');
  $('#dateContainer').css('display', 'block');
  $('#btnCrateNewFollow').css('display', 'none');

}

function paginationInquiry() {
  NProgress.start();
  displayEnquiryCount();
  if($('#pagination').data("twbs-pagination")){
    $('#pagination').twbsPagination('destroy');
}
  var $pagination = $('#pagination');
  var pageData = 0;
  var defaultOpts = {
    totalPages: 20
  };


  var obj = $('#pagination').twbsPagination({

    onPageClick: function (event, page) {
      var size;
      size = 10;
      var pageData = parseInt(localStorage.getItem('newEnquiryPage')) - 1;

      var followUpStartDate = $('#sDate').datepicker('getDate');
      var followUpEndDate = $('#eDate').datepicker('getDate');
      if (isNaN(followUpStartDate)) {
        var startDate = followUpStartDate.getTime();
      }
      else {
        var startDate = 0;
      }
      if (isNaN(followUpEndDate)) {
        var endDate = followUpEndDate.getTime();
      }
      else {
        var endDate = 0;

      }
      if (startDate <= endDate) {
        var searchName = $('#searchFollowup').val();
        var searchFollowup = $('#searchFollowup').val();
        var startDate = Date.parse($('#sDate').datepicker('getDate'));
        var endDate = Date.parse($('#eDate').datepicker('getDate'));
        if (isNaN(startDate) || isNaN(endDate)) {
          var startDate = new Date();
          startDate.setHours(0, 0, 0, 0);
          var startDate  = Date.parse(startDate);
          // startDate += 19800000;
          var endDate = startDate + 86399000;

          var getAllEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/enquirySearch/?page=0&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyChar=' + searchFollowup;
        }
        else {
          endDate += 86399000;
         var getAllEnquiryURL = protocol + server + ':' + port + appName + '/inquiry/enquirySearch/?page=0&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyChar=' + searchFollowup;
        }
        if (window.XMLHttpRequest || window.ActiveXObject) {
          if (window.ActiveXObject) {
            try {
              var xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (exception) {
              var xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }
          } else {
            var xhr = new XMLHttpRequest();
          }
        } else {
          alert('Your browser does not support XMLHTTP Request...!');
        }
        xhr.open('GET', getAllEnquiryURL, true);
        xhr.send();
        xhr.onreadystatechange = function () {
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              $('#btnLebel').css('display', 'block');
              $('#followUp').html('');
              $('#enqContact').html('');
              $('#btnFollow').addClass('disabled');
              $('#btnFollow').css('display', 'block');
              $('#dateContainer').css('display', 'block');
              $('#btnCrateNewFollow').css('display', 'block');
              NProgress.done();
              allEnquiry = JSON.parse(xhr.responseText);
console.log('allEnquiry',allEnquiry);
              if(allEnquiry.totalPages == 0)
              {
                totalPages = 1;
                currentPage = 1;
              }
              else{

              totalPages = allEnquiry.totalPages;
              
              var newVarPage = localStorage.getItem('newEnquiryPage');
              var currentPage = $pagination.twbsPagination('getCurrentPage');
              }
              listOfPage = allEnquiry.content;
              $pagination.twbsPagination('destroy');

              $pagination.twbsPagination($.extend({}, defaultOpts, {
                totalPages: totalPages,
                startPage: currentPage,
                initiateStartPageClick: false,
                onPageClick: paginationFunction
              }));
              var count = 0;
              var no = 1;
              var enqTable =
                '<div class=\'row tblHeading\' >' +
                '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Date </div>' +
                '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Name </div>' +
                '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Courses </div>' +
                '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> Mobile No. </div>' +
                '<div class=\'col-lg-2 \' style="word-wrap: break-word;     padding: 0px;"> Email Id </div>' +
                '<div class=\'col-lg-3 \'>' +
                ' <div class=\'row \'> ' +
                '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> From Page </div> ' +
                '<div class=\'col-lg-6 \' style="word-wrap: break-word;     padding: 0px;"> Enquiry Type </div> ' +
                '</div> ' +
                '</div>' +
                '<div class=\'col-lg-1 \'> </div>' +
                '</div>' +
                '<div class=\'col-lg-12  \' id=\'tbldata1\'   style="height: 360px;overflow-y: auto;padding:0px;  overflow-style: marquee,panner;" >';
              $('#enquiry').append(enqTable);
              if(jQuery.isEmptyObject(listOfPage))
              {
                var markup =
                '<div class=\'row  \'  \'style=" width: 100%;  margin: 0 auto;  border-bottom: 1px solid rgba(170, 170, 170, 0.53);" >' +
                '<div class=\'col-lg-12 \' style="word-wrap: break-word;  text-align:center; vertical-align: top; padding: 0px;"><strong>No data found</strong></div></div>';
                $('#tbldata1').append(markup);
              } 
              
              $.each(listOfPage, function (index, enquiry) {
                var string = enquiry.fromPage;
                var FromPageTech = string;

                count++;
                $('#numbers').html('<li>' + count + '</li>');
                var enqToday = new Date(enquiry.inquiryTimestamp);

                var dd = enqToday.getDate();
                var mm = enqToday.getMonth() + 1; //January is 0!
                var yyyy = enqToday.getFullYear();
                if (dd < 10) {
                  dd = '0' + dd;
                }
                if (mm < 10) {
                  mm = '0' + mm;
                }
                enqToday = dd + '-' + mm + '-' + yyyy;
                var markup =
                 '<div class=\'row  \' id=\'rowId' + index +
                  '\'style=" width: 100%;  margin: 0 auto;  border-bottom: 1px solid rgba(170, 170, 170, 0.53); padding: 7px 0px;" >' +
                  '<div class=\'col-lg-1 \' style="word-wrap: break-word;     padding: 0px;"> ' + enqToday +
                  '</div>' +
                  '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.applicantName +
                  '</div>' +
                  '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.courses +
                  '</div>' +
                  '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.mobileNo +
                  '</div>' +
                  '<div class=\'col-lg-2 \' style="word-wrap: break-word;  padding: 0px;"> ' + enquiry.eMail +
                  '</div>' +
                  '<div class=\'col-lg-3 \' style="word-wrap: break-word;">' +
                  '<div class=\'row\' >' +
                  '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + FromPageTech + '</div>' +
                  '<div class=\'col-lg-6 \' style="word-wrap: break-word; padding: 0px;">' + enquiry.inquiryType + '</div>' +
                  '</div> ' +
                  '</div>' +
                  '<div class=\'col-lg-1 \' style="word-wrap: break-word;  padding: 0px;text-align: left;">' +
                  '<a onClick=\'clickAddFolloupModel("' + enquiry.id + '","' + index + '");\' style=" color:#07639d;    cursor: pointer;"> <u> Add Follow Up </u></a> ' +
                  '</div>' +
                  '</div> ';
                $('#tbldata1').append(markup);

              });
            }
          }
        };

      } else {
        alert('Start Date is bigger than End date');
      }
      $('#btnLebel').css('display', 'block');
      $('#followUp').html('');
      $('#enquiry').html('');
      $('#btnFollow').addClass('disabled');
      $('#btnFollow').css('display', 'block');
      $('#dateContainer').css('display', 'block');
      $('#btnCrateNewFollow').css('display', 'block');
    },
    totalPages: totalPages,
    visiblePages: 5,
  });

}

var selectedEnquiry;
var tempRowID;
var selectedId;
var viewSeletedRowData
function clickAddFolloupModel(id, index) {
console.log('id',id);
  selectedEnquiry = index;
  selectedId = id;
  var localView = allEnquiry;
  viewSeletedRowData = localView.content[index];
  viewSeletedRowData = localView.content[index];
  console.log('viewSeletedRowData',viewSeletedRowData);

  $('#rowId' + tempRowID).css('background-color', 'white');
  $('#rowId' + index).css('background-color', 'rgba(170, 170, 170, 0.24)');
  tempRowID = index;
  $('#modal-1').modal('show');


  $('#closeEnrollForm')[0].reset();
  $('#addFollowUpForm')[0].reset();
  $('.courseListAdd').css('border', 'none');
  $('.multiselect-selected-text').text('Please select ');
  $('.multiselect').attr('title', 'Please select ');
  $('form#addFollowUpForm :input').each(function () {
    var input = $(this);
    $(this).css('border', '1px solid #aaa');
  });
  $('form#closeEnrollForm :input').each(function () {
    var input = $(this);
    $(this).css('border', '1px solid #aaa');
  });


  $(document).keyup(function (e) {
    if (e.keyCode == 27) { // esc keycode
      $('#modal-1').modal('hide');
    }
  });

  $('#mobileAdd').val(viewSeletedRowData.mobileNo);
  $('#emailAdd').val(viewSeletedRowData.eMail);
  $('#locationAdd').val(viewSeletedRowData.locality);
  $('#uname').val(viewSeletedRowData.applicantName);
  
  
    var optionValueBatch = viewSeletedRowData.batchType;
    var optionValueFollowup = viewSeletedRowData.followUpType;
  if(optionValueFollowup != null)
  {
    $('#followUpTypeAdd').val(optionValueFollowup)
    .find('option[value=' + optionValueFollowup + ']').attr('selected', true);
  }
    if(optionValueBatch != null){
      $('#batchTypeAdd').val(optionValueBatch)
      .find('option[value=' + optionValueBatch + ']').attr('selected', true);
    }
  var htm;
  var options = $('#chCourseList option');
  //console.log("All selected courses : ", viewSeletedRowData.courses);
  $.each(options, function (index, value) {
    if (viewSeletedRowData.courses.indexOf(value.text) !== -1) {
      htm += '<option value ="' + value.text + '" selected>' + value.text + '</option>';
    } else {
      htm += '<option value ="' + value.text + '" >' + value.text + '</option>';
    }
  });
  //console.log("All courses : ", htm);
  $('#chCourseList').html('');
  $('#chCourseList').append(htm);
  $('#chCourseList').multiselect('rebuild');

}

$('#btnFollow').click(function () {
  $('#modal-1').modal('show');
  var selectedData = allInquiry[selectedEnquiry]
  var htm;
  var options = $('#chCourseList option');
  $.each(options, function (index, value) {
    if (selectedData.courses.indexOf(value.text) !== -1) {
      htm += '<option value ="' + value.text + '" selected>' + value.text + '</option>';
    } else {
      htm += '<option value ="' + value.text + '" >' + value.text + '</option>';
    }
  });
  $('#chCourseList').html('');
  $('#chCourseList').append(htm);
  $('#chCourseList').multiselect('rebuild');
});

// ------------------------------------------- enroll or not interested FollowUp  -----------------------------------------------
$('#createEnqFollowUp2').click(function () {
  var enroll = $('#enrollMode').val();
  if (enroll === null) {
    $('#enrollMode').css('border', '1px solid red');
  } else {
    $('#enrollMode').css('border', '1px solid green');
    var selectedData = viewSeletedRowData;
    console.log('selectedData',selectedData);
var date = new Date();
var dateNxtFollowUp = date.getTime();
// var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/updateFollowUp?id=' + selectedId + '&status=' + enroll + '&nextFollowUpDate=' + dateNxtFollowUp;

    var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/addFollowUp';
    // var followUpSend = {
    //   followUpType: selectedData.followUpType,
    //   followUpDate: dateNxtFollowUp,
    //   nextFollowUpDate: dateNxtFollowUp,
    //   comment: '',
    //   addCourses: selectedData.courses,
    //   followUpStatus: enroll,    
    //   enteredBy: '',
    //   batchType: '',
    //   eMail: selectedData.eMail,
    //   mobileNo: selectedData.mobileNo,
    //   enquiries: {
    //     id: selectedData.id
    //   }
    // };
      var followUpSend = {
      eMail:selectedData.eMail,
      addCourses:selectedData.courses,
      mobileNo:selectedData.mobileNo,
      nextFollowUpDate: dateNxtFollowUp,
      enteredBy: localStorage.userName,
      inquiryType : "course",
      fromPage : "Admin Panel",
      tenant : "codekul",
      followUpStatus:enroll,
      enquiries: {
        id: selectedData.id
      }
     };

    console.log('followUpSend',followUpSend);
    if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
        try {
          var xhr = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (exception) {
          var xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
      } else {
        var xhr = new XMLHttpRequest();
      }
    } else {
      alert('Your browser does not support XMLHTTP Request...!');
    }
    xhr.open('POST', addFollowUpURL, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
     xhr.send(JSON.stringify(followUpSend));
     //xhr.send();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          console.log('got success');
        //  alert('Status updated');
          $('#alertText').html('Status updated...!!!');
          $('#myModal').modal('show');
          $('#closeEnrollForm')[0].reset();
          $('.multiselect-selected-text').text('Please select  ');
          $('.multiselect').attr('title', 'Please select  ');
          $('#modal-1').modal('hide');
          paginationFunction();
          $('form#closeEnrollForm :input').each(function () {
            var input = $(this);
            $(this).css('border', '1px solid #aaa');
          });
        } else {
          console.log('xhr.statusText', xhr.statusText);
        }
      }
    };
  }
});
// ------------------------------------------- Close enroll or not interested FollowUp  -----------------------------------------------

// -------------------------------------------  Close Get Enquiry Page    -----------------------------------------------

// -------------------------------------------- Follow Up date ---------------------------------------------------------------------------
function sDate() {
  var followUpStartDate = $('#sDate').val();
  if (followUpStartDate === '') {
    $('#sDate').css('border-bottom', '1px solid green');
    return false
  } else {
    $('#sDate').css('border-bottom', '1px solid green');
    return true
  }
}
function eDate() {
  var followUpEndDate = $('#eDate').val();
  if (followUpEndDate === '') {
    $('#eDate').css('border-bottom', '1px solid green');
    return false
  } else {
    $('#eDate').css('border-bottom', '1px solid green');
    return true
  }

}
$('#FollowUpdates').click(function () {
  var ref_this = $('ul.mynavlist li a.active');
  var selectedTab = ref_this.data('id');
  var followUpStartDate = $('#sDate').val();
  var followUpEndDate = $('#eDate').val();
  if (isNaN(followUpStartDate) == isNaN(followUpEndDate)) {
    if (selectedTab == 1) {
       var clickEve = 'OK';
      getAllEnquiry(clickEve);
    }
    else {
      var clickEve = 'OK';
      getAllFollowUp(clickEve);
    }
  }
  else {
    console.log('Enter all values');
    if (followUpStartDate === '') {
      $('#sDate').css('border-bottom', '1px solid red');

    } else {
      $('#sDate').css('border-bottom', '1px solid green');
    }

    if (followUpEndDate === '') {
      $('#eDate').css('border-bottom', '1px solid red');
    } else {
      $('#eDate').css('border-bottom', '1px solid green');
    }

  }
});

$('#searchFollowup').keypress(function (e) {
  var key = e.which;
  if(key == 13)  // the enter key code
   {
    var ref_this = $('ul.mynavlist li a.active');
    var selectedTab = ref_this.data('id');
    var followUpStartDate = $('#sDate').val();
    var followUpEndDate = $('#eDate').val();
    if (isNaN(followUpStartDate) == isNaN(followUpEndDate)) {
      if (selectedTab == 1) {
         var clickEve = 'OK';
        getAllEnquiry(clickEve);
      }
      else {
        var clickEve = 'OK';
        getAllFollowUp(clickEve);
      }
    }
    else {
      console.log('Enter all values');
      if (followUpStartDate === '') {
        $('#sDate').css('border-bottom', '1px solid red');
  
      } else {
        $('#sDate').css('border-bottom', '1px solid green');
      }
  
      if (followUpEndDate === '') {
        $('#eDate').css('border-bottom', '1px solid red');
      } else {
        $('#eDate').css('border-bottom', '1px solid green');
      }
  
    }
   }
 });
// ************************************************ in FollowUp Tab  ***********************************************************

// -----------------------------------------------    getAllFollowUp --------------------------------------
var AllEnquiry;
var allEnquiry;
var listOfPage;
var allInquiry;
var totalPages = 35;
function getAllFollowUp(clickEve) {
 
  if(clickEve != 'OK'){
    setDefaultDate();
    $('#searchFollowup').val('');
  }
  $('#PageView').css('display', 'block');
  $('#pagination').twbsPagination('destroy');
  paginationFollowup();
};
function paginationFollowup() {
  NProgress.start();
  displayFollowupCount();
  
  var $pagination = $('#pagination');
  var pageData = 0;
  var defaultOpts = {
    totalPages: 20
  };

  var obj = $('#pagination').twbsPagination({
    onPageClick: function (event, page) {
      var size;
      size = 10;
      // var pageData = parseInt(localStorage.getItem('newFollowupPage')) - 1;
      var followUpStartDate = $('#sDate').datepicker('getDate');
      var followUpEndDate = $('#eDate').datepicker('getDate');
      if (isNaN(followUpStartDate)) {
        var startDate = followUpStartDate.getTime();
      }
      else {
        var startDate = 0;
      }
      if (isNaN(followUpEndDate)) {
        var endDate = followUpEndDate.getTime();
      }
      else {
        var endDate = 0;

      }
      if (startDate <= endDate) {
        var searchName = $('#searchFollowup').val();
        var searchFollowup = $('#searchFollowup').val();
        var startDate = Date.parse($('#sDate').datepicker('getDate'));
        var endDate = Date.parse($('#eDate').datepicker('getDate'));
        if (isNaN(startDate) || isNaN(endDate)) {
          var startDate = new Date();
          startDate.setHours(0, 0, 0, 0);
          var startDate  = Date.parse(startDate);
          // startDate += 19800000;
          var endDate = startDate + 86399000;
          var getAllFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/followUpSearch/?page=' + pageData + '&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyString=' + searchFollowup;
        }
        else {
          // startDate += 19800000;
          endDate += 86399000;
          // endDate += 19800000;
          var getAllFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/followUpSearch/?page=' + pageData + '&size=' + size + '&startDate=' + startDate + '&endDate=' + endDate + '&anyString=' + searchFollowup;
        }
        if (window.XMLHttpRequest || window.ActiveXObject) {
          if (window.ActiveXObject) {
            try {
              var xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (exception) {
              var xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }
          } else {
            var xhr = new XMLHttpRequest();
          }
        } else {
          alert('Your browser does not support XMLHTTP Request...!');
        }
        xhr.open('GET', getAllFollowUpURL, true);
        xhr.send();

        xhr.onreadystatechange = function () {
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              $('#enqContact').html('');
              $('#enquiry').html('');
              $('#btnFollow').css('display', 'none');
              $('#btnLebel').css('display', 'block');
              $('#btnCrateNewFollow').css('display', 'none');
              $('#dateContainer').css('display', 'block');
              $('#PageView').css('display', 'block');
              // NProgress.done();
              allEnquiry = JSON.parse(xhr.responseText);
              console.log('allEnquiry',allEnquiry);
              if(allEnquiry.totalPages == 0)
              {
                totalPages = 1;
                currentPage = 1;
              }
              else{
                totalPages = allEnquiry.totalPages;
                
                var newVarPage = localStorage.getItem('newFollowupPage');
                var currentPage = $pagination.twbsPagination('getCurrentPage');
              }
              listOfPage = allEnquiry.content
              $pagination.twbsPagination('destroy');
              $pagination.twbsPagination($.extend({}, defaultOpts, {
                totalPages: totalPages,
                startPage: currentPage,
                initiateStartPageClick: false,
                onPageClick: paginationFunctionFollowup

              }));
              NProgress.done();
              allEnquiry = JSON.parse(xhr.responseText);
              var FollowUp = allEnquiry.content;
              
              var count = 1;
              var no = 1;
              var enqTable =
                '<div class=\'row tblHeading\' >' +
                '<div class=\'col-lg-1 \'> Next FollowUp Date </div>' +
                '<div class=\'col-lg-2 \'> Name </div>' +
                '<div class=\'col-lg-3 \'> Courses </div>' +
                '<div class=\'col-lg-1 \'> Mobile No. </div>' +
                '<div class=\'col-lg-3 \'> Email Id </div>' +
                '<div class=\'col-lg-1 \'> Demo Lecture status </div>' +
                '<div class=\'col-lg-1 \'> Location </div>' +
                '</div>' +
                '<div class=\'col-lg-12  \' id=\'tbldataFollow\' style=\'height: 360px;overflow-y: auto;padding:0px; cursor: pointer; overflow-style: marquee,panner; \'>';
              $('#followUp').append(enqTable);

              if(jQuery.isEmptyObject(FollowUp))
              {
                var markup =
                '<div class=\'row  \' style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\'>' +
                '<div class=\'col-lg-12 \' style=\'text-align:center;\'><strong>No data found</strong></div>' +
                '</div> ';
                $('#tbldataFollow').append(markup);
              } 
              console.log('FollowUp',FollowUp);

              $.each(FollowUp, function (index, FollowUp) {
                var followUpDate = new Date(FollowUp.followUpDate);
                var nextDateFollowUp = new Date(FollowUp.nextFollowUpDate);
               
                var todayTime = new Date(followUpDate.toString());
                var month = todayTime.getMonth() + 1;
                var day = todayTime.getDate();
                var year = todayTime.getFullYear();
                var todayTimeE = new Date(nextDateFollowUp.toString());
                console.log('nextDateFollowUp',nextDateFollowUp);
                var monthE = todayTimeE.getMonth() + 1;
                var dayE = todayTimeE.getDate();
                var yearE = todayTimeE.getFullYear();
                var fDate = day + '/' + month + '/' + year;
                if (dayE < 10) {
                  dayE = '0' + dayE;
                }
                if (monthE < 10) {
                  monthE = '0' + monthE;
                }
                var nDate = dayE + '/' + monthE + '/' + yearE;

                var id = FollowUp.id;
                var applicantName = FollowUp.enquiries.applicantName;
                var mobileNo = FollowUp.mobileNo;
                var eMail = FollowUp.eMail;
                var demoLectureStatusUpdate = 'Not Attend';

                if (FollowUp.demoLectureStatus) {
                  demoLectureStatusUpdate = 'Attended';
                } else {
                  demoLectureStatusUpdate = 'Not Attend';
                }

                var markup =
                  '<div class=\'row  \' id=\'rowId' + index + '\'  style=\'width:100%;    margin: 0 auto; border-bottom:1px solid #aaa ;padding: 5px 0px;\' onClick=\'viewFolloupModel(' + index + ');\'>' +
                  '<div class=\'col-lg-1 \' id=\'colId' + index +'\' >  ' + nDate + ' </div>' +
                  '<div class=\'col-lg-2 \'>  ' + applicantName + '   </div>' +
                  '<div class=\'col-lg-3 \'>  ' + FollowUp.addCourses + '   </div>' +
                  '<div class=\'col-lg-1 \'>  ' + mobileNo + '   </div>' +
                  '<div class=\'col-lg-3 \'>  ' + eMail + '   </div>' +
                  '<div class=\'col-lg-1 \'>  ' + demoLectureStatusUpdate + '   </div>' +
                  '<div class=\'col-lg-1 \'>  ' + FollowUp.locality + '  </div>' +
                  '</div>';
              
                $('#tbldataFollow').append(markup);
                
                if (FollowUp.followUpStatus == 'Enrolled') {
                  $('#colId' + index).css('background-color', '#a5d6a7');
                } else if (FollowUp.followUpStatus == 'Fake') {
                  $('#colId' + index).css('background-color', '#ffab91');
                } else if (FollowUp.followUpStatus == 'NotInterested') {
                  $('#colId' + index).css('background-color', '#ffcc80');
                }
                else if (FollowUp.followUpStatus == 'pending') {
                  $('#colId' + index).css('background-color', '#b2ebf2');
                }
                else if (FollowUp.followUpStatus == 'Duplicate') {
                  $('#colId' + index).css('background-color', '#bdbdbd');
                }
                else {
                  $('#colId' + tempRowID).css('background-color', 'white');
                }
              });

            } else {
              document.getElementById('req_text').innerHTML = 'Error Code: ' + xhr.status + 'Error Message: ' + xhr.statusText;
              alert('Not');
            }
          }
        };

      } else {
        alert('Start Date is bigger than End date');
      }
      $('#btnLebel').css('display', 'block');
      $('#followUp').html('');
      $('#enquiry').html('');
      $('#btnFollow').addClass('disabled');
      $('#btnFollow').css('display', 'block');
      $('#dateContainer').css('display', 'block');
      $('#btnCrateNewFollow').css('display', 'none');
    },
    totalPages: totalPages,
    visiblePages: 5,
  });

}

var enquiriesList
// -----------------------------------------------    View Form  Validations--------------------------------------
function validateLocationsView(id) {
  var placeView = $('#' + id).val();
  var validatePlaceView = /^[0-9]+$/;
  if (placeView === '') {
    $('#errorFName').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFName').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    if (placeView.match(validatePlaceView)) {
      $('#errorFName').addClass('showItem');
      $('.errorMessage').css('color', 'red');
      $('#errorFName').html('Please enter only alphabets');
      $('#' + id).css('border', '1px solid red');
      $(id).css('display', 'none');
      return false;
    } else {
      $('#errorFName').removeClass('showItem');
      $('#errorFName').addClass('showItem');
      $('#errorFName').html(' ');
      $('#' + id).css('border', '1px solid green');
      $(id).css('display', 'none');
      return true;
    }
  }
}
function validateFollowUpTypeView(id) {
  var followUpType = $('#' + id).val();

  if (followUpType === '') {
    $('#errorFollowUpType').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorFollowUpType').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorFollowUpType').removeClass('showItem');
    $('#errorFollowUpType').addClass('showItem');
    $('#errorFollowUpType').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateBatchTypeView(id) {
  var BatchType = $('#' + id).val();

  if (BatchType === '') {
    $('#errorBatchTypeAdd').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorBatchTypeAdd').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorBatchTypeAdd').removeClass('showItem');
    $('#errorBatchTypeAdd').addClass('showItem');
    $('#errorBatchTypeAdd').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateCoursesView() {
  var technology = [];
  $.each($('#chCourseList2 option:selected'), function () {
    technology.push($(this).html());
  });
  if (technology.length === 0) {
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html('Please select technology');
    $('.courseListView').css('border', '1px solid red');
    return false;
  } else {
    $('#errorTechnology').removeClass('showItem');
    $('#errorTechnology').addClass('showItem');
    $('#errorTechnology').html(' ');
    $('.courseListView').css('border', '1px solid green');
    return true;
  }

}
function validateCommentMsgView(id) {
  var CommentMsgAdd = $('#' + id).val();
  var trimMsg1 = $.trim(CommentMsgAdd);
  if (trimMsg1 >= 0) {
    $('#errorCommentMsgAdd').addClass('showItem');
    $('.errorCommentMsg').css('color', 'red');
    $('#errorCommentMsgAdd').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorCommentMsgAdd').removeClass('showItem');
    $('#errorCommentMsgAdd').addClass('showItem');
    $('#errorCommentMsgAdd').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function validateDatePickerView(id) {
  var DatePicker = $('#' + id).val();

  if (DatePicker === '') {
    $('#errorDatePickerAdd').addClass('showItem');
    $('.errorMessage').css('color', 'red');
    $('#errorDatePickerAdd').html('Please enter name');
    $('#' + id).css('border', '1px solid red');
    $(id).css('display', 'none');
    return false;
  } else {
    $('#errorDatePickerAdd').removeClass('showItem');
    $('#errorDatePickerAdd').addClass('showItem');
    $('#errorDatePickerAdd').html(' ');
    $('#' + id).css('border', '1px solid green');
    $(id).css('display', 'none');
    return true;
  }
}
function viewFolloupModel(index) {
  $('#modal-2').modal('show');

  $('#viewFollowUpForm')[0].reset();
  $('.courseListView').css('border', 'none');
  $('.multiselect-selected-text').text('Please select ');
  $('.multiselect').attr('title', 'Please select ');
  $('form#viewFollowUpForm :input').each(function () {
    var input = $(this);
    $(this).css('border', '1px solid #aaa');
  });

  $(document).keyup(function (e) {
    if (e.keyCode == 27) { // esc keycode
      $('#modal-2').modal('hide');
    }
  });

  var selectedFollowUp = allEnquiry.content[index];
  console.log('selectedFollowUp',selectedFollowUp);
  enquiriesList = selectedFollowUp.enquiries;
  console.log('enquiriesList',enquiriesList);
  var followUpDate = new Date(selectedFollowUp.followUpDate);
  var nextDateFollowUp = new Date(selectedFollowUp.nextFollowUpDate);
  var todayTime = new Date(followUpDate.toString());
  var month = todayTime.getMonth() + 1;
  var day = todayTime.getDate();
  var year = todayTime.getFullYear();
  var todayTimeE = new Date(nextDateFollowUp.toString());
  var monthE = todayTimeE.getMonth() + 1;
  var dayE = todayTimeE.getDate();
  var yearE = todayTimeE.getFullYear();
  var fDate = day + '/' + month + '/' + year;
  var nDate = dayE + '/' + monthE + '/' + yearE;
console.log(enquiriesList);
  $('#name').text(enquiriesList.applicantName);
  $('#mobNo').text(enquiriesList.mobileNo);
  $('#emailId').text(enquiriesList.eMail);
  $('#locality').text(selectedFollowUp.locality);
  $('#courses').text(selectedFollowUp.addCourses);
  $('#batch').text(selectedFollowUp.batchType);
  $('#lastDate').text(fDate);
  $('#nextDate').text(nDate);
  $('#followupGiven').text(selectedFollowUp.enteredBy);



  var getAllCommentsURL = protocol + server + ':' + port + appName + '/inquiry/getAllFollowUpByEnquiriesId/' + enquiriesList.id;

  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('GET', getAllCommentsURL, true);
  xhr.send();
  var resultcomments;
  var htmlComments;
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        resultcomments = JSON.parse(xhr.responseText);
        var commentData = resultcomments.comment;
        var htmlComments = '';
        $('#msg').html('');
        $.each(commentData, function (index, value) {
          var comment = value.comment;
          var date = new Date(value.followUpDate);
          var dd = date.getDate();
          var mm = date.getMonth() + 1; //January is 0!
          var yyyy = date.getFullYear();
          if (dd < 10) {
            dd = '0' + dd
          }
          if (mm < 10) {
            mm = '0' + mm
          }
          var date = dd + '/' + mm + '/' + yyyy;
          
          if (comment != null) {
            htmlComments = "" + date + ': ' + comment + '<br />';

            $('#msg').append(htmlComments);
          }
        });
      } else {
        console.log('start web services', xhr.statusText)
      }
    }
  };


  if (selectedFollowUp.demoLectureStatus) {
    $('#viewDemoStatus').prop('checked', true);
    $('#viewDemoStatus').attr('disabled', true);
  } else {
    $('#viewDemoStatus').prop('checked', false);
    $('#viewDemoStatus').attr('disabled', false);
  }

  $('#exCourse2').val(selectedFollowUp.addCourses);
  $('#exCourse2').attr('readonly', true);

  $('#id').val(selectedFollowUp.id);
  var dummy = $('#id').val();
  console.log('id',dummy);
  $('#mobileView').val(selectedFollowUp.mobileNo);
  $('#emailView').val(selectedFollowUp.eMail);
  $('#locationView').val(selectedFollowUp.locality);


  var optionValueBatch = selectedFollowUp.batchType;
  var optionValueFollowup = selectedFollowUp.followUpType;

  $('#followUpTypeView').val(optionValueFollowup)
    .find('option[value=' + optionValueFollowup + ']').attr('selected', true);

  $('#batchTypeView').val(optionValueBatch)
    .find('option[value=' + optionValueBatch + ']').attr('selected', true);

  var htm;
  var options = $('#chCourseList2 option');
console.log('options',options);
  $.each(options, function (index, value) {
    if (selectedFollowUp.addCourses.indexOf(value.text) !== -1) {
      htm += '<option value ="' + value.text + '" selected>' + value.text + '</option>';
    } else {
      htm += '<option value ="' + value.text + '" >' + value.text + '</option>';
    }
  });
  $('#chCourseList2').html('');
  $('#chCourseList2').append(htm);
  $('#chCourseList2').multiselect('rebuild');
console.log('Selected followup:',selectedFollowUp.id);
  $('#idbox').val(selectedFollowUp.id);
}
$('#btnCloseFollowUp').click(function () {
  var id = $('#idbox').val();
  console.log('id',id);
  var enrollClose = $('#enrollStatus').val();
  if (enrollClose === null) {
    $('#followUpType2').css('border', '1px solid red');
  } else {
    $('#followUpType2').css('border', '1px solid green');
    var date = new Date();
    var dateNxtFollowUp = date.getTime();
    
    var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/updateFollowUp?id=' + id + '&status=' + enrollClose + '&nextFollowUpDate=' + dateNxtFollowUp;

    if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
        try {
          var xhr = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (exception) {
          var xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
      } else {
        var xhr = new XMLHttpRequest();
      }
    } else {
      alert('Your browser does not support XMLHTTP Request...!');
    }

    xhr.open('POST', addFollowUpURL, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();

    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
         // alert('Status updated...!!!');
          $('#alertText').html('Status updated...!!!');
          $('#myModal').modal('show');
          $('#closeFollowUpForm')[0].reset();
          $('#modal-2').modal('hide');
          var clickEve = 'followup';
//          getAllFollowUp(clickEve);
paginationFunctionFollowup();
          $('.multiselect-selected-text').text('Please select  ');
          $('.multiselect').attr('title', 'Please select  ');
        } else {
          console.log('xhr.statusText', xhr.statusText);
        }
      }
    };
  }
});
// -----------------------------------------------    View Form  call --------------------------------------
$('#createFollow2').click(function () {
  validateMobileNoAdd('mobileView');
  validateEmailAdd('emailView');
  validateLocationsView('locationView');
  validateFollowUpTypeView('followUpTypeView');
  validateBatchTypeView('batchTypeView');
  validateCoursesView();
  validateCommentMsgView('commentMsgView');
  validateDatePickerView('datePickerView');
  if (validateFollowUpTypeView('followUpTypeView') && validateBatchTypeView('batchTypeView') && validateCoursesView() &&
    validateCommentMsgView('commentMsgView') && validateDatePickerView('datePickerView') && validateMobileNoAdd('mobileView') && validateEmailAdd('emailView') && validateLocationsView('locationView')) {
    console.log('Okay');
    callFollowUp();

  } else {
    console.log('Check All Filled');
  }
});
function callFollowUp() {


  var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/addFollowUp';
 
  var locationView = $('#locationView').val();
  var followUpType = $('#followUpTypeView').val();
  var batchType = $('#batchTypeView').val();
  var email = $('#emailView').val();
  var mobile = $('#mobileView').val();
  var technology = [];
  var courses;
  $.each($('#chCourseList2 option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });
  var commentMsg = $('#commentMsgView').val();
  var nxtFollowUpdate = $('#datePickerView').datepicker('getDate');
  console.log('Newdate',nxtFollowUpdate);
  var dateNxtFollowUp = nxtFollowUpdate.getTime();
  console.log('dateNxtFollowUp :',dateNxtFollowUp);
 // var newdate = dateNxtFollowUp.toUTCString();
//  dateNxtFollowUp += 19800000;
// demoLStatusView = false;
$('#viewDemoStatus').click(function () {
  if ($(this).is(':checked')) {
    demoLStatusView = true;
  } else {
    demoLStatusView = false;
  }
});
  var followUpSend = {
    locality: locationView,
    followUpType: followUpType,
    batchType: batchType,
    addCourses: technology,
    nextFollowUpDate: dateNxtFollowUp,
    comment: commentMsg,
    demoLectureStatus: demoLStatusView,
    enteredBy: localStorage.userName,
    eMail: email,
    mobileNo: mobile,
    enquiries: {
      id: enquiriesList.id
    },
  };
  console.log('followUpSend',followUpSend);
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', addFollowUpURL, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(followUpSend));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       // alert('Followup added...!!!');
        $('#alertText').html('Followup added...!!!');
        $('#myModal').modal('show');
        $('#viewFollowUpForm')[0].reset();
        $('.multiselect-selected-text').text('Please select Courses');
        $('.multiselect').attr('title', 'Please select Courses');
        $('#modal-2').modal('hide');
        paginationFunctionFollowup();
        demoLStatusView = false;
      } else {
        console.log('xhr.statusText', xhr.statusText);
      }
    }
  };
}
$('#modal-2').on('hidden.bs.modal', function (e) {
  $('#followupView').html('');
  $('#tbldataViewFollow').html('');
  $('#chCourseList2').multiselect('rebuild');
})
$('#modal-1').on('hidden.bs.modal', function (e) {
  $('#followupView').html('');
  $('#tbldataViewFollow').html('');
})

// -----------------------------------------------    Followup Form --------------------------------------
$('#createFollowUp2').click(function () {
  var enroll = $('#enrollModeFollow').val();
  if (enroll === null) {
  } else {
    console.log('Yes');
  }
});

$('#createFollowUpInFollowUpTab').click(function () {
  var addFollowUpURL = protocol + server + ':' + port + appName + '/inquiry/addFollowUp';
  var followUpType = $('#followUpType2').val();
  var batchType = $('#batchTypeView').val();
  var courseNameEx = [] = $('#exCourse2').val();
  var technology = [];
  var courses;

  $.each($('#chCourseList2 option:selected'), function () {
    technology.push($(this).html());
    courses = technology.toString();
  });
  var exCourselist = enquiriesList.courses;
  var newCoursesList = technology;
  var addCourses = exCourselist.concat(newCoursesList);

  var commentMsg = $('#exampleTextarea2').val();
  var nxtFollowUpdate = $('#datepickerS1').val();
  var followUpSend = {
    followUpType: followUpType,
    batchType: batchType,
    addCourses: addCourses,
    comment: commentMsg,
    nextFollowUpDate: nxtFollowUpdate,
    demoLectureStatus: demoLStatusCH,
    enteredBy: localStorage.userName,
    enquiries: {
      id: enquiriesList.id
    },
  };
  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        var xhr = new ActiveXObject('Msxml2.XMLHTTP');
      } catch (exception) {
        var xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
    } else {
      var xhr = new XMLHttpRequest();
    }
  } else {
    alert('Your browser does not support XMLHTTP Request...!');
  }
  xhr.open('POST', addFollowUpURL, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(followUpSend));
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        console.log('got success');
        $('#addFollowUpForm')[0].reset();
        $('.multiselect-selected-text').text('Please select Courses');
        $('.multiselect').attr('title', 'Please select Courses');
        $('#openModel_viewFolloupModel').click();
        demoLStatusCH = false;
      } else {
        console.log('xhr.statusText', xhr.statusText);

      }
    }
  };
});
// --------------------------------- Timer Login --------------------------------------------
